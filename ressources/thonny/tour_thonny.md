---
title: "Premier tour d'horizon de THONNY "
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# Présentation et installation

![Thonny](./fig/thonny_ico.jpg)  

__Thonny__ est un environnement de programmation en Python ce qui signifie qu'il intégre à la fois une distribution Python récente (à l'heure actuelle __Python 3.7__) et un _éditeur de code_ associé à un _terminal_.

Ce logiciel libre peut être installé sur les plateformes usuelles : Windows, Mac et Linux. 


Thonny est déjà installé au lycée mais si vous souhaitez l'utiliser sur votre ordinateur personnel vous pouvez le télécharger à l'adresse http://www.thonny.org.

_Remarque_ : en installant Thonny, il devient donc inutile d'installer Python depuis le [site officiel](https://www.python.org/)

# Utilisation

Lors du premier lancement du logiciel voilà la fenêtre qui s'ouvrira :

![Thonny](./fig/ouverture_thonny.jpg)  

Cette fenêtre présente :
* une barre de menus   
* une barre d'icônes  
* une première zone pour l'instant nommée <untitled>  
* une deuxième zone nommée __Shell__  

## Le Shell

C'est la zone que vous utiliserez dès le début : elle permet d'exécuter des instructions Python simples, d'évaluer des valeurs simplement en les tapant à la suite de l'invite de commandes (prompt en anglais) marquée par trois chevrons (>>>) puis en validant avec la touche `Entrée`.

![Shell Thonny](./fig/shell.jpg) 


## La zone d'édition

C'est la zone pour l'instant nommée <untitled>. Vous pouvez y rédiger un code plus long.

![Editeur](./fig/editeur.jpg) 

Puis le sauvegarder sous un nom adapté (extension __.py__)

![sauvegarde](./fig/save.jpg) 

Et enfin l'exécuter 

![run](./fig/run.jpg) 

La zone d'interprétation (Shell) contient la trace d'éxécution de votre script. En grisé vous voyez apparaître la mention `%Run script.py` suivi à la ligne du résultat de l'exécution de votre script.

![execution shell](./fig/execution_shell.jpg) 


# Configuration

Afin de profiter au mieux des possibilités du logiciel, il est recommandé d'activer certaines options.  
Dans le Menu __Tools__/__options__  cohez tout

![options](./fig/options.jpg) 

Vous pouvez si vous le souhaitez également configuer le thème (couleurs) et la police en utilisant l'onglet __Theme & Font__

# Variables

Thonny permet pour les scripts sauvegardés et enregistrés de visualiser l'ensemble des variables utilisées.  
Il suffit d'activer __Variables__ dans le menu __View__

![menu view](./fig/view_var.jpg) 

Une boite supplémentaire s'affiche avec le nom de toutes les variables utilisées et les valeurs stockées.

![valeurs variables](./fig/var_boite.jpg) 

# Installation de modules supplémentaires

Vous utiliserez au cours de l'année des modules non fournis avec la distribution standard de Python tels que numpy, matplotlib,...
Thonny permet de gérer et d'installer ces modules très simplement.

Ouvrez le gestionnaire de package (menu __Tools__/ __Manage packages..__)

![valeurs variables](./fig/package.jpg) 

Tapez le nom du package à installer et cliquez sur `Find package from PyPi`

![valeurs variables](./fig/matplotlib.jpg)

Puis enfin validez en cliquant sur bouton  `install` en bas de la fenêtre

![valeurs variables](./fig/install.jpg)

Vous pouvez dorénavant importer votre module dans un script ( l'installation ne sera bien sûr plus à refaire)

