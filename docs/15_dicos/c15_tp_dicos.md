---
title: "Chapitre 15  : Les dictionnaires"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : QCM &#x1F3C6;

__1.__ On définit le dictionnaire `capitales = {'France': 'Paris', 'Espagne': 'Madrid, 'Italie' : 'Rome'}`  
Quelle expression permet de récupérer la valeur associée à la clé `'Espagne'`?

* [ ] `capitales.keys()`
* [ ] `capitales[1]`
* [ ] `capitales[2]`
* [ ] `capitales['Espagne']`


__2.__ Soit le dictionnaire `contacts = {'Ahmed': 0890807562, 'Caroline': 0875125623, 'Thomas': 0889125232}`  
Quelle instruction permet d' ajouter un nouveau contact nommé `Fred` avec pour numéro de téléphone `0855445678` ?

* [ ] `contacts.append({'Fred': 0855445678})`
* [ ] `contacts.append({'Fred', 0855445678})`
* [ ] `contacts['Fred'] = 0855445678`
* [ ] `contacts[3] = {'Fred', 0855445678}`


__3.__ Soit le dictionnaire `dico = {'nom' : 'John', 'mail' :'john_theboss@internet.com', 'age' : 24 }`. On exécute le code suivant

```python
for (cle, valeur) in dico.items() :
    if type(valeur) == int:
        print(cle)
```

Qu'affiche ce programme ?

* [ ] `24`
* [ ] `('age', 24)`
* [ ] `'age'`
* [ ] `{'age', 24}`

__4.__  Soit la variable `postes_travail` suivante

```python
postes_travail = [{'nom' : 'Yoan', 'poste' : 15}, 
                  {'nom' : 'Marina', 'poste' : 28}, 
                  {'nom' : 'John', 'poste' : 11}]
```

Quel expression permet d'accéder au numéro de poste de `Marina` ?

* [ ] `postes_travail[1][1]`
* [ ] `postes_travail[2][2]`
* [ ] `postes_travail['poste'][2]`
* [ ] `postes_travail[1]['poste']`


# Exercice 2 : Pokemons &#x1F3C6;
(_Extrait de Prépabac NSI, Hatier_)

On modélise des informations (nom, taille et poids) sur des Pokémons de la façon suivante :

```python
exemple_pokemons = {'Bulbizarre' : (70, 7),
                    'Herbizarre' : (100, 13),
                    'Abo' : (200, 7),
                    'Jungko' : (170, 52)}
```

Par exemple, Bulbizarre est un Pokémon qui mesure 70cm et qui pèse 7kg.

__1)__ Quel est de type de `exemple_pokemons`?    
__2)__ Quelle instruction permet d'ajouter à cette structure de données le Pokémon Goupix qui mesure 60cm et qui pèse 10kg ?   
__3)__ On donne le code suivant:

```python
def le_plus_grand(pokemons):
    grand = None
    taille_max = None
    for (nom, (taille, poids)) in pokemons.items():
        if taille_max is None or taille > taille_max:
            taille_max = taille
            grand = nom
    return (grand, taille_max)
```

__a-__ Que vaut l'expression `le_plus_grand(exemple_pokemons)` ?  
__b-__ Ecrire le code d'une fonction `le_plus_leger` qui prend un dictionnaire de Pokémons en paramètre et qui renvoie un tuple dont la première composante est le nom du Pokémon le plus léger et la deuxième correspondante est son poids.

```python
>>> le_plus_leger(exemple_pokemons) 
('Bulbizarre', 7)
```
__c-__ 🥇 Réécrire le code de la fonction `le_plus_grand` en parcourant cette fois-ci les valeurs du dictionnaire.

__4)__ Ecrire le code d'une fonction `taille` qui prend un dictionnaire de Pokémons en paramètre ainsi que le nom d'un Pokémon et qui renvoie la taille de ce Pokémon

```python
>>> taille(exemple_pokemons, 'Abo') 
200
```

# Exercice 3 : Dictionnaires par compréhension &#x1F3C6;&#x1F3C6;

Construire les dictionnaires par compréhensions suivants :  

__1)__ __clés__ : les nombres entiers de 0 à 100 inclus ; __valeurs__ :  leur racine carrée.  

__2)__ __clés__ : les nombres pairs de 0 à 20 inclus ; __valeurs__ : leur cube  

__3)__ __clés__ :  les caractères ASCII imprimables ; __valeurs__ : le code décimal associé  

__4)__ Soit la liste `pays = ['France', 'Espagne', 'Italie', 'Allemagne', 'Belgique']`  
__clés :__ les pays présents dans `liste` ; __valeurs :__ le nombre de caractères utilisés pour écrire ce pays.  


# Exercice 4 : Magasin en ligne &#x1F3C6;&#x1F3C6;

(_Extrait de https://bioinfo.mnhn.fr/abiens/ISUP5G3/docu/ISUP-elemprog-exos_2016-09-16-student.pdf_)

Dans cet exercice, nous nous familiarisons avec les manipulations de dictionnaires sur une thématique de magasin en ligne.
_«Chez Geek and sons tout ce qui est inutile peut s’acheter, et tout ce qui peut s’acheter est un peu trop cher.»_  
La base de prix des produits de _Geek and sons_ est représentée en Python par un dictionnaire de type `dict[str:float]` avec :

* les noms de produits, de type `str`, comme clés
* les prix des produits, de type `float`, comme valeurs associées.


__1)__ Donner une expression Python pour construire un dictionnaire `base_geek_sons` comprenant les produits correspondant à la table suivante associés à leur prix repectif.

|Nom du produit| Prix TTC|
|:----:|:----:|
|Sabre laser| 229.0|
|Mitendo DX |127.30|
|Coussin Linux |74.50|
|Slip Goldorak |29.90|
|Station Nextpresso| 184.60|


__2)__ Définir le prédicat `disponibilite` qui étant donné un nom de produit `prod` (_sous forme de chaîne de caractères_) et une base de prix `prix` (_sous forme de dictionnaire_), renvoie `True` si le produit est présent dans la base, `False` sinon.

__3)__ Définir la fonction `prix_moyen` qui, étant donné une base de prix `base_prix` (contenant au moins un produit), renvoie le prix moyen des produits disponibles.

Par exemple :

```python
>>> prix_moyen({'Sabre Laser': 229.0, 
                'Mitendo DX': 127.30,
                'Coussin Linux' : 74.50,34, 
                'Slip Goldorak' : 29.90,
                'Station Nextpresso' : 184.60})
129.06
```

__4)__ Définir la fonction `fourchette_prix` qui, étant donné un prix minimum `mini`, un prix maximum `maxi` et une base de prix `base_prix`, renvoie  l’ensemble des noms de produits disponibles dans cette fourchette de prix sous forme de tuple de chaînes de caratères.

Par exemple :

```python
>>> fourchette_prix(50.0, 200.0, 
                    {'Sabre Laser': 229.0,
                    'Mitendo DX': 127.30, 
                    'Coussin Linux' : 74.50,
                    'Slip Goldorak' : 29.90, 
                    'Station Nextpresso' : 184.60})

('Coussin Linux', 'Mitendo DX', 'Station Nextpresso')
```

__5)__ Le panier est un concept omniprésent dans les sites marchands, Geeks and sons n’échappepas à la règle. En Python, le panier du client sera représenté par un dictionnaire de type `dict[str:int]` avec :

* les noms de produits comme clés
* une quantité d’achat comme valeurs associées.

Donner une expression Python correspondant à l’achat de 3 sabres lasers, de 2 coussins Linux, et de 1 slip Goldorak.

__6)__  Définir le prédicat `tous_disponibles` qui, étant donné un panier d’achat `panier` et une base de prix `base_prix` renvoie `True` si tous les produits demandés sont disponibles, `False` sinon.

__7)__ Définir la fonction `prix_achats` qui, étant donné un panier d’achat `panier` et une base de prix `base_prix`, renvoie le prix total correspondant.

Par exemple :

```python
>>> prix_achats({'Sabre Laser': 3, 'Coussin Linux': 2, 'Slip Goldorak': 1},
                {'Sabre Laser': 229.0,
                'Mitendo DX' : 127.30, 
                'Coussin Linux' : 74.50,
                'Slip Goldorak' : 29.90,
                'Station Nextpresso' : 184.60})
865.9
```
_Remarque_ : on supposera que tous les articles du paniers sont disponibles dans la base de produits.


# Exercice 5 :  Statistiques sur les lettres 🚀

(_Extrait de https://bioinfo.mnhn.fr/abiens/ISUP5G3/docu/ISUP-elemprog-exos_2016-09-16-student.pdf_)

Dans cet exercice, on effectue quelques calculs statistiques sur les fréquences de lettres dans des textes (chaînes de caractères).

Les fréquences (ou nombre d’occurrences) des lettres sont représentées sous la forme d’un dictionnaire de type dict[str:int] avec :

* des lettres (caractères) comme clés
* des entiers naturels (fréquence du caractère) pour les valeurs associées

Pour séparer les lettres de la langue française des autres caractères possibles dans les chaînes, on utilise la fonction suivante :

```python
def est_lettre(c):
    """
    :param str c: un caractère  len(c) == 1
    :return: True si le caractère c est une lettre, ou False sinon.
    :rtype: bool
    """
    return (c >= 'a' and c <= 'z') or (c >= 'A' and c <= 'Z') or (c in ['é', 'è', 'à', 'ù', 'oe'])
```

__1)__ Définir la fonction `frequences_lettres` qui étant donnée un chaîne de caractère `s` renvoie les fréquences des lettres de `s` sous la forme d’un dictionnaire de type dict[str:int].
Par exemple :

```python
>>> frequences_lettres('alea jacta est')
{'j': 1, 'e': 2, 't': 2, 'c': 1, 'a': 4, 's': 1, 'l': 1}
>>> frequences_lettres("l'élève")
{'é': 1, 'e': 1, 'v': 1, 'l': 2, 'è': 1}
```

__2)__ Définir une fonction `lettre_freq_max` qui renvoie la lettre de fréquence maximale dans un dictionnaire `freqs` de fréquences.

Par exemple :

```python
>>> lettre_freq_max(frequences_lettres('alea jacta est'))
'a'
>>> lettre_freq_max(frequences_lettres("l'élève"))
'l'
```

_Remarque_ : s’il y a plusieurs lettres de fréquence maximale, alors on n’en retourne qu’une choisie arbitrairement.


__3)__ Dans cette question, nous aimerions effectuer notre petit test statistique sur un véritable texte. 
Pour cela, nous allons tout d’abord définir une fonction `chargement_texte` permettant de lire un fichier texte et de placer le résultat dans une chaîne de caractères.

_Remarque_ : nous n’étudions pas le chargment et la sauvegarde des fichiers dans ce cours, donc
on utilisera cette fonction en suivant simplement sa spécification.

```python
def chargement_texte(fichier):
    """
    :param str fichier: nom du fichier (le fichier est placé dans le même dossier que le script)
    :return: la chaîne de caractères correspondant au contenu du fichier.
    :rtype :str
    """
    contenu = '' # contenu du fichier
    with open(fichier, 'r') as f:
        contenu = f.read()
    return contenu
```

On récupérera alors un fichier texte (encodage UTF-8) de langue française pour en étudier le contenu.
On peut par exemple récupérer un texte intégral via le Projet Gutemberg, à l’adresse suivante : http://www.gutenberg.org    

Pour les exemples on a choisi Quatrevingt treize de Victor Hugo que l’on trouvera
dans :  `/Vrac/1I001/quatrevingt-treize.txt`

Donner deux expressions Python permettant de :  

__a-__ Récupérer le dictionnaire des fréquences des lettres présentes dans votre texte d’exemple.  
__b-__ Trouver la lettre dont la fréquence est la plus grande.  

