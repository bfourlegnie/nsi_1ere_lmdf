---
title: "Chapitre 15  : Les dictionnaires "
subtitle: "Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# La structure de données dictionnaires

## Inconvenients des tuples et des listes

Les tuples correspondent à une structure de données qui permet de représenter une suite ordonnée d'éléments.

Par exemple, pour représenter un élève de 1ère dans une base de données, on pourrait avoir envie de conserver : 

* son nom
* son prénom
* sa classe
* ses trois spécialités

Si on utilise un tuple pour représenter un élève, on aurait :

```python
e1 = ("Kaczmarek", "Anissa", "1F", ("nsi", "mathématiques","SVT"))
e2 = ("Dupont", "Kévin", "1G", ("SVT", "mathématiques", "biologie-écologie"))
```
On peut aussi représenter l'ensemble des élèves ainsi par exemple :

```python
eleves = [e1, e2]  ## et sûrement beaucoup d'autres
```

Supposons qu'on veuille écrire maintenant une fonction qui renvoie la liste des élèves (nom et prénom sous forme d'une chaîne de caractères) qui ont choisi une certaine spécialité :

```python
def eleves_specialite(spe, eleves) :
    """ 
    Renvoie la liste des noms et prénoms des élèves qui ont choisi la spécialité spe.
    :param str spe: la spécialité
    :param list eleves : tableau contenant tous  les élèves
    :return : ["nom1 prenom1", "nom2 prenom2", ... ]
    :rtype: list
    """
    res = []
    for eleve in eleves :
        if spe in eleve[3] :
            res.append(eleve[0] + " " + eleve[1])
    return res
```

```python
>>> eleves_specialite("nsi",eleves)
["Kaczmarek Anissa", "Dupont Kévin"]
```

Ici, le fait de numéroter les différentes caractéristiques d'un élève :

* conduit à un code qui n'est pas clair (quel peut être le sens de `eleve[3]` ?)
* n'est pas facilement maintenable : si on décide d'ajouter l'âge de l'élève après son nom et son prénom, il faut réécrire la fonction

> 📢 Il existe une structure de donnée appelé __tuple nommé__ dans laquelle les indices sont remplacés par des __clés__  (souvent sous forme de chaînes de caratères) associée à une valeur. En Python les tuples nommés sont implémentés par des __dictionnaires__.

## Utilisation de dictionnaires

Si on reprend l'exemple de l'élève, on pourrait associer à chaque caractéristique de l'élève le nom de cette caractéristique :

* `"nom"` : la chaîne de caractères représentant le nom
* `"prenom"`  : la chaîne de caractères représentant le prénom
* `"classe"`  : la chaîne de caractères représentant la classe 
* `"spes"`  : le tuple des spécialités choisies

On reprend notre exemple :

```python
>>> e1 = {"nom" : "Kazcmarek", "prenom" : "Anissa", "classe" : "1F", "spes" : ("nsi","mathématiques","SVT")}
>>> type(e1)
<class 'dict'>
```
> 📢 On a donc créé ainsi un `dictionnaire` contenant des paires `clé:valeur`.   
>  Chaque paire est composée d'une clé et de la valeur correspondante séparées par deux points __`:`__. Les paires sont séparées entre elles par des __`,`__.  Un dictionnaire est délimité par des accolades __`{}`__

* Dès lors __on peut accéder à une valeur du dictionnaire par sa clé__ et non  plus par son indice ce qui sera plus 'parlant'

```python
>>> e1["classe"]
'1F'
```

* __Les valeurs contenues dans un dictionnaire sont mutables__

Imaginons que l'èleve 1 change de classe

```python
>>> e1["classe"] = "1A"   
>>> e1
{'nom': 'Kazcmarek', 'prenom': 'Anissa', 'classe': '1A', 'spes': ('nsi', 'mathématiques', 'SVT')}
```

* __On peut tout à fait ajouter de nouvelles clés à un dictionnaire__

```python
>>> e1["spe_validee"] = "SVT"
>>> e1
{'nom': 'Kazcmarek', 'prenom': 'Anissa', 'classe': '1A', 'spes': ('nsi', 'mathématiques', 'SVT'), 'spe_validee': 'SVT'}
```

Grâce à ces élèments, on va pouvoir clarifier le code de la fonction `eleves_specialite` précédente

```python
def eleves_specialite_bis(spe, eleves) :
    # chaque élément de la liste eleves est un dictionnaire
    res = []
    for eleve in eleves : 
        if spe in eleve["spes"] :
            res.append(eleve["nom"] + " " + eleve["prenom"])
    return res
```

On a éliminé les indices rendant le code ici beaucoup plus compréhensible.

# Opérations sur un dictionnaire

## Construire un dictionnaire 

* On peut construire des dictionnaires vides ou contenant déjà des paires cle:valeur

```python
>>> dico_vide = {} # initialisation dictionnaire vide
>>> contacts = {'Lea' : "lea.flo@truc.com", 'Milo' : "mimi@liberty.fr"} # dictionnaire rempli lors de l'initialisation
```

* On peut tout à fait créer un __dictionnaire en compréhension__ avec une syntaxe équivalent à celle vue pour les listes. Il faut préciser la clé et la valeur pour chaque élément.

Par exemple : Dictionnaire contenant pour les clés, les nombres entiers de 1 à 10 inclus et pour valeurs associées le carré de ces nombres.

```python
>>> carres = {n : n**2 for n in range(1, 11)}
>>> carres
{1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81, 10: 100}
```

## Modifier un dictionnaire

* __Créer une clé avec une valeur__. On peut aussi modifier celle-ci facilement :

```python
>>> smoothie = {'bananes':2,'oranges':1, 'litchis': 4 } 
>>> smoothie['kiwis'] = 1  # créé une clé "kiwis" associée à la valeur 1
>>> smoothie['kiwis'] = None  # on modifie la valeur associée à "kiwis"
>>> smoothie
{'bananes': 2, 'oranges': 1, 'litchis': 4, 'kiwis': None}
```

* __Supprimer une clé avec la méthode `del()`__

```python
>>> del(smoothie['kiwis'])
>>> smoothie
{'bananes': 2, 'oranges': 1, 'litchis': 4}
```

## Éléments d'un dictionnaire, taille d'un dictionnaire

> 📢 Chaque *clé* ne peut apparaître qu'une seule fois dans un dictionnaire (chaque *clé* n'est associée qu'à une seule *valeur*). Par contre, une même *valeur* peut être associée à plusieurs *clés* : on peut avoir 18 ans, et aussi 18 de moyenne : 

```python
>>>  mon_dico = {'nom': 'Durant', 'prenom': 'Jean', 'age': 18, 'moyenne': 18}
```

* La **taille** d'un dictionnaire est donc le nombre de paires clé:valeur qui est égal au **nombre de clés**.

```python
>>> len(mon_dico)
4
```

## Parcourir un dictionnaire

__Un dictionnaire n'est pas une structure ordonnée contrairement aux listes ou aux tuples__. Cela se taduit notamment par le fait que les clés ne s'affichent pas dans l'ordre dans lequel on les rentre. On ne peut donc pas parcourir un dictionnaire suivant un ordre donné ce qui n'aurait aucun sens, cependant on peut parcourir :

* ses clés
* ses valeurs
* ou encore ses paires clé:valeur

* __Parcourir les clés d'un dictionnaire avec `keys()`__

La méthode `keys()` appliquée à un dictionnaire renvoie un itérateur (Objet qui permet de parcourir tous les éléments contenus dans une collection) sur les clés.

A Gondecourt il y a 2 salons de coiffure, 2 boulangeries, 1 bureau de poste :

```python
>>> gondecourt = {"salon_coiffure" : 2, "boulangerie" : 2, "poste" : 1}
>>> for cle in gondecourt.keys():
        print(cle)

salon_coiffure
boulangerie
poste
```

On peut tester si une clé existe en utilisant l'opérateur `in` :

```python
>>> 'bibliotheque' in gondecourt.keys()
False
```

* __Parcourir les valeurs d'un dictionnaire avec `values()`__

La méthode `values()` appliquée à un dictionnaire renvoie un itérateur sur les clés.

```python
>> for val in gondecourt.values():
    print(val)

2
2
1
```

* __Parcourir les paires clé:valeur  avec `items()`__

La méthode `items()` appliquée à un dictionnaire renvoie un itérateur sur les couples donnés sous forme de tuples

```python
>>> for couple in gondecourt.items():
    print(couple)

('salon_coiffure', 2)
('boulangerie', 2)
('poste', 2)
```



    
