---
title: "Chapitre 1 Découverte Python"
subtitle: "PARTIE 2 - Cours : Les fonctions"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

Dans ce cours vous aprendrez comment utiliser des fonctions prédéfinies mais également comment définir vos propres fonctions réutilisables.

# Les fonctions natives et leur documentation 

En Python un certain nombre de fonctions sont déjà disponibles. 
Vous en avez déjà rencontré une : `type()`.  
Il en existe bien d'autres, par exemple la fonction `round()` :

```python
>>> round(1.2356)
1
```

On devine bien son intérêt mais en mathématiques plusieurs types d'arrondis sont possibles (inférieur, supérieur, au plus proche avec un certains nombres de décimales...) il est sage de vérifier dans la __documentation__ de cette fonction.  

Cela est possible avec la fonction native `help()`

```python
>>> help(round)
Help on built-in function round in module builtins:

round(number, ndigits=None)
    Round a number to a given precision in decimal digits.
    
    The return value is an integer if ndigits is omitted or None.  Otherwise
    the return value has the same type as the number.  ndigits may be negative.
```

Cette fonction permet d'accéder à la documentation de n'importe quelle fonction dont le nom est passé en __argument__ (_entre parenthèses_)

Ici par exemple on peut lire que :  

* `round()` est une fonction native du langage Python : `function....in module builtins`  
  
* Elle peut accepter au maximum 2 arguments : `(number, ndigits=None)`  
  
* Elle arrondit à un certain nombre de décimales : `Round a number to a given precision in decimal digits.`  
  
* Si le 2ème argument n'est pas précisé, l'arrondi est entier : `The return value is an integer if ndigits is omitted or None`

Lors de la conception d'une fonction les programmeurs ont donc veillé à bien documenter leur code : une __pratique indispensable__ !

La liste compléte des fonctions natives est disponible [ici](https://docs.python.org/fr/3.5/library/functions.html)


# Fonctions personnalisées

## Définir ses propres fonctions 

Les fonctions natives sont peu nombreuses, un langage de programmation avec lequel aucune nouvelle fonction ne pourrait être créé serait peu intéressant et très limité.

Voici un exemple de bloc de code définissant une nouvelle fonction personnalisée dont l'intérêt est d'additionner deux nombres.

```python
>>> def addition(a, b):
        somme = a + b
        return somme
```

_Remarques_ :  

* Ici, la fonction a pour objectif de calculer la somme de deux nombres a et b.  

* Le bloc de définition commence par l'__en-tête__ de la fonction composé du mot clé `def` et  suivi d'un nom personnalisé ici _addition_ (à l'instar de celui qui pourrait être donnée à une variable).  

* Après le nom de la fonction, il est obligatoire d'insérer des parenthèses `()` et si nécessaire des noms de paramètres séparés par des `,`.  
Le nombre de paramètres dépend de la fonction (il peut même ne pas y en avoir, mais __les parenthèses restent obligatoires__).   

* La première ligne se termine obligatoirement  par `:`

* Après le saut de ligne __4 espaces__ sont automatiquement créés : le code ne sera pas valide s'ils ne sont pas présents : on parle d'__indentation__.   
Cela permet notamment de délimiter le bloc de code correspondant au __corps de la fonction__.

* Une première instruction est réalisée avec notamment la création une nouvelle variable `somme` et l'affectation du résultat de `a + b`.  
  
    > On peut utiliser autant d'instructions que nécessaire dans une fonction.

* Enfin, l'instruction `return` indique que la fonction founira une __valeur de retour__ lorsqu'elle sera utilisée.
Cette valeur correspondra ici à l'évaluation de l'expression `somme`.

    > L'instruction `return` permet non seulement de renvoyer un résultat mais aussi d'__interrompre__ l'exécution de la fonction.

On aurait pu bien évidemment dans ce cas simple définir la fonction ainsi :

```python
>>> def addition(a, b):
        return a + b
```

En résumé la déclaration d'une fonction dans le cas général se réalise ainsi : 

```python
def nom_fonction(param1, param2, ...):
    <instruction_1>
    <instruction_2>
    ......
    return <expression>
```

## Appel de fonctions

Une fois une fonction personnalisée proprement définie, on peut l'utiliser dans le script en procédant à un __appel__, par exemple :

```python
>>> addition(4, 5)
9
```

Le nombre d'__arguments__ fournis lors de l'appel de la fonction doit être le bon et bien sûr parfois l'ordre à une importance.

Les arguments d'appels (ici `4` et `5`) sont les valeurs prises par les paramètres avec lesquels sont définis la fonction (respectivement `a` et `b`).


### Sauvegardez ses fonctions dans un script

Jusqu'à présent nous avons utiliser la fenêtre `Shell` de l'éditeur Python : chaque instruction devait être entrée ligne par ligne puis validée.

Un inconvénient majeur de cette pratique est que vos programmes disparaissent quand vous fermez le logciel Thonny.  
Nous pouvons donc rédiger un code entier dans la partie `Editeur`.

* Testons-le avec la fonction précédente :

![Fenêtre editeur](./fig/editeur.png)

* Sauvegardons le script : __Menu File/Save as__  
Remarquez l'extension du fichier créé : `.py`

* Pour exécutez le script, il suffit dorénavant d'appuyez sur Run (fléche blanche sur fond vert) ou `F5`

La fonction addition est dorénavant connue, elle peut être appelée autant de fois que l'on souhaite.

![Execution script](./fig/execution_script.png)

## Générez la documentation associée : les doctstrings.

Précédemment, nous avons utilisé la fonction native `help()` pour accéder à la documentation de quelques fonctions.  
On peut générer ce type de documentation en ajoutant une __docstring__ c'est à dire une chaîne de caractères (sur plusieurs lignes éventuellement) délimitée par un couple de 3 guillemets doubles `"""` et placée juste après l'en-tête de la fonction.

```python
>>> def addition(a, b):
        """
        Renvoie la somme de deux nombres
        """
        somme = a + b
        return somme
```
_remarque_ : n'oubliez pas l'indentation !

On peut dès lors accéder à la __documentation__ de la fonction :

```python
>>> help(addition)
 Help on function addition in module __main__:
 
 addition(a, b)
     Renvoie la somme de deux nombres
```

Beaucoup plus d'informations peuvent être intégrées dans les docstring, nous en reparlerons...

# Importer des fonctions  : les modules

Les __modules__ sont des __scripts__ Python qui contiennent notamment un ensemble de fonctions que l'on est amené à réutiliser souvent (on les appelle aussi bibliothèques ou librairies). 

L'installation de Python fournit en plus des fonctions natives une bibliothéque standard contenant plusieurs modules pouvant répondre à des besoins divers :

* le module `math` contenant des fonctions mathématiques courantes
* le module `random` utilisant des nombres aléatoires
* le module `Tkinter` permettant de réaliser des interfaces graphiques
* le module `turtle` permettant de dessiner
et bien d'autres .....

Toutes ces fonctionnalités n'étant pas utiles dans tous les programmes, les modules correspondants ne sont donc pas chargés au démarrage. 
Il faut le réaliser manuellement.

## Le module math 

Voyons tout d'abord comment importer le module  `math` :

```python
>>> import math
```

Le mot clé `import` suivi du nom du module permet d'importer l'intégralité de ce dernier.  
On peut le vérifier en accédant à la documentation :

```python
>>> help(math)
 ........
 FUNCTIONS
 ........
 sqrt(x, /)
        Return the square root of x.
```

On constate par exemple que le nombre de fonctions disponibles est conséquent.
Pour plus de simplicité on peut consulter la [documentation en ligne](https://docs.python.org/fr/3.5/library/math.html)

Pour utiliser l'une de ces fonctions on doit l'appeler à l'aide d'une __notation pointée__ : `nom_module.nom_fonction()`. 
Ceci donne par exemple pour la fonction _racine carrée_ :

```python
>>> math.sqrt(9)
3.0
```

Bien évidemment, il est rarement intéressant d'importer la totalité du module pour des raisons évidentes de consommation mémoire et d'espace de nommage (_notion vue ultérieurement_).   
On peut procéder à l'import des seules fonctions nécessaires si on connait leurs noms. Leur utilisation se fera alors directement comme n'importe quelle autre fonction créée dans le script.

```python
>>> from math import sqrt
>>> sqrt(4)
2.0
```

Il est bien évidemment possible d'importer plusieurs fonctions, la syntaxe générale sera alors : 
> from `nom_module` import `fonction_1`, `fonction_2`, ...

## Le module random

Ce module regroupe toutes les fonctions liées à la génération de nombres aléatoires, la documentation est disponible [ici](https://docs.python.org/fr/3/library/random.html?highlight=random#module-random)

Prenons un cas simple nous souhaitons générer le tirage d'un dé 6 utilisé dans beaucoup de jeux de société.  
La fonction `randint()` présente dans le module nous sera utile.

Commençons d'abord par importer uniquement cette fonction et consultons la documentation associée :  

```python
>>> from random import randint
>>> help(randint)
 Help on method randint in module random:

 randint(a, b) method of random.Random instance
     Return random integer in range [a, b], including both end points.
```

La documentation nous précise que `randint()` accepte deux paramètres entiers notés `a` et `b` et que la valeur de retour est un entier compris entre _a_ et _b_ (inclus tous les deux) : $`randint(a, b) \in [\![a,b]\!]`$.

Ce qui donne dans notre cas :

```python
>>> randint(1, 6)
```

# Complements : les commentaires

Vous allez être amené à manipuler de très nombreuses fonctionnalités du langage Python et de ses modules, il est inenvisageable d'acquérir une connaissance exhaustive de toutes celles-ci.  

Dans les scripts que vous utiliserez vous pouvez déposer des commentaires à l'aide du symbole `#` pour expliquer certaines parties de codes ou certains choix.    

> Ils ne seront pas interprétés mais vous aideront à mieux comprendre le code.

N'hésitez pas à en ajouter dans vos scripts pour expliciter une ligne de code ambigüe.

```python
>>> x = 3  # x est une variable correspondant à une abscisse
>>> x
3
```