---
title: "Chapitre 1 Découverte Python "
subtitle: "Partie 1 - Cours: Manipuler les nombres "
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

Les documents suivants sont construits de telle manière à ce que vous observiez des exemples simples commentés puis que vous réalisiez les exercices proposés au fur et à mesure.
Il est important de tester vous même les petits morceaux de code proposés dans les exemples avant de passer aux exercices

# Calculer avec Python

Dans cette introduction, vous utiliserez Python en mode interactif via la zone __Shell__ du logiciel Thonny. 

Cette zone permet de tester du code sans avoir à la sauvegarder au préalable.  

![shell de Thonny](./fig/shell_thonny.jpg)

Une invite de commande aussi appelée __prompt__ et symbolisée par 3 chevrons `>>>` vous permet d'écrire directement du code qui sera interprété après l'appui sur la touche `entrée`.

## Les nombres entiers

En langage Python on distingue deux catégories de nombres : les nombres __entiers__ et les nombres __réels__ (appelés aussi nombres à _virgule flottante_)

On peut bien évidemment utiliser les quatre opérations arithmétiques sur ces nombres (tester-le dans le _Shell_ et valider avec la touche `entrée`.

```python
>>> 50 - 4
46
```

Les [priorités des opérations](https://fr.wikibooks.org/wiki/Programmation_Python/Tableau_des_op%C3%A9rateurs) sont bien sûr respectées

```python
>>> 2 * 3 + 4
10
```

En Python il n'existe aucune limitation sur la taille des entiers.  
Voici comment par exemple calculer $`5^{100}`$

```python
>>> 5 ** 100
7888609052210118054117285652827862296732064351090230047702789306640625
```

On peut vérifier le type de la donnée (dans nos exemples précédents :  type entier) à l'aide de la fonction `type` .
Cette fonction retourne dans le cas d'un entier `<class 'int'>` : int provient du mot anglais _integer_ (nombre entier)

```python
>>> type(4)
<class 'int'>
```

## Les nombres à virgule flottante

Les nombres à virgule flottante sont des nombres comportant une partie entière et une partie décimale après la virgule  marquée par un point (notation anglo-saxonne).
Par exemple :  _1.2_     ou   _1._   ou encore _.001_

```python
>>> 3.141592
3.141592
```

Lorsque la partie entière d'un nombre flottant devient trop grande : le nombre est donné en notation scientifique (comme sur vos calculatrices).  
Ici __e__ signifie _"fois dix puissance "_ :   $`\times10^{n}`$.

```python
>>> 4.2 ** 6
1.601332619247766e+16
```
```python
>>> 15.1 ** -8
3.699852593912391e-1
```

On peut d'ailleurs directement écrire les nombres flottants sous cette forme :

```python
>>> 6.02e21
6.02e+21
```

On peut vérifier le type de cette nouvelle donnée.  
Ici, la fonction retourne  `<class 'float'>`

```python
>>> type(1.60e-19)
<class 'float'>
```

Enfin, il est intéressant de noter que Python convertit implicitement les entiers en flottants si nécessaire :

```python
>>> 4 + 5.2
9.2
```

## Les divisions

En Python3, l'opérateur de division donne __toujours__ en résultat un nombre à virgule flottante.

```python
>>> 1 / 3
0.3333333333333333
```
```python
>>> 18 / 6
3.0
```

On peut également effectuer une division entière (aussi appelée __division euclidienne__)  

![division euclidienne](./fig/divisioneuclidienne.png)

[^1] 

On rappelle que $`dividende = diviseur \times quotient + reste`$

On peut alors utiliser l'opérateur __division entière__  `//`  qui permet de retrouver le quotient.

```python
>>> 18 // 7
2
```

Mais aussi l'opérateur __reste de la division entière__   `%`, lui,  permet de retrouver le reste.  
Cet opérateur se nomme aussi __modulo__.

```python
>>> 18 % 7
4
```

# Les variables

## Déclaration et affectation

Une __variable__ est un emplacement réservé dans la mémoire associée à un _nom de variable_ : c'est à dire un ou plusieurs caractères que le programmeur aura choisi.  
On peut lui __affecter__ une __valeur__ à l'aide de l'opérateur `=`, par exemple :

```python
>>> x = 5.0
```

Remarquez qu'en exécutant ces lignes à priori rien ne se passe.  
 En fait une nouvelle référence désignant une adresse mémoire ( _un emplacement précis de la mémoire_) a été créé et a été associée à un objet de type _int_ ou _float_ suivant le cas.

```python
>>> type(x)
<class 'float'>
```

Puisque nous avons défini la variable `x`, nous pouvons l'utiliser dans des expressions pour effectuer des calculs.

```python
>>> 2 * x + 4
14.0
```

Le nom d'une variable est assez libre veillez cependant à ne pas commencer par un chiffre et qu'il __ne comporte pas d'espace!__

```python
>>> c_lumiere_vide = 299792458
14.0
```
```python
>>> c lumiere vide = 299792458
  File "<pyshell>", line 1
    c lumiere vide = 299792458
            ^
 SyntaxError: invalid syntax
```

Certains noms sont réservés, attention ! En voici la [liste](https://fr.wikibooks.org/wiki/Programmation_Python/Tableau_des_mots_r%C3%A9serv%C3%A9s)


```python
>>> def = 4
  File "<pyshell>", line 1
    def = 4
        ^
SyntaxError: invalid syntax
```

## Recommandations

En python, il existe des recommandations d'écriture qui facilitent la lecture du code.  

[recommandations PEP8](http://nguyen.univ-tln.fr/share/Python/pep8.pdf)  

On préférera par exemple éviter les accents dans les noms de variables et utiliser uniquement des lettres minuscules.  

Si le nom de la variable est composé on utilise généralement le caractère _blanc souligné_ `_` pour séparer les mots.  

En fin les noms de variables doivent être explicites, on évite sauf cas particulier les lettres uniques.  

Par exemple :  
* On préférera `vitesse` à ~~`Vitesse`~~  
* On préférera `moyenne_generale` à ~~`moyennegenerale`~~  
* On préférera `abscisses` à ~~`x`~~  

---

_Sources_

[^1]: image division euclidienne  https://www.jeuxmaths.fr
