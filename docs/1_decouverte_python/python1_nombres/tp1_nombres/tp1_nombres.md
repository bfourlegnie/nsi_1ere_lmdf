---
title: "TP Calculer avec Python"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---


# Exercice n°1 : Opérateurs mathématiques &#x1F3C6;
Calculez les valeurs des expressions suivantes d'abord sans le Shell puis vérifiez.  
Déduisez-en à chaque fois les priorités lors d'évaluations des expressions contenant `()`, `*`, `+` , `-`, `**`, `/`.

|Expressions|Valeurs après évaluation|Priorités lors de l'évaluation|
|:----------|:------|:--------|
|10 + 2 * 7 |24     | * prioritaire sur +|
|(10 + 2) * 7||
|2 * (3 - 1)||
|35 - 5 * 8||
|2 ** 1 + 1||
| 3 * 1 ** 10||
|1 ** (5 - 2)||
|3 / 2 * 4||
|3 / 1 / 3||

__Conclusions sur les priorités :__


# Exercice n°2 : Nombres entiers et à virgule flottante &#x1F3C6;

__1)__ Ecrire les nombres suivants dans le Shell (en respectant la syntaxe imposée par le langage).  
Evaluez-les et notez leur type. 

|Nombres| Evaluation | type|
|:--------|:------:|:----:|
|_exemple :_ 123|    123       | int (entier)|
| 0,0005  | | |
| 2,0 |  | |
| 2  | | |
| $`1,38\times10^{2}`$  | | |
| $`1\times10^{2}`$ (à écrire avec `**`)  | | |
|  $`1\times10^{2}`$ (à écrire avec `e`)| | |
| $`2\times10^{-4}`$  | | |
| 423  | | |


__2)__ 🥇Tapez les instructions suivantes

```python
>>> import sys
>>> sys.float_info
```
On accéde ainsi aux limitations du langage lors de la manipulation de nombres à virgule flottante.  

__a-__ Notez la valeur maximale pouvant être atteinte par un flottant en python :  `max`  
__b-__ Testez ce qui se passe au delà de cette valeur.  
__c-__ Que signifie l'indication renvoyée ?

# Exercice n°3 :   Divisions &#x1F3C6;

Evaluez les expressions suivantes d'abord sur papier puis vérifier en utilisant le Shell  
```python
>>> 5 / 2

>>> 4 / 2

>>> 4 // 2

>>> 4 % 2

>>> 5 % 2

>>> 7 + 7 % 2
```


# Exercice n°4 : utilisation de variables &#x1F3C6;&#x1F3C6;

__1)__ Pour chacune des lignes de code suivantes indiquez l’erreur commise

```python
>>> moyenne eleve = 10

>>> 12 = moyenne

>>> a = 3 + s

>>> y - 1 = 8

>>> if = 50
```

__2)__ Déclarez deux variables nommées `dividende` et `diviseur` en leur attribuant respectivement les valeurs 15 et 3.  

- Déclarez une variable `quotient` et établissez son expression.
- Déclarez une variable `reste` et établissez son expression.


__3)__ Assignez respectivement les valeurs __1__, __5__ , __9__ aux trois variables __x__, __y__, __z__ puis testez ensuite les expressions suivantes et vérifier leur valeur

|Expression             |	Valeur attendue|
|:---------------------:|:----------------:|
| $`x^{4}+z^{y}`$      |       	59050  |
|  $`y\times10^{x}-z`$ |   	41         |
|$`\frac{(x+y)}{z}`$    |0.6666666666666666|
|$`\frac{(x+y)^{2}}{z^{3}}`$|0.04938271604938271|
| $`(\frac{y\times10^{x}+y}{z})^{3}`$|228.22359396433467|


# Exercice n°5 : Choix des variables &#x1F3C6;&#x1F3C6;

__1)__ Proposer des noms de variables correspondants à ces situations et respectant les recommandations d'écriture:

* Température mesurée dans la salle de classe
* nombre de poissons dans un aquarium
* nombre à deviner

__2)__ La fréquence d'un signal sonore est de 20kHz. Après avoir déclaré la variable adaptée à la situation. Donnez l'expression de la période de ce signal (vous utiliserez bien évidemment une autre variable)


__3)__ 🥇 __L'indice de masse corporelle__ est une grandeur permettant d'estimer la corpulence d'une personne.  

__a-__ Après avoir consulté l'[article de Wikipedia](https://fr.wikipedia.org/wiki/Indice_de_masse_corporelle) le présentant proposer un nombre de variables adaptées pour calculer cette grandeur.  
 
__b-__ Testez l'expression établie avec les couples de valeurs suivants correspondant à une partie des [exemples donnés](https://fr.wikipedia.org/wiki/Indice_de_masse_corporelle#Exemples)  

|.....|....|  
|:---:|:--:|  
|1,50 |  50|    
|1,70 | 70 |  
|1,90 |90  | 

__c-__ Quel(s) inconvénient(s) trouvez-vous à utiliser le Shell et les variables précédentes?  
