---
title: "TP n°3 : Les chaînes de caractères"
papersize: a4
geometry: margin=1.5cm
fontsize: 10pt
lang: fr
---


# Exercice n°1 : Evaluations d'expressions  &#x1F3C6;
__A réaliser sans ordinateur dans un premier temps__  
Comment sont évaluées les expressions suivantes par l'interpréteur Python ?   
Si l'évaluation est impossible, expliquez pourquoi.

|Expressions|Evaluation en Python|
|:-----|------|
|'a' + 'b'| |
|'a' + ' ' + 'b'| |
|'a' + "5"| |
|5 + 'a'| |
|'5' + 'a"| |
|2 + 23| |
|"23" + '2'| |
|'4' - '2'| |
|4 * 'I'| |  

# Exercice n°2 : Délimiteurs et échappement  &#x1F3C6;

Ecrivez chacune des séquences suivantes sous forme d'une seule chaîne de caractères  
__a-__ ```L'apostrophe est noté : '```   
__b-__ ```Si on utilise des "guillemets" : attention !```  
__c-__ ```Il l'affirme : "l'apostrophe peut se mélanger avec le guillemet" ```   
__d-__ 🥇 ```Le chemin d'accès au fichier est "C:\Utilisateurs\Public\NTUSER.DAT"```  
_Remarque_ : Le caractère d'échappement `\` peut être utilisé sur lui même.


# Exercice n°3 : Indice de position &#x1F3C6;

__1)__ Définissez une fonction `first_char` renvoyant le premier caractère d'une chaîne passée en paramètre.

__2)__ Définissez une fonction `last_char` renvoyant le dernier caractère d'une chaîne passée en paramètre.


# Exercice n°4 : Extraction d'informations &#x1F3C6;

Une variable `date` référence une date de naissance de type chaîne de caractères sous la forme `jj/mm/aaaa`  

Ecrivez les expressions Python permettant d'extraire :  
* le jour  
* le mois  
* l'année  


# Exercice n°5 : QCM

Indiquez à chaque fois __la bonne réponse__ (l'utilisation de l'ordinateur est interdit):

__Question 1__  
On considère le code Python suivant :
```python
navigateur = "Firefox"
```
A quel caractère correspond `navigateur[2]`?  
* [ ] i  
* [ ] o  
* [ ] r  
* [ ] f  

__Question 2__  
On considère le code Python suivant :
```python
s_1 = "Système "
s_2 = "d'exploitation."
```
Que vaut `len(s_1) + len(s_2)`?  
* [ ] 2  
* [ ] 21  
* [ ] 22  
* [ ] 23  

__Question 3__  
On considère le code Python suivant :  
```python
x = 3
s = ' est un nombre '
q = 'premier'
```
Quelle expression sera évaluée comme `'3 est un nombre premier'`?  
* [ ] x + str(s) + str (q)   
* [ ] x + s + q   
* [ ] int(x) + str(s) + str(q)  
* [ ] str(x) + s + q  


__Question 4__  
On considère le code Python suivant :
```python
definition = "Un octet correspond à 8 bits"
```
Quelle expression permet d'extraire la séquence de caractères `'octet'` ?  

* [ ] definition[4:8]   
* [ ] definition[4:9]   
* [ ] definition[3:7]   
* [ ] definition[3:8]  


# Exercice n°6 : Mise en forme (Exercice BILAN)  &#x1F3C6;&#x1F3C6;

Les données fournies par les utilisateurs à travers de nombreux formulaires présents sur les pages web sont souvent mises en forme pour être stockées ou réutilisées.

![Exemple de formulaire web](./fig/Sample_web_form.png) [^1]

Cet exercice aborde ce problème et doit permettre à terme de réaliser une fonction `name_format` acceptant en paramètre une seule chaîne de caractères représentant le prénom et le nom d'une personne (séparés par un espace) `'prenom nom'` et renvoyant une chaîne mise en forme où seules la première lettre du prénom et toutes les lettres du nom seront en majuscules.

```python
>>> name_format('aLaN TUring')
'Alan TURING'
```

Nous allons décomposer le problème

__1)__ Définissez une fonction `lastname_format` :   
* acceptant un paramètre : un nom sous forme d'une séquence de caractères     
* renvoyant le nom mis en forme (toutes les lettres en majuscule)     

```python
>>> lastname_format('turing')
'TURING'
```

__2)__ Définissez une fonction `firstname_format` :  
* acceptant un paramètre : un prénom sous forme d'une séquence de caractères    
* renvoyant le prénom mis en forme (uniquement la première lettre en majuscule)    

```python
>>> firstname_format('alAN')
'Alan'
```


__3)__ Définissez une fonction `find_firstname` :  
* acceptant un paramètre un nom complet sous forme d'une séquence de caractères    
* renvoyant uniquement le prénom    

```python
>>> find_firstname('aLaN TUring')
'aLaN'
```

__4)__ Définissez une fonction `find_lastname` :  
* acceptant un paramètre un nom complet sous forme d'une séquence de caractères    
* renvoyant uniquement le nom    

```python
>>> find_firstname('aLaN TUring')
'TUring'
```

__5)__ Enfin, utilisez les fonctions précédentes pour définir la fonction `name_format` souhaitée.     

__6)__ 🥇 __Complément__   
 On peut améliorer la fonction `firstname_format` pour tenir compte des prénoms composés.

```python
>>> firstname_format_bis('marie-antoinette')
'Marie-Antoinette'
```

# Exercice n°7 : Tirage aléatoire  &#x1F3C6;&#x1F3C6;&#x1F3C6;

Définissez une fonction `caractere_alea` qui devra renvoyer un caractère au hasard contenu dans la chaîne de caractères passée en seul paramètre.


*** 


__POUR ALLER PLUS LOIN__



# Exercice n°8 : Quelques expressions intéressantes &#x1F3C6;

On considére le code suivant :
```python
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
```
__Evaluez les expressions qui suivent et expliquez leur intérêt__   
__1)__
```python
alphabet[len(alphabet)-1] 
```
__2)__
```python
alphabet[:]
```
__3)__
```python
alphabet[2:22:2]
```
__4)__
```python
alphabet[::-1]
```


[^1]: [Sample web form.png by Cburnett sur Wikipédia anglais](https://commons.wikimedia.org/wiki/File:Sample_web_form.png?uselang=fr)