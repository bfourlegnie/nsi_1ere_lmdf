---
title: "Chapitre 7 : Une structure de donnée construite, les tableaux "
subtitle: "TP : Découverte des listes python"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---  

# Exercice 1 : QCM   &#x1F3C6; 

__Question 1 :__  Quelle instruction crée une liste python vide?   

__a)__ `liste()`    &nbsp;&nbsp;&nbsp;  __b)__ `[ ] `  &nbsp;&nbsp;&nbsp;  __c)__ `print(liste)` &nbsp;&nbsp;&nbsp;  __d)__ `{ }`


__Question 2 :__   Comment sera évaluée l'expression  `[0, 1, 2, 3] + [4]`  ?  

* [ ]  `[4, 5, 6, 7]`       
* [ ]  `[4, 0, 1, 2, 3] `    
* [ ]  `[4, 1, 2, 3] `   
* [ ]  `[0, 1, 2, 3, 4]`        

__Question 3:__  
Quel est le contenu de `liste` après ces deux instructions?     
```python
liste = [0, 2, 4, 6]
liste.append(5)  
```  

__a)__ `[5, 2, 4, 6] `    &nbsp;&nbsp;&nbsp; __b)__ `[5, 0, 2, 4, 6]`  &nbsp;&nbsp;&nbsp;  __c)__ `[0, 2, 4, 6, 5] ` &nbsp;&nbsp;&nbsp;  __d)__ `[5]`  

__Question 4 :__  
Que fait la méthode pop() ?     

* [ ]  Un air de musique   
* [ ]  Elle ajoute un élément au début de la liste.    
* [ ]  Elle retire l'élément le plus à droite dans la liste.  
* [ ]  Elle trie la liste par ordre croissant.      

__Question 5 :__  

Quel est le contenu du tableau `joueurs_98` après les instructions suivantes : 

```python
joueurs_98 = ['Pogba', 'Dugarry', 'Lizarazu']
joueurs_98[0] = 'Zidane'
```  
* [ ]  `['Zidane', 'Pogba', 'Dugarry', 'Lizarazu']`      
* [ ]  `['Zidane']`     
* [ ]  `[Zidane, 'Dugarry', 'Lizarazu']`   
* [ ]  `['Zidane', 'Dugarry', 'Lizarazu']`    




# Exercice 2 : Créations de listes.  &#x1F3C6; &#x1F3C6; 

__1)__ Définir une fonction qui crée la liste [0, 1, 2] et la renvoie. (Exercice très simple !)

__2)__ Définir une fonction avec un paramètre `n` (_de type entier_). La fonction renverra une liste contenant par ordre croissant les n premiers entiers naturels. 
Réaliser quatre versions de cette fonction :    

* l'une sans boucle for   
* une autre avec boucle `for` et par concaténation   
* une autre avec boucle `for` mais sans concaténation et sans utiliser la méthode `append`   
* la dernière avec boucle `for` et `append`    

__3)__ Définir une fonction qui prend comme paramètre un entier `n`, elle renverra une liste contenant n nombres aléatoires compris entre 0 et 10.

__4)__ Définir une procédure `affichage_mois` qui affichera les mois de l'année précédés de leur numéro séparés par le caractère `-`. 
On utilisera une liste contenant les mois de l'année et une boucle for pour faire l'affichage.

``` python
>>> affichage_mois()
>>> 1 : Janvier - 2 : Février - 3 : Mars ...
```
_Remarque_ : La liste des mois sera utilisée comme séquence dans la boucle. On utilisera un accumulateur pour les nombres devant les mois.

__5)__ Définir une fonction qui prend deux paramètres, un entier `n` et une chaine de caractères `parite`.   
Si la chaîne fournie est `'pair'`, alors la fonction renvoie une liste des premiers entiers pairs jusque n.  
Si la chaîne fournie est `'impair'`, alors la foncion renvoie une liste des premiers entiers impairs jusque n.   
Sinon : la liste renvoyée est vide.  

# Exercice 3 : Le bulletin  &#x1F3C6; &#x1F3C6; 

__1)__ Définissez une procédure `affiche_notes(texte, notes)` acceptant deux paramètres :

* `texte` : une chaine de caractères  
  
* `notes` : une liste (ou un nombre)

L'affichage réalisé devra correspondre à l'exemple suivant :

```python
>>> affiche_notes("Voici les notes obtenues", [10, 14, 8])

 Voici les notes obtenues : [10, 14, 8]
```
   
__2)__ Définissez une fonction __entrer_notes__ acceptant comme paramètre `notes` une liste de notes à compléter.
Cette fonction demandera à l'utilisateur de rentrer des notes. Les notes seront ajoutées à la liste `notes`. 
Pour cela, on demandera à chaque fois si l'utilisateur veut entrer une note. Si la réponse est __oui__ on demande la note, on la stocke et on repose la question, si la réponse est __non__ on retourne la liste complétée. Si la réponse est différente de __oui ou non__ on repose la question après avoir précisé que l'entrée était mauvaise.  

Testez votre fonction, comme ceci par exemple :

```python
>>> entrer_notes([10, 14, 8])

    Voulez-vous entrer une note? (répondez par oui ou non)  oui
    Quelle est la note? 14
    Voulez-vous entrer une note? (répondez par oui ou non)  non
 [10, 14, 8, 14]
```

__3)__ Définissez une fonction __calcul_moyenne__ acceptant comme paramètre une liste `notes` et renvoyant la moyenne des éléments de la liste.  

```python
>>> calcul_moyenne([10, 15, 8])
 11.0
```
__4)__  Définissez une fonction __principale()__   dont l'objectif est d'utiliser les fonctions précédentes.
Un affichage final donnant toutes les notes ainsi que leur moyenne devra être réalisé

Compléments :  

- On pourra créer une fonction qui recherche la meilleure note. (On n'utilisera pas une méthode existante.)    

- On pourra chercher la note minimale. (On n'utilisera pas une méthode existante.)   
    
# Exercice 4 : Modifications de listes &#x1F3C6;&#x1F3C6;

_(extraits de : "Numériques et sciences informatiques", Balabonski, Conchon, Filiâtre, Nguyen)_

__1)__ Définissez une fonction `echange(tab, i, j)` qui échange dans le tableau `tab` les éléments aux indices `i` et `j`. 
Cette fonction n'a pas de valeur de retour, mais un __effet de bord__ : elle modifie la liste passée en paramètre.
Pour le vérifier créer d'abord le tableau, puis appeler la fonction et enfin évaluer de nouveau le tableau.

Par exemple :
```python
>>> tab = ['a', 'b', 'c', 'd']
>>> echange (tab, 1, 3)
>>> tab

 ['a', 'd', 'c', 'b']
```


__2)__ Définissez une fonction `miroir(tab)` qui accepte comme paramètre un tableau et le modifie pour échanger le premier élément avec le dernier, le second avec l'avantdernier etc. Vous utiliserez la fonction `echange`



# Exercice 5 : Analyse de code &#x1F3C6;

Tout d'abord, __sans tester avec Thonny__, pour chacun des cas suivants : __expliquer__ l'erreur commise puis __corriger-la__.  
Notez enfin le type d'__Exception levée__ en testant.    

__a)__  

```python
liste = []
liste[0] = 'a'
```

__b)__  

```python
liste = [0, 1, 2, 3]
liste = liste + 4
```


__c)__  

```python
films= ['Un nouvel espoir', "L'Empire contre-attaque", 'Le Retour du Jedi']
for i in range(4):
    print(films[i])
```

__d)__  

```python
films= ['La Menace fantôme', "L'Attaque des clones", 'La Revanche des Sith']
for film in len(films):
    print(film)
```

__e)__  

```python
films= ['Le Réveil de la Force', "Les Derniers Jedi", "L'Ascension de Skywalker"]
print("Bientôt au cinéma :",films[len(films)])
```

\newpage

# Exercice 6 : Mutabilité des listes &#x1F3C6; &#x1F3C6; 
(_Extrait de Eduscol, Ressources pour NSI_)  
__PYTHON TUTOR__ : http://pythontutor.com/visualize.html#mode=edit

__1)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
t = [1,2,3]
s = t
t[0]= 5 
```


__2)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
def truc(t):
    t.append(0)

t = [4]
s = [1]
truc(s)
```

__3)__  Que vaut `s` après ces instructions ? (__Expliquer puis visualiser l'exécution avec python tutor__)

```python
from copy import copy
t=['a', 'b', 'c']
s = copy(t)
del(t[1])
s.pop()
```

__4)__ Que se passera-t-il après ces instructions ? (__Expliquer__)

```python
s = 'abce'
s[3]='d'
```

_Remarque_ : les chaînes de caractères sont __immuables__ (on dit aussi __non mutables__).


# Exercice 7 : Cryptographie  le code de César.  &#x1F3C6;&#x1F3C6;&#x1F3C6;

C'est une méthode ancienne de cryptographie qui consiste à réaliser un décalage constant dans l'ordre alphabétique. Ce mode de cryptographie a été rapidement abandonné car une fois qu'on connait la méthode, le décryptage est très simple. [^1]

![chiffrement par decalage](./fig/caesar.png)

__1)__ Définissez une fonction `creation_alphabet` qui renvoie une liste comportant l'ensemble de l'alphabet dans l'ordre croissant. 

``` python  
>>> creation_alphabet()
 ['a', 'b', 'c', ...]
```

Pour que la création ne soit pas trop fastidieuse, vous utiliserez notamment la fonction `chr`, elle renvoie le caractère correspondant au code Unicode fournit en paramètre.  
Dans le cas des lettres de l'alphabet français ces codes sont compris entre __97__ et __122__

exemple :

``` python  
>>> chr(97)
 'a'
```

En dehors de la fonction, définissez une variable `alphabet` qui stockera la valeur de retour de la fonction créée : cette variable sera utilisée assez souvent par la suite.


__2)__ Définissez une fonction `decalage`  qui prendra en paramètres :

*  un entier `n` qui représente le décalage à effectuer      
*  une liste `alphabet`      
   
La fonction renverra une liste aura subi un décalage de n.
``` python
>>> liste_decalee = decalage(2)
>>> print(liste_decallee)
 ['y', 'z', 'a', 'b', 'c', ...]
```

__AIDES__ :  

* On pourra utiliser les méthodes `pop` et `insert`     

|syntaxe| description|  
|:---:|:---:|  
|`liste.pop()`| renvoie le dernier élément de liste et le supprime de liste|    
|`liste.insert(i, x)`| insère x dans liste à l'index donné par i|  

* __Attention, il faut prendre en compte la mutabilité des listes__.


__3)__ Définissez fonction `demande_mot` sans paramètre qui va demander à l'utilisateur le mot à crypter. 
Stocker la valeur de retour dans la variable __mot__. (On se contentera d'un mot pour le moment.)

__4)__ Définissez une fonction  `demande_cle` qui va demander à l'utilisateur la clé de cryptage c'est à dire ici le décalage souhaité (un entier). 
Stocker la valeur de retour dans la variable __cle__. 

__5)__ Créer une fonction `cryptage` qui prendra trois paramètres :  

* `lettre` :la lettre à crypter    
* `alphabet` : la liste représentant l'alphabet créée en 1)     
* `liste_decalee`      

La valeur de retour sera la nouvelle lettre cryptée correspondant au caractère passé en paramètre. 

_AIDE_ : On pourra utiliser la méthode `index`

|syntaxe| description|  
|:---:|:---:|  
|`liste.index(x)`| renvoie l'indice de la première occurence de x dans liste|    


__6)__ Fonction principale 

* Créer une liste représentant l'alphabet dans l'ordre croissant stockée dans la variable `alphabet`  
* Demander à l'utilisateur la clé de cryptage.      
* Créer une liste décalée en utilisant la clé de cryptage et la variable `alphabet`    
* Demander le mot à l'utilisateur.      
* Crypter une lettre du mot et l'afficher. (On répètera cette opération pour toutes les lettres du mot.)     

[^1]: https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage

