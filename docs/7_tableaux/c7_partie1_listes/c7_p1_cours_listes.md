---
title: "Chapitre 7 : Une structure de donnée construite, les tableaux "
subtitle: "PARTIE 1 Cours - Listes Python"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Une __séquence__ est une collection ordonnée d'objets qui permet d'accéder à ses éléments par leur numéro de position dans la séquence.
La structure de donnée __tableau__ appelée __LISTE__ en Python est un exemple de séquence.  


# La structure de donnée TABLEAU 

## Qu'est-ce qu'un tableau ?

Lorsqu'il y a plusieurs variables à déclarer et que ces variables sont étroitement liées, il est préférable de les regrouper dans une structure de donnée plus complexe  

Ainsi, au lieu d'écrire :  

```python
ville_1 = 'Bordeaux'      
ville_2 = 'Lille'    
ville_3 = 'Paris'  
```

On pourra regrouper ces données dans un __tableau__ :
```python
villes = ['Bordeaux', 'Lille', 'Paris']
```

- On remarque que les trois villes sont regroupées dans une même structure.    
   
- Il n'y a qu'un nom pour accéder à l'ensemble de la collection : `villes`  
    

Cette séquence étant ordonnée, comme pour les chaînes des caractères : on pourra facilement accéder aux éléments du tableau en utilisant leur indice de position respectif.

```python
>>> villes[1]

 'Lille'
```

__📌 Indices de position :__  

> On rappelle que pour une séquence nommée `tableau` les indices de position varient de `0` à `len(tableau) - 1`


## Comment déclarer un tableau  

En général, ce qu'on appelle _tableau_ dans n'importe quel langage de programmation est une collection d'élèments tous de même type et de longueur fixe établie lors de leur création. 

On pourra donc créer des tableaux de plusieurs manière

* __En énumérant toutes les valeurs entre crochets séparées par des virgules__   

```python
>>> notes = [ 8, 10, 12]
```

* __Pour de grands tableaux on pourra utiliser l'opérateur de duplication  `*`__

```python
>>> tableau = [0] * 100
```
Ceci aura pour effet de créer un tableau de longueur 100

```python
>>> len(tableau)

 100
```
Une fois le tableau construit, on peut toujours le _remplir_ avec des valeurs de son choix

```python
>>> tableau[0] = 2
>>> tableau[30] = 26
```

* __Par concaténation de tableaux plus petits__  

```python
>>> tableau_concatene = [1, 2, 3] + [4, 5]
>>> print(tableau_concatene)

 [1, 2, 3, 4, 5]
```


# Les spécificités des tableaux en Python

Le langage Python a développé quelques particularités en ce qui concerne les tableaux :  

- Les tableaux en Python s'appellent des __listes__. 
  
- Le langage Python permet un agrandissement ou un rétrecissement par la _droite_ d'une liste.   

Toutes ces particularités nous montrent que ce que Python appelle des __listes__ ne sont ni des listes (_qui en réalité sont des structures différentes dans d'autres langages_) ni des tableaux :  mais des structures de type construit spécifiques à Python. 

## Déclarer des listes en python : d'autres méthodes

* __Il est possible de créer une liste vide en utilisant les crochets ou encore le constructeur `list()`:__    

```python
>>> liste = []
>>> liste_2 = list()
```

On peut vérifier que le type de ces deux structures :

```python
>>> type(liste)
 <class 'list'>
```
* __On peut également transformer les valeurs fournies par  l'instruction `range` en liste en une seule opération :__  

```python
>>> nombres = list(range(6))
>>> print(nombres)
 [0, 1, 2, 3, 4, 5]
```

## Comment ajouter des éléments à la fin d'une liste Python ?  

__On peut utiliser la méthode `append()` qui ajoute un élément après le dernier.__
  

```python
>>> tableau = [1, 2, 3]
>>> tableau.append(4)
>>> print(tableau)

 [1, 2, 3, 4]
```
On voit dans cet exemple que la valeur 4 a bien été ajoutée comme __dernier élément__ de la liste.  


## Comment retirer des éléments d'une liste?  

* On peut utiliser la méthode `pop()`.  

``` python  
>>> tableau = [0, 1, 2]
>>> tableau.pop()
 
 2

>>> print(tableau)

 [0, 1]
```

__Attention l'utilisation de cette méthode renvoie une valeur (le dernier élément) et en même temps modifie la liste__

- On peut supprimer un ou plusieurs éléments du tableau avec la fonction `del()` si on connait leurs indices.    

``` python
>>> tableau = ['A', 'B', 'C', 'D']
>>> del(tableau[2])
>>> print(tableau)

 ['A', 'B', 'D']
```
Ici, l'élément d'indice 1 dans le tableau a été supprimé.

_Remarque_ : bien d'autres méthodes existent, à vous de chercher....  

https://docs.python.org/fr/3/tutorial/datastructures.html


# La mutabilité des listes.  

## Mutabilité d'une liste Python  

Prenons un exemple dans lequel nous allons créer une liste "tableau" non vide puis nous allons l'affecter à une autre liste "tableaubis":

``` python
>>> tableau = [0, 1, 2, 3, 4, 5]
>>> tableau_bis = tableau
```  

Nous allons vérifier si les variables tableau et tableaubis font référence au même espace mémoire. Pour cela, nous allons utiliser la fonction `id()` qui donnera l'__identifiant mémoire__ des listes.

``` python
>>> print(id(tableau))
>>> print(id(tableau_bis))

 57778904
 57778904
```
Les variables   `tableau` et `tableau_bis` référencent la même liste qui se trouve à l'emplacement mémoire dont l'identifiant est _57778904_.  

Nous allons vérifier que si nous modifions la liste `tableau`, la liste `tableaubis` est aussi modifiée.  

```python
>>> tableau_bis.pop() # on retire le dernier élémet de tableau_bis
>>> print(tableau_bis)
>>> print(tableau)

    [0, 8, 2, 3, 4]
    [0, 8, 2, 3, 4]
```
Tout changement qui sera réalisé sur `tableau` sera réalisé sur `tableau_bis` et inversement car __ils référencent la même liste__. 
__Il faudra donc être très prudent lors d'une copie de liste par simple affectation.__  

## Comment réaliser une copie de liste indépendante ?  

Pour réaliser une copie de liste qui sera affectée sur un autre espace mémoire, on peut importer la bibliothèque `copy`.

```python
>>> from copy import copy
>>> tableau = [0, 1, 2, 3, 4, 5]
>>> tableau_bis = copy(tableau)   
>>> print(id(tableau))
>>> print(id(tableau__bis))

 57778904
 63775343
```

La fonction `copy()` __réalise donc une copie de liste sur un autre emplacement mémoire__ ce qui rend les deux listes indépendantes.