---
title: "Chapitre 7 : Une structure de donnée construite, les tableaux "
subtitle: "TP - Compréhensions de listes et structures imbriquées"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1 : QCM Compréhensions de listes   &#x1F3C6; 

__Question 1 :__  Comment sera évaluée l'expression suivante: `[2 * i + 1 for i in range(5)]` ?  

* [ ]  [3, 5, 7, 9]     

* [ ]  [1, 3, 5, 7, 9]      

* [ ]  [2 * i + 1, 2 * i + 2, 2 * i + 3, 2 * i + 4]    

* [ ]  [1, 3, 5, 7]      


__Question 2:__   

Soit `liste = [-5, 2, 3, 0, -7, 42, 7]`.     
Comment sera évaluée l'expression suivante: `[n for n in liste if n > 0]` ?   

* [ ]  [2, 3, 0, 42, 7]    

* [ ]  [-5, -7]      

* [ ]  [2, 3, 42, 7]   

* [ ]  [False, True, True, False, False, True, True]   


__Question 3:__ 

Parmi les extraits les expressions suivantes laquelle permet de construire la liste `[1, 3, 5, 7, 9]` ?  

* [ ]  [2 * i + 1 for i in range(1, 9)]    

* [ ]  [i for i in range(0, 9, 2)]       
 
* [ ]  [i for i in range(9) if i % 2 == 1]   

* [ ]  [i for i in range(10) if i % 2 == 1]   


__Question 4:__ 

Quelle liste est construite par l'expression `[i % 3 for i in range(7)]` ?  

* [ ]  [0, 1, 2, 0, 1, 2, 0]    

* [ ]  [0, 0, 0, 1, 0, 0, 1]     
   
* [ ]  [0, 0, 0, 1, 0, 0, 0]  
    
* [ ]  [3, 3, 3, 3, 3, 3, 3]    


# Exercice n°2: Premières applications de listes par compréhension.   &#x1F3C6; &#x1F3C6;   

__1)__ Créer une liste par compréhension qui contient  les 20 premiers entiers en commençant par 0.  

__2)__ Créer une liste par compréhension contenant les quotients entiers de la division par 2 des nombres entiers compris entre 0 et 20.

__3)__ Créer une liste par compréhension qui contient tous les multiples entiers de 3 jusqu'à 24 compris. 

__4)__ Ecrire en compréhension la liste des nombres inférieurs à 100 qui sont multiples de 5 ou bien de 7.

__5)__ 🥇 Soit la liste de villes suivantes :

```python
['Paris', 'Bordeaux', 'Lille', 'Lyon', 'Annecy']
```

Ecrire en compréhension la liste des villes précédentes comportant la lettre 'a' (quelque soit la casse.) 


# Exercice n°3: Tracé les courbes représentatives de fonctions.   &#x1F3C6;   &#x1F3C6; 
   
Nous allons découvrir l'utilisation de la bibliothéque `matplotlib` et de son module `pyplot` afin de tracer des courbes représentatives de quelques fonctions mathématiques.

__1)__ Découverte  
   __a-__ Si elle n'est pas installée, ajouter `matplotlib` via le menu `Outils\gérer les paquets` de `Thonny`.    
   __b-__ Testons sur un exemple le tracé d'un graphique.  

```python
import matplotlib.pyplot as plt  # utilisation d'un alias plt

x = [1, 2, 3, 4, 5]  # liste des abscisses
y = [1, 4, 9, 16, 25]  # liste des ordonnées
plt.plot(x, y)  # construction de la courbe (sans affichage)

plt.xlabel("abscisses")  # Nom de l'axe des abscisses
plt.ylabel("ordonnées")  # Nom de l'axe des ordonnées
plt.title("Test")  # titre du graphique
plt.show()  # affiche la figure à l'écran
```

_Remarquez que par défaut, les points ne sont pas marqués par un symble particulier et qu'ils sont reliés. Si vous souhaitez plutôt visualiser un nuage de points, remplacez la ligne `plt.plot(x, y)` par `plt.scatter(x,y)` : __testez-le__._


__2)__  On souhaite obtenir la représentation graphique de la fonction $`f(x) = 2 \times x + 1`$.    
Construisez par compréhension :    

* une liste d'abscisses composés de nombre entiers compris entre 0 et 50   
  
* une liste des ordonnées correspondantes à la fonction  f(x)   

Tracez le graphique correspondant.


__3)__ Réalisez de la même manière la représentation graphique de la fonction $`g(x) = 2 \times x^{2} + 4`$ pour des valeurs de x comprises entre 0 et 10 par pas de 0.5. 

_Remarques :_   

* Il vous est conseillé de __construire les listes par compréhension d'abord__ afin de vérifier votre code.  
  
* Nommez les listes des abscisses et des ordonnées de manière différente `x_f`, `x_g` .... elles seront réutilisées par la suite.
  
* vous ne pourrez pas utiliser un pas de type flottant avec la fonction `range`. Faites autrement.


__3)__ Réalisez la représentation graphique de la fonction $`h(x) = 2 \pi + \sqrt x`$ pour des valeurs de x comprises entre 0 et 100 par pas de 0.1.


__4)__ 🥇 On peut représenter plusieurs courbes sur le même graphique. 

* Il suffit de nommer les listes des abscisses et des ordonnées différemment.  
    
* De plus on peut rajouter une étiquette ('label') à la courbe 

```python
plt.plot(x, y, label="nom de la courbe") 
plt.legend()  # permet l'affichage des étiquettes 
```

* Et changer l'apparence de la courbe (couleur, style, marqueurs pour les points), par exemple :

```python
plt.plot(x, y, "b:o")
 # ici "b:o" signifie que la courbe sera "b" : bleue, 
 # ':' en pointillés, 
 # "o" : avec un marqueur de points en forme de cercle.
```

Pour plus de précision consulter cette [documentation](https://courspython.com/introduction-courbes.html#formats-de-courbes)

__Essayez d'obtenir la représentation ci-dessous__
 
![Graphique à obtenir ](./fig/ex3.png)  

\newpage

# Exercice n°4 : QCM Tableaux à 2 dimensions  &#x1F3C6; 

__Question 1:__     

Soit la liste suivante : `t = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]`.    
Comment accéder à la donnée `14` ?   

* [ ]  `t[1][3]`   

* [ ]  `t[0][3]`      

* [ ]  `t[0][2]`     

* [ ]  `t[14]`    


__Question 2:__  Comment créer une liste qui contient 4 listes de 6 éléments?    

* [ ]  `[0 for j in range(6) for i in range(4)]`       
  
* [ ]  `[[0 for j in range(4)] for i in range(6)]`      

* [ ]  `[[0 for j in range(6)] for i in range(4)]`     

* [ ]  `[[0 for j in range(4)] for i in range(4)]`       



__Question 3:__  (_Extrait de qcm sujet 0 prime_)

Quelle est la valeur de la variable `image` après exécution du programme Python suivant ?        

```python
image = [[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0]]
for i in range(4):
   for j in range(4):
      if (i+j) == 3:
         image[i][j] = 1
```

* [ ]  `[[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]`   

* [ ]  `[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]`      

* [ ]  `[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]`     

* [ ]  `[[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 1, 1, 1]]`  


# Exercice n°5: Vérifier un carré magique &#x1F3C6;&#x1F3C6;
(_Extrait de prépabac NSI, Hatier_)

![carré magique](./fig/carre_magique.jpg)

Un carré d'ordre n est un tableau carré contenant $`n^{2}`$ entiers strictement positifs.  
On dit qu'un carré d'ordre _n_ est magique si :  

* il contient tous les nombres entiers de 1 à $`n^{2}`$;   

* les sommes des nombres de chaque rangée, les sommes des nombres de chaque colonne et les sommes des nombres de chaque diagonale principale sont égales.  

On modélise un carré par une liste de liste de nombres.


__1)__   
__a-__  Quelle est la valeur de `len(carre4)`?   

__b-__ Quelle est la valeur de `carre3[1]`?    

__c-__ Quelle est la valeur de `carre3[0][2]`?    

__d-__ Quelle instruction permet de récupérer la valeur `3` de `carre4` ?    


__2) a-__  On propose le code suivant:

```python
def somme_ligne(carre, n):
   """
   carre est un tableau carré de nombres
   n est un nombre entier
   """
   somme = 0
   for nombre in carre[n]:
      somme = somme + nombre
   return somme
```

Que vaut `somme_ligne(carre4, 2)` ?
A quoi sert cette fonction ?

__b-__ Proposer le code d'une fonction qui prend un carré en paramètre, ainsi que le numéro d'une colonne, et qui renvoie la somme des nombres de cette colonne.

__c-__ 🥇 Ecrire le code d'une fonction qui prend un carré en paramètre et qui vérifie que les sommes des nombres de chaque ligne sont égales.

__d-__ 🥇🥇 POUR ALLER PLUS LOIN :_ Définissez un prédicat `est_magique()` qui prend un carré en paramètre et qui renvoie un booléen indiquant si le carré est bien magique.  



# Exercice n°6 : Création de listes de listes par compréhensions  &#x1F3C6; &#x1F3C6; &#x1F3C6;  

On peut réaliser plusieurs tests dans les listes par compréhension, par exemple :

```python
>>> t = ['*' if i%2==0 else i for i in range(11)]
>>> t

['*', 1, '*', 3, '*', 5, '*', 7, '*', 9, '*']
```

la liste t est une liste des entiers compris entre  0 et 10 inclus affichant :  

* des symboles `*` si les entiers sont pairs  

* les entiers dans le cas contraire  
  
Etudiez bien la structure employée et réutilisez-la dans les exemples qui suivent.

__1)__ Créer une liste par compréhension à deux dimensions 3x3 qui comportent un 0 si la somme de l'indice de ligne et l'indice de colonne est paire sinon un 1.   

```python
[[0, 1, 0], [1, 0, 1], [0, 1, 0]]
```

__2)__ Créer une liste par compréhension qui a pour affichage :    

```
1 0 0 0 0 0 0 0 0 0  
0 1 0 0 0 0 0 0 0 0  
0 0 1 0 0 0 0 0 0 0  
0 0 0 1 0 0 0 0 0 0  
0 0 0 0 1 0 0 0 0 0  
0 0 0 0 0 1 0 0 0 0  
0 0 0 0 0 0 1 0 0 0  
0 0 0 0 0 0 0 1 0 0  
0 0 0 0 0 0 0 0 1 0  
0 0 0 0 0 0 0 0 0 1
```

__On créera une fonction pour créer la liste et on utilisera le cours pour créer une fonction qui affichera cette liste.__


__3)__  Créer une liste par compréhension qui a pour affichage :  

```
1 0 0 0 0 0 0 0 0 1  
0 1 0 0 0 0 0 0 1 0  
0 0 1 0 0 0 0 1 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 1 0 0 0 0 1 0 0  
0 1 0 0 0 0 0 0 1 0  
1 0 0 0 0 0 0 0 0 1  
```

__4)__ Créer une liste par compréhension qui a pour affichage :  

```
1 0 0 0 0 0 0 0 0 0  
1 1 0 0 0 0 0 0 0 0  
1 1 1 0 0 0 0 0 0 0  
1 1 1 1 0 0 0 0 0 0  
1 1 1 1 1 0 0 0 0 0  
1 1 1 1 1 1 0 0 0 0  
1 1 1 1 1 1 1 0 0 0  
1 1 1 1 1 1 1 1 0 0  
1 1 1 1 1 1 1 1 1 0  
1 1 1 1 1 1 1 1 1 1  
```


