---
title: "Chapitre 7 : Une structure de donnée construite, les tableaux "
subtitle: "PARTIE 2 Cours - Compréhensions de listes et structures imbriquées"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Construire des listes Python par compréhension.  

Les compréhensions de listes fournissent un moyen de construire des listes de manière très concise et élégante.

## Premier exemple

Supposons que l'on veuille créer une liste de carrés de nombres, comme :

```python
>>> squares = []
>>> for x in range(10):
      squares.append(x**2)

>>> squares
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

On peut aussi procéder par __compréhension__ comme ceci : 

```python
>>> squares = [x ** 2 for x in range(10)]
```
L'expression `x ** 2` est appliquée à tous les éléments `x` générés par `range(10)` et sont placés dans une liste `[]`.     
Le résultat est équivalent à la première méthode mais avec une forme plus compacte et intuitive.  

## Utilisation de tests 

__📌 Méthode:__  

> Une compréhension de liste consiste à placer entre crochets `[]` une __expression__ suivie par une clause `for` puis par zéro ou plusieurs clauses `for` ou `if`.     
> Le résultat est une nouvelle liste résultat de l'évaluation de l'expression dans le contexte des clauses `for` et `if` qui la suivent.  

Il est possible de rajouter un test à la syntaxe précédente.  
Soit la liste d'entiers suivants : 

```python
entiers = [4, 9, 100, -1]
```
On souhaite construire une nouvelle liste contenant les racines carrés de ces nombres or parmis eux, il existe un nombre négatif. Il va falloir rajouter un __test__ à notre compréhension de liste

```python
>>> from math import sqrt  # on importe la fonction racine carrée
>>> racines_carres = [sqrt(n) for n in entiers if n >= 0]
>>> racines_carres

 [2.0, 3.0, 10.0]
```

Dans l'exemple précédent  :  

* On commence par construire la liste :  `[]`     
  
* On écrit l'expression à évaluer :  `sqrt(n)`    
   
* On parcourt la liste `entiers` éléments par éléments :  `for n in entiers`   
     
* On ajoute un test : `if n >= 0`    


__📝Application 1__ : 

On considère la liste suivante

```python
>>> liste = [1, 4, 2, 7, 1, 9, 0, 3]
```

On ne souhaite garder à partir de celle-ci que les valeurs strictement supérieures à 3. On peut procéder par compréhension.

```python
>>> [i for i in liste if i > 3]

 [4, 7, 9]
```

__📝Application 2__ : 

L'application des listes par compréhension ne se limitent pas aux nombres bien entendus. Imaginons une liste de prénoms mal capitalisés.

```python
>>> prenoms = ['Yann', 'eVA', 'BOB']
```

On peut facilement reconstituer une liste avec les prénoms en minuscules en utilisant la compréhension.

```python
>>> prenoms = [p.lower() for p in prenoms]
>>> prenoms

 ['yann', 'eva', 'bob']
```


# Les tableaux à plusieurs dimensions 

Nous avons pour le moment utilisé des tableaux pour stocker des données de type simple (nombres, chaînes de caractères). Il est également possible de stocker des données de type construit comme d'autres tableaux.

__📝Application__ : 

Nous souhaitons construire un tableau qui contient les notes d'une élève du lycée. Madeline a eu 15 notes pendant l'année à raison de 5 par trimestre.  
Nous souhaiterions mettre ces notes dans un seul tableau mais pouvoir les regrouper par trimestre. Pour cela, il est possible de créer un tableau de tableaux. Nous appellerons cette structure un __tableau à deux dimensions__.

```python
>>> notes = [[10, 12, 14, 8, 13], [11, 16, 9, 17, 6], [15, 18, 16, 14, 7]]
```

## Accés aux éléments de tableaux à deux dimensions

Nous souhaitons accéder à la note `18` grâce aux indices, comment faire ?  
On commence par accéder au bon tableau parmis les 3 présents dans `notes`. Ici c'est le 3ème soit `notes[2]`.  
Puis on accéde à l'élément souhaité dans ce tableau, ici c'est le deuxième soit `notes[2][1]`

```python
>>> notes[2][1]

 18
```

Le tableau précédent a deux dimensions :  la première a une  taille 3, la seconde une taille 5. On pourra donc dire que la taille de `notes` est $`3\times 5`$.


L'accés aux éléments se fera donc par l'expression `notes[i][j]` ou $`0   \le i < 3 \ et \  0 \le j < 5`$

Pour un tel tableau, il est donc possible de le représenter graphiquement comme une grille.

|Représentation du tableau `notes`|   
|:----:|  
|$`\begin{array}{cccccc}&^{0}&^{1}&^{2}&^{3}&^{4} \\ \hline ^{0}& 10 & 12 & 14 & 8 & 13 \\ \hline ^{1}&11 & 16 & 9 & 17 & 6 \\ \hline ^{2}&15 & 18 & 16 & 14 & 7 \\ \hline \end{array}`$|   



Les indices `i` de la première dimension sont représentés verticalement, ceux `j` de la seconde horizontalement

__📝Application__ : on souhaite accéder à la note 13

```python
>>> notes[0][4]

 13
```

## Construction de listes Python à deux dimensions.  

- On peut utiliser une double boucle afin de créer une liste à deux dimensions.
```python
1   liste = []
2   for i in range(3):
3       liste.append([])
4       for j in range(5):
5          liste[i].append(0)
6   print(liste)

   [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
```
Cette méthode est intéressante car nous comprenons qu'à la ligne 1 nous créons une liste vide puis grâce à la première boucle (ligne 2), nous plaçons des listes dans la liste vide.  
La deuxième boucle (ligne 4) remplie ces listes de zéros.  

\newpage

- Il est possible de réaliser la même chose de manière plus compacte avec des listes par compréhension.  

```python
   liste = [[0 for j in range(5)] for i in range(3)]

   [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
```
On commence par ouvrir deux crochets, on définit ensuite le contenu de chaque élément de la liste à deux dimensions puis comme dans l'exemple précédent on réalise une double boucle.   

_Remarque_: Lorsque la liste contient des listes de taille identique, on peut la qualifier de __matrice__. Il est cependant tout à fait possible que la liste contiennent des listes de tailles différentes.



## Comment parcourir une liste à plusieurs dimensions ?

Il est utile de parcourir une liste à plusieurs dimensions pour l'afficher ou pour l'utiliser dans sa globalité.  
Pour cela, on est contraint de réaliser une double boucle.  

On réalise ici l'impression de la liste:  

```python
>>> liste = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]
>>> for i in range(3):
      for j in range(3):
         print(liste[i][j], end=" ")
      print("")

   12 13 14 
   6 16 18 
   9 14 17 
```

_Remarque_ : il est préférable de construire la totalité de  chaîne de caractères avant de l'afficher, le code précédent deviendra ainsi : 


```python
chaine=""
for i in range(3):
   for j in range(3):
      chaine = chaine + str(liste[i][j]) + " "
   chaine = chaine + "\n"   # le caractère \n correspond à un saut de ligne
print(chaine)  # un seul affichage (plus rapide)
```