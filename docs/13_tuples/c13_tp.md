---
title: "Chapitre 13 : les tuples"  
subtitle: "TP "
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Exercice 1 : QCM &#x1F3C6;

__a.__ Pour la fonction suivante, quel code python ne permet pas de renvoyer un 2-uplet (_couple_) contenant a et b ?

```python
def ma_fonction(x) :
    a = x * 2
    b = x * 3
```
* [ ] `return a, b`
* [ ] `return (a, b)`
* [ ] `return (b, a)`
* [ ] `return [a, b]`

__b.__ Soit le tuple défini de la façon suivante :

```python
mon_tuple = (4,5,3,7,8)
```
L'instruction `mon_tuple[2] = 4` :

* [ ] remplace l'élément `2` par `4`
* [ ] léve une exception
* [ ] remplace par `4` l'élément d'indice __2__ 
* [ ] remplace l'élément `5` par `4`

__c.__ Soit la liste suivante : `notes_eleves = [('Marie', 10), ('Lucas', 15), ('Lea', 19), ('Theo', 8)]`

Quel est le type de `notes_eleves` ?

* [ ] int
* [ ] list
* [ ] tuple
* [ ] autre chose

__d.__ Soit la même liste que précédemment. Que vaut l'expression `notes_eleves[2]` ?

* [ ] `'Lea'`
* [ ] `15`
* [ ] `(Lucas, 15)`
* [ ] `('Lea', 19)`

__e.__ Parmi ces expressions laquelle est évaluée à `True`

* [ ] `(1,) == (1)`
* [ ] `[ (1,) ] [0][0] == 1`
* [ ] `[ (1), ] [0][0] == 1`
* [ ] `1 == 1, `



# Exercice 2 : Quelques Fonctions &#x1F3C6;&#x1F3C6;

Pour chacune des fonctions qui suit vous n'oublierez pas de réaliser une __docstring compléte__ et écrirez au moins une __assertion__. 

__1)__ Définir une fonction `somme` qui renvoie la somme des éléments d'un tuple.

_Exemple :_

```python
>>> somme((1, 5, 8, 10))
24
```

__2)__ Définir une fonction `inverse` qui inverse les éléments d'un tuple pour construire un nouveau tuple et le renvoyer.

_Exemple :_

```python
>>> inverse(('a', 2, 3))
(3, 2, 'a')
```

__AIDE__ : On pourra notamment utiliser les fonctions `list` ou `tuple` permettant repectivement de convertir une séquence quelconque en liste ou en tuple (_voir la vidéo de complements de cours pour des exemples_)


__3)__ On souhaite définir un prédicat `is_right_angle` admettant un triplet `t` pour argument, ce triplet contenant la longueur des trois côtés d'un triangle (exprimés dans la même unité).  
Le prédicat devra renvoyé `True` si le triangle est rectangle, `False` dans le cas contraire

_Exemple :_

```python
>>> is_right_angle((6, 8, 10))
True
```

__4)__ 🥇 Définir une fonction `infos_notes` qui accepte une liste de notes en paramètre et qui renvoie un triplet contenant dans l'ordre la moyenne arrondi au dixième, la note la plus basse ainsi que la note la plus haute.

_Exemple :_

```python
>>> infos_notes([15, 12, 10, 8, 17, 11])
(12.2, 17, 8)
```



# Exercice 3 : Coordonnées de points &#x1F3C6;&#x1F3C6;

Pour chacune des fonctions qui suit vous n'oublierez pas de réaliser une __docstring compléte__ et mettrez en place des __doctests__ adaptés.  

__1)__ On considère deux points A et B d'un repère quelconque. Leurs coordonnées sont des tuples à deux éléments.[^1]  
Écrire une fonction `milieu` qui prend en argument les coordonnées de ces deux points et qui renvoie les coordonnées du milieu du segment formé par ces deux points.

__Par exemple :__

```python
>>> A = (3, 12)
>>> B = (-4, 5)
>>> milieu (A, B)
(-0.5, 8.5)
```


__2)__ Etant donné une liste de points dans un plan par exemple, on souhaite trouver le point le point le "plus haut et le plus à droite possible".[^2]
(_Autrement dit : parmi les points les plus à droite, celui qui est le plus haut_).  
Définir une fonction `point_sup_droit` acceptant comme paramètre une liste de tuples correspondant à des coordonnées de points dans un plan.  
Cette fonction renverra le tuple correspondant aux coordonnées du point recherché.

```python
>>> point_sup_droit([(0, 1),(1, 2),(1, 1),(1.2, 3)]) 
(1.2, 3)
```


_Conseil :_
On peut comparer deux tuples : le premier élément du premier tuple sera comparé au premier du second et ainsi de suite .....

```python
>>> (0,1) < (1,2) 
True

>>> (1,1) < (1,2) 
True
```



# Exercice 4 : Synthèse additive RGB 🚀

Définir une fonction `rgb_to_hex` qui renvoie la valeur hexadécimale d'une couleur lorsqu'on lui passe en paramètre un triplet de valeur décimale.

_Exemple :_

```python
>>> rgb_to_hex((255, 0, 255))
'#ff00ff'
```

__Rappel__ : dans le système RGB (ou RVB en français) chaque composante est codé sur un octet : le premier octet correspond à la composante rouge, le second au vert, le dernier au bleu.
Pour un octet les valeurs équivalentes :
* en décimal vont de `0` à `255`
* en hexadécimal vont de `00` à `ff`

__Attention__ : la valeur de retour doit être une chaîne toujours composée de __6 caractères hexadécimaux__ et précédée de __#__

--> Vous n'oublierez pas de réaliser une __docstring compléte__ , écrirez quelques __assertions__ et mettrez en place des __doctests__ adpatés. 


---
[^1]: sur une idée de http://isn.cassin.free.fr
[^2]: sur une idée de http://www.mathly.fr/
