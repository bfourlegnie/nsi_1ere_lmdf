---
title: "Mini - projet : PIERRE FEUILLE CISEAUX"
lang: fr
---

# Présentation du jeu 

> Le jeu de pierre – feuille – ciseaux est un jeu qui se joue à 2.   
> À chaque tour, les joueurs proposent simultanément un objet qui est soit pierre, soit feuille, soit ciseaux.   
> La pierre gagne sur les ciseaux en les cassant (et perd contre la feuille), les ciseaux gagnent sur la feuille en la coupant (et perd contre la pierre), et la feuille gagne sur la pierre en l’enveloppant (et perd contre les ciseaux). 

# Programmation et contraintes

## Représentations choisies en Unicode 

|Point de Code |	U+270A |	U+270B |	U+270C|
|:---:|:---:|:---:|:---:|
|Nom|	RAISED FIST	|RAISED HAND	|VICTORY HAND|
|Glyphe|	✊|	✋	|✌|


## Concevez un programme simulant ce jeu pour 2 joueurs en __n__ manches gagnantes, où vous devrez :  

  - Demandez les noms des joueurs et le nombre de manches gagnantes
  - Tirer aléatoirement pour chacun des 2 joueurs un __code décimal__ correspondant à un des points de code précédents à chaque manche
  - Déterminer le vainqueur de chaque manche 
  - Afficher à chaque manche : le numéro de la manche, le glyphe tiré au sort pour chaque joueur et le vainqueur
  - Un temps de pause correctement choisi devra être utilisé entre chaque manche
  - Déterminer le vainqueur du jeu et l'afficher
  - Affichez en fin de partie les graphiques permettant de visualiser l'évolution du nombre de manches gagnées par chacun des joueurs au cours de la partie.
  - __+__ une amélioration à choisir parmi celles proposées en fin d'énoncé
  
## Structures de données à utiliser obligatoirement :

* __Le résultat de chaque manche__ sera stocké sous la forme d'un triplet `(glyphe1, glyphe2, vainqueur)`
  
_Par exemple :_ 

```python
('✊', '✋', 2)
```

* Une __liste des résultats__ sera complétée progressivement après chaque manche __en regroupant les triplets précédents__

_Par exemple :_ 

```python
[('✋', '✋', 0), ('✊', '✋', 2) , ('✋', '✊', 1), .....]
```
    
* Le __nombre de manches gagnées__ par chacun des 2 joueurs sera stockée sous la forme d'un __couple d'entiers__ et mis à jour à la fin de chaque manche

_Par exemple :_ 

```python
(0, 0) # en début de partie
(0, 1) # après la première victoire du joueur2
(0, 1) # si un match nul a lieu juste après : les valeurs ne changent pas 
(1, 1) # après la première victoire du joueur1
```

* __Une fonction dédiée devra en fin de partie exploiter les données stockées dans la liste des résultats pour afficher les graphiques demandés précédemment__

_Par exemple pour une partie en 50 manches gagnantes_ : 

![Exemples de graphiques à obtenir](./fig/mini_projet_stat.png)

  
* __Toutes les fonctions conçues devront posséder une docstring compléte (Aucun doctest n'est demandé ici).__


* Vous pouvez vous inspirer des projets  déjà réalisés ou des exercices déjà traités pour concevoir votre programme : 

    * Pour la trame aidez vous par exemple du projet [NIM](../nim/mini_projet_1_nim_enconce.md)

    * Pour les graphiques utilisez [matplotlib](../../7_tableaux/c7_partie2_listes/tp2_listes/tp2_listes.md#exercice-n3-trac%C3%A9-les-courbes-repr%C3%A9sentatives-de-fonctions-)


# Amélioration obligatoire

Choisissez une amélioration au moins parmi la liste suivante :


## 1) Faire une version humain contre ordinateur "intelligent"

* Le choix devra être laissé en début de partie 
* Dans ce cas l'humain choisira lui-même le symbole à utiliser
* L'ordinateur pourra utiliser [quelques uns de ces principes](https://www.pierrefeuilleciseaux.fr/wp-content/uploads/sites/5/2013/07/chiffre_infographie_647.jpg) au lieu de tirer aléatoirement au sort un symbole.  


## 2) Proposer une variante pierre-papier-ciseaux-lezard-spock

* [Les régles de cette variante](https://www.pierrefeuilleciseaux.fr/pierre-papier-ciseaux-lezard-spock/)

* il pourra être intéressant d'utiliser un tableau de tableaux pour stocker les conditions de résolution 
    
__Par exemple dans le cas de pierre feuille ciseau la modélisation pourrait être la suivante :__ 
    
|&nbsp;| Pierre  |Feuille | Ciseau |  
|:---:|:---:|:---:|:---:|  
|Pierre| 0 |-1 | 1|  
|Feuille| 1 |0 | -1|  
|Ciseau| -1 |1 | 0|  
    
_Comment lire ce tableau ?_ On regarde la __ligne__ Pierre :  0 signifie match nul, -1 signifie perdu (en effet la pierre perd contre la feuille) et 1 victoire
    
Avec la variante pierre-papier-ciseaux-lezard-spock l'utilisation d'un tel tableau simplifiera grandement l'écriture des conditions de victoire sur une manche
    
![Variante](./fig/pfs_variante.jpg)

## 3) Afficher des graphiques supplémentaires représentant la proportion de chaque élément utilisé par les joueurs 

_Par exemple :_

![Diagramme circulaire](./fig/diag_circulaire.jpg)
    
* Une petite aide pour concevoir des  [diagrammes circulaires](https://python.doctor/page-creer-graphiques-scientifiques-python-apprendre)
* Une petite aide pour afficher [plusieurs graphiques en même temps](./graph_multiples.py)
  
  