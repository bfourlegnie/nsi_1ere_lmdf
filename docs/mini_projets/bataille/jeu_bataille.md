---
title: "Le jeu de BATAILLE"
subtitle : "Mini Projet n°2"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

__Ce travail est individuel__. Le script finalisé doit être remis uniquement par mail à __`patrice.thibaud@ac-lille.fr`__ avant le __16/01/20__.    


# Règles du jeu.  

__Les règles du jeu sont simples:__    
 
* A partir d'un jeu de 52 cartes, on distribue de façon aléatoire 26 cartes à chacun des deux joueurs qui les rassemblent en paquet devant eux. Les cartes sont faces cachées.    

* Chacun tire la carte du dessus de son paquet et la pose face découverte sur la table.    

* Celui qui a la carte la plus forte ramasse les deux cartes et les place à la fin de son paquet.      

* L'as est la plus forte carte, puis le roi, dame, valet, 10, 9, 8, 7, 6, 5, 4, 3 et 2.      

- Lorsque deux joueurs posent en même temps deux cartes de même valeur il y a "bataille". Lorsqu'il y a "bataille" les joueurs tirent la carte suivante et la posent, face cachée, sur la carte précédente. Puis ils tirent une deuxième carte qu'ils posent cette fois-ci face découverte. Ce sont ces deux dernières cartes qui départageront les joueurs. Si il y a encore égalité, on recommence le processus de la bataille.   

* Si un joueur ne peut plus fournir de cartes pour la bataille, il perd la partie   
  
* Sinon, le gagnant est celui qui remporte toutes les cartes   

![Jeu de Bataille](./fig/jeu_bataille.jpg)   



# Matériel disponible  

* L'énoncé ici présent    
  
* Le script `jeu_bataille.py` à télécharger [ici](./jeu_bataille.py)    

# Travail à réaliser

__Vous devrez compléter le script fourni afin d'implémenter en Python un jeu de BATAILLE automatisé__

Réalisez les étapes qui suivent dans l'ordre et vérifier au fur et à mesure votre code.  

## Etude du script fourni

__a)__ Ouvrez avec Thonny le fichier nommé `jeu_bataille.py`.  

__b)__ Observez la structuration du code en plusieurs zones délimitées par des commentaires :   

* Importation des modules.   
 
* Fonction création jeu de cartes.   
  
* Fonctions d'affichage.     
  
* Fonctions relatives aux jeux des joueurs.     
  
* Fonctions relatives aux cartes des joueurs.    
  
* Fonction principale.     
  

__c)__ Les noms des fonctions et des paramètres associés sont imposés, respectez-les! 
Les noms de paramètres réutilisés dans plusieurs fonctions différentes correspondent volontairement aux mêmes données (leurs significations respectives ne seront donc fournies qu'une fois dans l'énoncé à traiter)  

__d)__ Remarquez le mot-clé `pass` utilisé dans chaque fonction. Il permet de réserver un espace pour la définition d'une fonction à venir. Autrement dit, tant qu'aucun code n'est écrit dans une fonction, aucune erreur ne sera levée. __Veillez donc, à n'enlever les mot-clés `pass` qu'au fur et à mesure de votre travail.__  

__e)__ N'oubliez pas d'écrire au fur et à mesure une __docstring__ adaptée à chaque fonction.  

__f)__ La fonction création jeu de cartes vous est donnée. Pour bien comprendre ce qu'elle retourne, vous allez taper le code suivant dans le shell de Thonny.  
```python
>>> jeu_temporaire = jeu_de_cartes()
>>> print(jeu_temporaire[5])

 6 ♠
```
Faîtes plusieurs essais pour bien comprendre la liste retournée par cette fonction.  

## Définitions des procédures d'affichage  

### Affichage des premières cartes des jeux des joueurs:  

Compléter le code de la procédure `afficher_carte` acceptant deux paramètres de type list `jeu1` et `jeu2` (correpondant aux jeux des joueurs).  
Elle affichera les premières cartes des joueurs. Vous testerez votre fonction avec le code suivant:  

```python
>>> jeu_temporaire1 = jeu_de_cartes()
>>> jeu_temporaire2 = jeu_de_cartes()
>>> afficher_carte(jeu_temporaire1, jeu_temporaire2)

 As ♠ As ♠

>>> del(jeu_temporaire2[0])
>>> afficher_carte(jeu_temporaire1, jeu_temporaire2)

 As ♠ 2 ♠
```

### Affichage du nombre de cartes de chaque jeu:  

Compléter le code de la procédure `afficher_nombres_cartes` acceptant quatre paramètres:  
- `jeu1`,  `jeu2`  
- Deux paramètres de type chaine de caractères `nom1` et `nom2` correspondant aux noms des joueurs   

Elle affichera les noms des joueurs avec le nombre de cartes dans leur jeu.  

_Par exemple :_ 

```python
>>> jeu_test_1 = jeu_de_cartes()
>>> jeu_test_2 = ['As ♠', '5 ♥', '2 ♠']
>>> afficher_nombres_cartes(jeu_test_1, jeu_test_2, 'Paul', 'Patrick')

 Paul a un paquet de 52 cartes.
 Patrick a un paquet de 3 cartes.
```

### Affichage du vainqueur du tour:  
On appellera vainqueur du tour, la personne qui gagnera à l'issue de la comparaison des deux cartes ou après une ou plusieurs batailles.

Compléter le code de la procédure `afficher_vainqueur_tour` acceptant trois paramètres:  
- Le paramètre `gagnant` qui est de type chaîne de caractères. (Il pourra prendre trois valeurs 'joueur1', 'joueur2' ou 'bataille'.)    
- `nom1`, `nom2`.    

Elle affichera le nom d'un des joueurs ou 'Bataille' en fonction de la valeur du paramètre `gagnant`.

```python
>>> afficher_vainqueur_tour('joueur1', 'Paul', 'Patrick')

 Paul a gagné le tour.

>>> afficher_vainqueur_tour('bataille', 'Paul', 'Patrick')

 Bataille!
```

### Affichage du vainqueur du jeu:  

Compléter le code de la procédure `afficher_vainqueur_jeu` acceptant cinq paramètres:  
Les paramètres `gagnant`, `jeu1`, `jeu2`, `nom1`, et `nom2` sont identiques aux fonctions précédentes.  

Elle affichera le nom du vainqueur du jeu en fonction de la valeur du paramètre `gagnant`.  

```python
>>> jeu_temporaire1 = jeu_de_cartes()
>>> jeu_temporaire2 = jeu_de_cartes()
>>> afficher_vainqueur_jeu('joueur2', jeu_temporaire1, jeu_temporaire2, 'Paul', 'Patrick')

 Patrick a gagné le jeu.

>>> afficher_vainqueur_jeu('bataille', [], jeu_temporaire2, 'Paul', 'Patrick')

 Patrick a gagné le jeu.
```  

Attention :  __Si un joueur ne peut plus fournir de cartes pendant une bataille, il perd la partie__.


## Définitions des fonctions relatives aux cartes des joueurs.  

### Création d'une fonction qui attribue une valeur à une carte.  

Compléter le code de la fonction `valeur_carte` qui accepte un paramètre `carte` de type chaîne de caractères.  
Cette fonction devra renvoyerr une valeur pour chaque carte. Comme nous l'avons vu dans les fonction précédentes, les cartes sont stockées sous la forme: `Roi ♠`.  
- Il faut donc dans un premier temps récupérer le début de cette chaîne de caractères soit 'Roi'.  
- On donnera ensuite une valeur afin de respecter la 'force' de la carte définie dans les règles du jeu. (On prendra arbitrairement la valeur 14 pour l'As et on décroit de un en un pour les autres cartes.)  

```python
>>> valeur_carte('As ♦')

 14

>>> valeur_carte('9 ♠')

 9
```
### Création d'une fonction qui permet de déterminer le vainqueur ou s'il y a bataille.  

Compléter le code de la fonction `comparer_carte` qui accepte deux paramètres `carte_joueur1` et `carte_joueur2` de type chaîne de caractères.  
Cette fonction devra renvoyer `joueur1` si le joueur 1 a gagné le tour, `joueur2` si c'est joueur 2 qui a gagné ou `bataille` en cas d'égalité.  

```python
>>> comparer_carte('Dame ♠', '7 ♠')

 'joueur1'

>>> comparer_carte('5 ♦', '7 ♠')

 'joueur2'
```  

## Définitions des procédures qui gèrent les jeux des joueurs.  

### Création des jeux des joueurs:  

Compléter le code de la fonction `distribution_jeu` qui accepte deux paramètres:  
- Le paramètre `jeu_complet` qui est de type list.  
- Le paramètre `nombre_cartes` qui est de type entier.  

Cette fonction devra renvoyer une liste de longueur `nombre_cartes` composée de cartes issues de la liste `jeu_complet`.  
On procèdera à un tirage aléatoire pour chaque carte.  
Chaque carte tirée au sort est placée dans la nouvelle liste sera également retirée de `jeu_complet`.

_Par exemple :_

```python
>>> jeu_complet = jeu_de_cartes()
>>> jeu_nouveau = distribution_jeu(jeu_complet, 10)
>>> print(jeu_nouveau)

 ['As ♠', '5 ♥', '2 ♠', '5 ♣', '3 ♣', 'Dame ♠', '7 ♦', '8 ♦', 'As ♣', 'Valet ♣']

>>> len(jeu_complet)

 42
```  
### Réorganiser les paquets de cartes après un tour simple :  

Compléter le code de la procédure `ordonner_jeu` qui accepte deux paramètres `jeu_vainqueur` et `jeu_perdant` de type list.  

Cette procédure devra réorganiser les jeux des joueurs __après la fin d'un tour simple__ (_sans bataille_). Pour les deux joueurs, chacune des premières cartes jouées pendant le tour ne doivent plus être en première position dans les listes respectives.  
Les deux cartes gagnées seront rangées en bout de liste du jeu du vainqueur. La carte perdue par le perdant du tour sera éliminée de son jeu.  

__ATTENTION__ cette procédure ne gére que 2 cartes  ! 

_Par exemple :_

```python
>>> jeu_joueur1 = ['As ♠', '5 ♥', '2 ♠']
>>> jeu_joueur2 = ['Dame ♠', '7 ♦', '8 ♦', 'As ♣', 'Valet ♣']
>>> ordonner_jeu(jeu_joueur1, jeu_joueur2)
>>> print(jeu_joueur1)

 ['5 ♥', '2 ♠', 'Dame ♠', 'As ♠']
   
>>> print(jeu_joueur2)

 ['7 ♦', '8 ♦', 'As ♣', 'Valet ♣']
```
On voit que les premières cartes des listes ne sont plus les mêmes et comme `jeu_joueur1` est le vainqueur, il a bien récupéré les cartes `Dame ♠` et `As ♠` qui ont été placées en fin de liste.  


### Gérer un tour de jeu.  

Compléter le code de la procédure `gestion_tour` qui accepte trois paramètres:  
- Le paramètre `gagnant` qui est du type chaîne de caractères.  
- Les paramètres `jeu_joueur1` et `jeu_joueur2` qui sont de type liste.  

Cette procédure, en fonction de la valeur de `gagnant`, appellera la procédure `ordonner_jeu` ou la procédure `gestion_bataille`. 


### Gérer une bataille.  

Compléter le code de la procédure `gestion_bataille` qui accepte trois paramètres:  
- Les paramètres `jeu_joueur1` et `jeu_joueur2` qui sont de type liste.  
- Le paramètre `gagnant_bataille` qui est de type chaîne de caractères.  Sa valeur initiale sera forcement `'bataille'` mais évoluera dans la procédure pour prendre la valeur `'joueur1'` ou `'joueur2'`.  

Cette procédure devra être capable de gérer une ou plusieurs batailles successives.   
Elle devra aussi être capable d'utiliser la procédure `gestion_tour` lorsque la bataille est terminée  

_Remarques_:

* Il est important de comprendre que tant qu'il y a égalité entre les cartes, la bataille continue.   
* Il ne faut pas oublier qu'il y a des cartes faces cachées lors d'une bataille.
* __Enfin si un joueur ne peut plus fournir de cartes pour la bataille, il perd la partie__.


 


## Définition de la procédure principale du jeu  

La procédure `jeu` est la seule procédure directement appelée par l'utilisateur pour exécuter le jeu. Son rôle est d'appeler les fonctions (procédures) précédentes afin de réaliser une partie compléte de jeu de Bataille entre deux joueurs de manière automatisée.  
Voici l'algorithme écrit en pseudo-code décrivant les instructions réalisées par cette fonction.  

```
1: jeu_complet ← Créer un jeu de carte comportant 52 cartes
2: nom_joueur1 ← Demander le nom du premier joueur  
3: nom_joueur2 ← Demander le nom du deuxième joueur  
4: jeu_joueur1 ← Créer le jeu du premier joueur  
5: jeu_joueur2 ← Créer le jeu du deuxième joueur  
6: TANT QUE un des jeux n'est pas vide:  
7:     vainqueur ← Chercher le vainqueur du tour   
8:     Afficher le nombre de cartes de chaque joueur  
9:     Afficher les deux cartes du tour de jeu  
10:    Afficher le vainqueur du tour  // On laissera un espace suffisant avant le prochain affichage   
11:    Gérer la fin du tour  
12:    Faire une brève pause dans le jeu 
13: fin TANT QUE 
14: Afficher le vainqueur du jeu      
```  

Pour réaliser une brève pause, on pourra utiliser la bibliothèque __time__.  

## Améliorations (2 OBLIGATOIRES)

Proposer au moins deux améliorations à ce jeu.    
Pour chacune d'entre elles vous rélaiserez un descriptif ressemblant à ceux utilisés dans ce sujet.  
Un deuxième script nommé `jeu_bataille_ameliore.py` sera remis, ainsi qu'un fichier `ameliorations.txt` contenant le descriptif.  

### Quelques idées

Vous pouvez envisager 2 améliorations de natures différentes :

* __une sans influence sur la mécanique et la résolution du jeu__ : représentation des cartes avec des bords, introduction au démarrage du jeu avec rappel des régles, amélioration de la phase de bataille avec affichage de toutes les cartes posées et nom du vainqueur de la bataille, modification des pauses pendant les batailles ....

* __une autre ajoutant un plus au jeu__ : modifications de régles avec choix au démarrage (par exemple la taille du paquet) , tracé de graphiques représentant l'évolution du nombre de cartes par joueur au cours de la partie, ....
