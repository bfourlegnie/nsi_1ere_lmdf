---
title: "Chapitre 10 : Les tris"
subtitle : "Cours : Tris par insertion et sélection"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Les algorithmes de tris sont essentiels dans le traitement des données.  
Il est en effet bien plus simple de rechercher un livre dans une bibliothéque déjà triée comme un mot dans un dictionnaire ou encore une valeur dans un tableau.  
Les algorithmes de tris sont nombreux, nous nous intéresseros principalement à deux algorithmes simples : le __tri par insertion__ et le __tri par sélection__.


# Le tri par insertion

## Algorithme

On considère un tableau de nombres `T` contenant `n` élèments.  
On souhaite le trier par ordre croissant des nombres. 

__Le principe de l'algorithme est le suivant :__

* On parcourt le tableau de _i = 1_ à jusqu'au dernier élément( _i= n-1_ ).
* A chaque étape _i_, on considère que les éléments de _0 à i-1_ du tableau sont déjà triés.
* On va alors placer le $`i^{ème}`$ élément à sa bonne place parmi les élèments précédents du tableau, en le faisant "redescendre" jusqu'à atteindre un élément qui lui est inférieur


__En pseudo -code :__


```
Entrée : T, un tableau de n nombres (d'indices allant de 0 à n-1)
Effet de bord : le tableau est trié en place par ordre croissant

1:  n ←  longueur de T
2:  POUR i allant de 1 à n-1 faire :
3:      x ← T[i]        
4:      j ← i            
5:      TANT QUE  j > 0 et T[j-1] > x faire :
6:          T[j] ← T[j - 1]
7:          j ← j-1
8:      fin TANT QUE
9:      T[j] ← x
10: fin POUR
```

\newpage

## Complexité


__📢 Définitions :__  

> Afin d’évaluer la __complexité__ en temps des différents algorithmes de tri on peut compter le nombre de comparaisons et d’échanges de valeur entre deux éléments du tableau.

La complexité du tri par insertion est fortement dépendante de l'ordre du tableau initial.  
Dans le __meilleur des cas__ si le tableau initial est trié, il n'y aura que __n-1__ comparaisons à effectuer.   
Dans le __pire des cas__ (celui où les entiers sont rangés par ordre décroissant) : il faudra réaliser des échanges à chaque comparaison. On peut montrer que le nombre d'échanges varie comme $`n^{2}`$.  

__📢 Définitions :__ 

> On dit que que l'algorithme de tri par insertion est de __complexité quadratique dans le pire des cas__. Le nombre d'opérations nécessaires __évolue proportionnellement avec le carré du nombre de données à traiter__.  
> On dit aussi que le tri par insertion a pour complexité  $`\mathcal{O}(n^{2})`$   
> _remarque_ :  $`\mathcal{O}`$ se lit "grand O".


# Le tri par sélection

## Algorithme

On considère toujours tableau de nombres à trier par ordre croissant des nombres.  

__Le principe de l'algorithme est le suivant :__ 

* On parcourt le tableau du début (_i= 0_) jusqu'à l'avant dernier élément (_n-2_).
* A chaque étape on fixe la valeur minimale comme étant celle à l'indice _i_, puis on parcourt le reste du tableau.
* On cherche dans le reste du tableau la valeur minimale qui doit être aussi plus petite que celle à l'indice __i__
* Si on l'a trouvé : on échange les valeurs contenues à _i_ avec la nouvelle valeur minimale


__En pseudo -code :__

```
Entrée : T, un tableau de n nombres (d'indices allant de 0 à n-1)
Effet de bord : le tableau est trié en place par ordre croissant

1:  n ←  longueur de T
2:  POUR i allant de 0 à n - 2 faire :
3:      min ← i         
4:      POUR j allant de i + 1 à n - 1 faire:           
5:          SI T[j] < T[min] alors :
6:              min ← j
7:      fin POUR
8:      SI T[i] différent T[min] alors :
9:          échanger T[i] et T[min]
10: fin POUR
```


## Complexité

On peut mesurer les temps d'exécution du tri par sélection sur des tableaux de tailles différentes contenant des valeurs aléatoires: [^1]

![Efficacité du tri par sélection](./fig/temps_tri_selec.jpg)

L'évolution du temps d'exécution ne dépend pas cette fois-ci de la nature du tableau à traiter (partiellement trié ou non) car lors d'un tri par sélection on parcourt :

* n éléments le premier tour
* n-1 le second
* etc...

soit $`n + n - 1 + n - 2 + \dots + 2 + 1`$ qui est égal à $`\dfrac{n(n+1)}{2}`$.
Là encore le temps d'exécution varie comme le carré du nombre de données à traiter.

__📢 Définitions :__ 

> L'algorithme de tri par sélection est de __complexité quadratique__ quelque soit le tableau.


# Beaucoup d'autres tris...


Il existe un grand nombre d'autres algorithmes de tris adaptés à des situations diverses : le tableau est déjà partiellement trié ou non, il est de petite taille ou non....
On citera par exemple: le tri à bulles, le tri fusion, le tri rapide, le tri à peigne ....

Pour plus d'informations : http://lwh.free.fr/pages/algo/tri/tri.htm


Python fournit également des fonctions permettant de trier de manière efficace les éléments d'un tableau :

* Soit on souhaite __trier en place__ (comme pour les tris insertion et selection) : on peut alors utiliser la méthode `sort` sur un tableau

```python
>>> tab = [2, 55, 34, 8, 13, 10 , 100]
>>> tab.sort()
>>> tab
[2, 8, 10, 13, 34, 55, 100]
```

* Soit on ne souhaite pas mofifier le tableau initial, on utilisera alors la fonction `sorted` acceptant un tableau en paramètre et renvoyant un _nouveau tableau_ trié :

```python
>>> tab = [2, 55, 34, 8, 13, 10 , 100]
>>> sorted(tab)
[2, 8, 10, 13, 34, 55, 100]
```


[^1]: Numérique et sciences informatiques (_Balabonski, Conchon, Filiâtre, Nguyen_)
