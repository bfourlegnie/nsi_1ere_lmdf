---
title: "Chapitre 10 : Les tris "
subtitle : "TP : le tri par insertion"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Pour ce cours sur les tris nous allons utiliser un __notebook jupyter__ (carnet)

> _De Wikipédia_ :  
> Jupyter est une application web utilisée pour programmer dans plus de 40 langages de programmation, dont: Python.  
>  Jupyter permet de réaliser des calepins ou notebooks), c'est-à-dire des programmes contenant à la fois du texte  et du code 

__1)__ Récupérez tout d'abord l'archive suivante : [tri_insertion.zip](./tri_insertion.zip)  et enregistrez-la. Puis décompressez-la impérativement dans un dossier adapté sur
votre répertoire personnel de travail (disque __H__).

__2)__ Renommez le fichier `tp_tri_insertion.ipynb` présent dans le dossier nouvellement créé en `tp_tri_insertion_<votrenom>.ipynb`  

__3)__ Installer le module __jupyter__ (via Thonny : `Outils\Gérer les paquets`)

__4)__ Toujours via Thonny, ouvrez une __invite de commandes__ : `Outils\Open system shell`)

Un terminal doit alors s'ouvrir (_si ce n'est pas le cas, enregistrez le script courant sous un nom quelconque puis réouvrez le terminal_)

![Invite de commandes](./fig/cmd.jpg)  

Comme pour le Shell de Python, le terminal attend des instructions qui seront validées par l'appui sur la touche `Entrée`.
Le prompt de python `>>>` est remplacé par le chemin menant au répertoire actif. 

__5)__ 

* Placez vous tout d'abord dans le disque où vous avez enregistré le notebook (_H normalement si vous avez suivi les consignes précédentes_)

```bash
h:
```

L'invite de commandes doit devenir 

```bash
H:\>
```

* Puis lancez l'application jupyter

```bash
jupyter notebook
```

Après quelques insants permettant l'initialisation d'un serveur local jupyter sur votre machine une page web s'affichera ressemblant à ceci 

![Serveur jupyter ](./fig/serveur_jupyter.jpg)  

Retrouvez le fichier `tp_tri_insertion_<votrenom>.ipynb`  dans l'arborescence et exécutez-le en cliquant dessus.


__6) Fonctionnement du notebook__

Le carnet est constitué de cellules :

- Certaines contiennent du texte mis en forme, des images, des animations.. 

![cellule texte ](./fig/text_cell.jpg)  

- D'autres sont utilisées comme blocs-notes pour répondre aux questions

![cellule reponse ](./fig/response_cell.jpg)  

- Enfin, les cellules précédées de `Entrée [ ]` sont des cellules de code Python

![cellule python ](./fig/python_cell.jpg) 

  - Toutes les cellules de code font parti du même script, il faudra les exécuter une par une (et chronologiquement) : sélectionnez la cellule puis appuyez simultanément sur `Ctrl + Entrée`  ou sur cliquer sur le bouton `Exécuter`

-   Lorsque le code est exécuté, l'ordre dans lequel les cellules ont été evaluées apparait entre  `[ ]`

![cellule python entrée ](./fig/python_cell_entree.jpg) 

_Attention, parfois vous aurez à réexécuter des cellules précédentes pour mettre à jour votre code !_

