"""
:mod: module tris
:date: fevrier 2020
:auteur: Lycée Marguerite de Flandre - NSI
"""

#--------------------------------------------------------
# Tri INSERTION
#--------------------------------------------------------

def insere(tab, i, x):
    """Insére l'élément x entre les indices 0 et i dans le tableau tab
    
    :param tab: un tableau de nombres
    :type tab: list(float) or list(int)
    :param int i: indice maximal du sous-tableau trié
    :param x: valeur  à insérer dans le sous-tableau
    :type x: int or float
    :return: None
    """
    j = i
    while j > 0 and tab[j - 1] > x :
        tab[j] = tab[j - 1]
        j = j - 1
    tab[j] = x
    

def tri_insertion(tab):
    """Modifie le tableau de nombres passé en paramètre
    de façon à ce que les éléments soit triés par ordre croissant.
    Le tableau est trié en place.
    
    :param tab: un tableau de nombres
    :type tab: list(float) or list(int)
    :return: None
    """
    for i in range(1, len(tab)):
        insere(tab, i, tab[i])
        
#--------------------------------------------------------
# Tri SELECTION
#--------------------------------------------------------
        
def selection(tab, debut):
    """séléctionne l'élément le plus petit dans le tableau tab entre
     l'indice debut et la fin du tableau. Puis echange les valeurs
    
    :param tab: un tableau de nombres
    :type tab: list(float) or list(int)
    :param int debut: indice maximal du sous-tableau trié
    :return: None
    """
    i_min = debut
    for j in range (i_min + 1, len(tab)):
        if tab[j] < tab[i_min] :
            i_min = j
    if i_min != debut:
        tab[debut], tab[i_min] =  tab[i_min], tab[debut]  # échange des valeurs
    

def tri_selection(tab):
    """Modifie le tableau de nombres  passé en paramètre
    de façon à ce que les éléments soit triés par ordre croissant.
    Le tableau est trié en place
    
    :param tab: un tableau de nombres
    :type tab: list(float) or list(int)
    :return: None
    """
    for i in range(len(tab) - 1):
        selection(tab, i)
  
