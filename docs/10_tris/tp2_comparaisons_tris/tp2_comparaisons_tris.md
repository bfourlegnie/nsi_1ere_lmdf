---
title: "Chapitre 10 : les tris"
subtitle: "TP : Etude de différents algorithmes de tris"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n° 1 : Suivi d'un tri par sélection

On fournit un module [`tris.py`](./tris.py)

__1.__ Etudier le code des fonctions `selection` et `tri_selection` fournies
puis compléter le tableau de suivi de l'algorithme ci-dessous pour le tableau en entrée suivant : `[3,7,5,12,8]`

|  contenu du tableau | i | nombre de comparaisons effectués | `i_min` sélectionné | t[i_min] | échange effectué  ?|
|:----------:|:-----------:|:-----------:|:---------------:|:-----------:|:----------------:|
| `[3,7,5,12,8]`|  0  | 4|  0 (_pas de valeur plus petite_) |  3       | pas d'échange  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]    |   |           |                 |             |                  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]   |      |       |                 |             |                  |
|  [ &nbsp;,&nbsp;  ,&nbsp;  ,&nbsp;  , &nbsp; ]  |   |          |                 |             |                  |


__2.__  Combien de comparaisons ont été effectuées ?  
Ce nombre aurait-il été différent si le tableau avait déjà été trié ?

# Exercice n° 2 : Complexité du tri par sélection

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

En supposant que le tri par sélection prend 6,8 secondes pour trier 16000 valeurs, estimer le temps qu'il faudrait pour trier un million de valeurs avec ce même tri.


# Exercice n° 3 : Tri par insertion et tableau décroissant

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

Sans programmer, expliquer simplement ce qui se passe lorsque le tri par insertion est appliqué à un tableau qui se présente en ordre décroissant.


# Exercice n° 4 : Tableau trié ?

_Extrait de Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

Ecrire un predicat `est_trie(t)` permettant de déterminer si le tableau de nombres passé en paramètre est trié par ordre croissant.

# Exercice n° 5 : Tri Timsort

Le tri utilisé par Python avec les fonctions `sorted` est un algorithme de tri concçu en 2002 et nommé [Timsort](https://fr.wikipedia.org/wiki/Timsort).

__a-__ Créez par compréhension une liste de 15000 nombres entiers tous compris entre 0 et 15000.  

__b-__ A l'aide du module timeit comparez le temps d'exécution des tris suivants pour un même tableau :

* selection
* insertion
* Timsort

_Remarque : il sera judicieux de créer des copies indépendantes de la liste précédente_


Rappel sur l'utilisation de `timeit`

```python
from timeit import timeit

timeit(lambda : fonction_a_tester(), number = 1)
```

__Conclure__


