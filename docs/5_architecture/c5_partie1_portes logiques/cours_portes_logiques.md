---
title: "Chapitre 5  Architecture des ordinateurs"  
subtitle: "PARTIE 1 - Cours : Les portes logiques"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---  


# Calcul booléen

## L'algèbre de Boole

C'est le mathématicien britannique __George Boole__ qui a réalisé cette algèbre à l'aube du 20-ème siècle. Boole a noté des similitudes entre le calcul algébrique sur les nombres (comme l'addition et la multiplication), et le calcul en logique propositionnelle (comme avec la disjonction et la conjonction) initiant ainsi l'algébrisation de la logique.

Les __circuits électroniques__ qui constituent un ordinateur s'appuient sur le calcul booléen et sont formés à partir de composants appelés __portes logiques__. 


## Les fonctions booléennes

Comme les fonctions mathématiques que vous avez déjà rencontrées, les fonctions booléennes peuvent s'exprimer de manière symbolique. 

`(non(x) et y) ou (x et z)` est bâtie sur le même modèle que l'expression $`(sin(x) \times y) + (x \times z)`$ à ceci près que x, y et z représentent des booléens.

__📢 Définition :__   

> L'algèbre de Boole associe __0 à faux et 1 à vrai__

__Nous avons déjà vu  les trois premières tables de vérité__ interprétant les fonctions `non(x)`: $`\lnot x`$, `et(x,y)` : $`x \land y`$, `ou(x,y)` : $`x \lor y`$     

![Fonctions non et ou](./img/non_et_ou.jpg)  

Un autre opérateur fréquemment utilisé (même s'il n'est pas élémentaire) est le __OU EXCLUSIF__. Cet opérateur binaire noté $`\oplus`$ est défini par la table de vérité ci-dessous.

|x|y|$`x \oplus y`$|    
|:---:|:---:|:---:|         
|0|0|__0__|       
|0|1|__1__|   
|1|0|__1__|    
|1|1|__0__|  

Il correspond au _ou_ de la carte d'un restaurant, quand il est indiqué dans un menu que l'on peut prendre _fromage ou dessert_


## Expressions booléennes

En utilisant les opérateurs élémentaires précédents, ont peut définir n'importe quelle fonction booléenne et établir une table de vérité adaptée.

__📝Application__ :  

Considérons la fonction multiplexeur  `mux` définie ainsi :  $`mux(x,y,z)=(\lnot x \land y) \lor (x \land z)`$

La table de vérité correspondante est la suivante : 

![Fonctions non et ou](./img/mus.jpg)  

Pour le montrer on peut calculer ligne par ligne en ajoutant autant que de colonnes que de calculs intermédiaires.

|x|y|z|$`\lnot x`$|$`\lnot x \land y`$|$`x \land z`$|$`(\lnot x \land y) \lor (x \land z)`$|       
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|          
|0|0|0|1|0|0|0| 

etc...


# Les portes logiques

## Histoire et conception

__Charles Babbage__, vers 1837, imagina la « machine analytique », assemblage de portes reliées à des roues dentées pour effectuer des opérations logiques.   [^1] 

![Machine analytique non terminée](./img/analytical_machine_babbage.jpg) 
  
Par la suite, les opérations logiques furent effectuées grâce à des relais électromagnétiques utilisés en téléphonie puis par des tubes à vides (composant uniquement électronique).
Le premier ordinateur numérique programmable utilisant initialement 1500 tubes à vides fut le Colossus  Mark 1 en 1943 (puis rapidement après le Mark II plus puissant) utilisé pendant la Seconde Guerre mondiale pour décrypter les communications militaires. 

|Tubes à vide  [^2]| Colossus Mark II  [^3]|   
|:---:|:---:|  
|![Tubes à vide](./img/tubes_a_vide.jpg)|![Colossus 2](./img/Colossus2.jpg)]|  

Le __transitor__ inventé en 1947 devient très fiable et bon marché à partir de 1954 : il remplace avantageusement les tubes à vide notamment en raison de sa petite taille et de sa faible consommation.

|CADET (1955)[^4]| Transitors [^5]|   
|:---:|:---:|  
|![CADET](./img/cadet.jpg)|![Transitors](./img/transistors.jpg)|  
| Un des premiers ordinateurs entièrement transitorisés| Différents modèles de transistors|      


De nos jours les ordinateurs peuvent être décrits à l'échelle microscopique comme un assemblage de transitors. Ces composants électroniques élémentaires se comportent comme des interrupteurs, laissant ou non passer le courant électrique

En combinant plusieurs transistors, on reproduit le comportement des fonctions booléennes étudiées precedemment : les composants électroniques ainsi créés sont appelés __portes logiques__.

La miniaturisation des ces composants a permis l'essor de l'informatique grand public : en 1971, le pprocesseur Intel 4004 contenait 2300 transitors, en 2019 le Threadripper 2990WX en contient 19,2 milliards.


## Les portes logiques NOT, AND et OR  

Les portes logiques sont représentées par des symboles électroniques. 
 
|Fonction logique   | Symbole américain  | Symbole européen |   
|:---:|:---:|:---:|    
|NOT| ![symbole américain](./img/NOT_a.png) | ![symbole européen](./img/NOT_e.png)|   
|AND|  ![symbole américain](./img/AND_a.png) | ![symbole européen](./img/AND_e.png)|  
|OR|  ![symbole américain](./img/OR_a.png)| ![symbole européen](./img/OR_e.png)|  
 

## Quelques autres fonctions et leurs portes logiques

* La fonction __NOR (_NON-OU_)__    

Voilà la table de vérité associée :   

|a|b|$`\lnot`$(a $`\lor`$b)|    
|:---:|:---:|:---:|  
|0|0|1|  
|0|1|0|  
|1|0|0|  
|1|1|0|  

La représentation symbolique de la porte est un mélange des deux symboles OR et NOT :  
  
|Fonction logique | Symbole américain| Symbole européen|   
|:---:|:---:|:---:|  
|NOR|![symbole américain](./img/NOR_a.png)|![symbole européen](./img/NOR_e.png)|   
  
* La fonction __NAND (_NON-ET_)__   

La table de vérité associée :   

|a|b|$`\lnot`$(a $`\land`$b)|  
|:---:|:---:|:---:|
|0|0|1|
|0|1|1|
|1|0|1|
|1|1|0|

La représentation symbolique de la porte est un mélange des deux symboles AND et NOT :   

|Fonction logique   | Symbole américain| Symbole européen|   
|:---:|:---:|:---:|  
|NAND |![symbole américain](./img/NAND_a.png)| ![symbole européen](./img/NAND_e.png)|  



# Circuits combinatoires

En 1958 Jack Kilby travaillant à Texas Instrument crée le tout premier circuit intégré.
Cetype de  composant reproduit une ou plusieurs fontions en utilisant une combinaison de plusieurs portes logiques.

|Premier circuit intégré[^6]|CI 7400 (composé de 4 portes NAND)|    
|:---:|:---:|        
|![Premier circuit intégré](./img/premier_circuit_integre.jpg)|![CI utilisant 4 portes NAND](./img/circuit_integre_7400.jpg)|     


De manière générale, les circuits électroniques possédent plusieurs entrées et plusieurs sorties qui dépendent les uns des autres : on parle de circuits combinatoires.
Ces circuits sont des combinaisons complexes de portes logiques qui ont chacun un rôle précis comme par exemple :

* des additionneurs  
* des comparateurs  
* des decodeurs ( pour sélextionner une sortie à partir de l'état des entrées)  
etc...

[^1]: Machine Analytique de Charles Babbage, exposée au Science Museum de Londres (Mai 2009), Bruno Barral (ByB)  

[^2]: Tubes à vide , Stefan Riepl https://commons.wikimedia.org/wiki/File:Elektronenroehren-auswahl.jpg  

[^3]: A Colossus Mark 2 codebreaking computer being operated by Dorothy Du Boisson (left) and Elsie Booker  

[^4]: http://www.theinfolist.com/

[^5]: Several thru-hole w:transistors, Several thru-hole w:transistors, ArnoldReinhold  

[^6]: http://www.espace-turing.fr/Demonstration-du-premier-circuit.html

Sources :  

* "Informatique et sciences du numerique" par Gilles Dowek  chez Eyrolles ISBN: 978-2-212-13676-0  

* "Numérique et sciences informatiques, 30 leçons avec exercices corrigés" Ellipses Balabonski T. Conchon S. Filliâtre J.C. Nguyen K.  


