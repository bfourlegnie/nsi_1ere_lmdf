---
title: "Chapitre 5  Architecture des ordinateurs"  
subtitle: "TP1 : Portes logiques"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Exercice n° 1 : Ou exclusif &#x1F3C6;

__1)__ En utilisant une table de vérité, vérifier que :  $`a \oplus b = (\lnot a \land b) \lor (a \land \lnot b)`$

__2)__  Définir en Python une fonction `xor(a, b)` qui réalise l'opérateur du même nom sur les booléens.  

__3)__ Appelez cette fonction en nfournissant comme arguments les valeurs `True` et `False` puis essayez avec `1` et `0`. (_En Python, les deux sont équivalents_) et vérifiez les valeurs de retour


# Exercice n° 2 : Table de vérité &#x1F3C6;

__1)__ En utilisant la fonction multiplexeur `mux(x, y, z)` vue en cours, retrouvez la table de vérité correspondante.

__2)__  Définir en Python une fonction `mux(x, y, z)` qui réalise l'opérateur du même nom sur les booléens. 

# Exercice n° 3 : Concevoir une fonction booléenne  &#x1F3C6;  &#x1F3C6;
_Très fortement inspiré de ex n°233, Numérique et Sciences Informatiques (Balabonski, Conchon, Filiâtre, Nguyen)_

__1)__ Etablissez la table de vérité d'une fonction booléenne __f__  valant 1 si deux variables x et y ont la même valeur (qu'elle soit 0 ou 1) ou valant 0 sinon. 

__2)__   En utilisant les opérateur logiques déjà rencontrés, définissez en Python une fonction `f(x, y)` qui réalise l'opérateur du même nom sur les booléens.  


# Exercice n° 4 : Simuler des portes logiques &#x1F3C6;

Vous allez utiliser un simulateur de circuit électronique en ligne : __CircuitVerse__   
 https://circuitverse.org/simulator

Afin de se familiariser avec son fonctionnement, choisir le menu `help` dans la barre des menus et explorer le dossier `Getting Started`  

__1)__ __Porte AND__  

Simulez le fonctionnement d'une porte AND et vérifier la table de vérité correspondante.

__2)__ __Porte NOR__

__a)__ Simulez le fonctionnement d'une porte NOR et vérifier la table de vérité correspondante.

__b)__ Ecrire en Python une fonction `nor()` qui réalise l'opérateur du même nom sur deux booléens.  

__c)__ Dans la pratique la porte NOR est universelle car elle permet de reconstituer toutes les autres fonctions logiques.[^1]

![Universalité des portes NOR](./img_tp/universalite_nor.jpg)

Réalisez ces 3 combinaisons et indiquez pour chacune d'entre elle à quelle porte elle est équivalente.

__3)__ __Porte NAND__

__a)__ Simulez le fonctionnement d'une porte NAND et vérifier la table de vérité correspondante.

__b)__ Ecrire en Python une fonction `nand()` qui réalise l'opérateur du même nom sur deux booléens.

_Remarque, on peut montrer de la même manière que la porte NAND est universelle_


__4)__ __Porte XOR__  

|Fonction logique|Symbole américain| Symbole européen|    
|:---:|:---:|:---:|  
|XOR|![symbole américain](./img_tp/XOR_a.png)|![symbole européen](./img_tp/XOR_e.png)|  

__a)__ Simulez le fonctionnement d'une porte XOR et vérifiez la table de vérité établie en cours.

__b)__ 🥇A partir de la définition de la fonction booléenne OU exclusif vue dans l'exercice 1, construisez un circuit combinatoire équivalent utilisant des portes AND, OR et NOT.

--> Faites une copie d'écran du circuit réalisé et enregistrez-la sous le nom `xor.jpg`

# Exercice n°5 : Le multiplexeur &#x1F3C6; &#x1F3C6;

Le multiplexage est une technique qui consiste à faire passer plusieurs informations à travers un seul support de transmission : cette fonction est utilisée dans les clés USB.  

__Un multiplexeur permet de sélectionner la bonne sortie lors d'une lecture.__

Le __multiplexeur 1 parmi 2__  a deux entrées `e0` et `e1` et un fil de sélection `s0` pour choisir l’une des deux entrées et la mettre en sortie `S`. Il est ainsi représenté :

![multiplexeur](./img_tp/mux_schema_rz.png)

__A l'aide de la fonction `mux(s0,e0,e1)` vue en cours, simuler avec les portes logiques adaptées un multiplexeur et vérifier la cohérence de la table de vérité.__   
_Remarque : dans la fonction vue en cours la variable __x__ joue le rôle du fil de sélection __s0___


--> Faites une copie d'écran du circuit réalisé et enregistrez-la sous le nom `mux.jpg`

# Exercice n°6 : L'additionneur 1 bit &#x1F3C6; &#x1F3C6; &#x1F3C6;

Un circuit additionneur est construit en mettant en cascade des addtionneurs de 1 bit. L'objectif est de réaliser ce circuit de base.

__1)__ _Le demi-aditionneur_   
Un additionneur de 1 bit est lui-même une combinaison de circtuit encore plus simple appelé __demi-additionneur__.  
Ces circuits prennent en entrée deux bits __e0__ et __e1__ et envoie en sortie __s__ la somme e0+e1.  
Si le calcul provoque une retenue, il positionne alors à 1 une autre sortie __c__.     

__a)__ Testez le demi-additionneur grâce au simulateur.  
 https://circuitverse.org/simulator/embed/35135    
 
![demi-additionneur](./img_tp/demi_additionneur.jpeg)  

__b)__ Et complétez la table de vérité suivante      

|__e0__|__e1__|__s__|__c__|    
|:---:|:---:|:---:|:---:|   
|0|0|||    
|0|1|||    
|1|0|||    
|1|1|||  
  
__2)__ _L'additionneur complet_

Pour réaliser un additionneur complet, il faut prendre en compte la retenue éventuelle de l'addition de deux bits précédents ( c'est grâce à cela que l'on pourra "chainer" des additionneurs et ainsi propager la retenue).
Ainsi le deuxième demi additionneur acceptera en entrées :  
* la sortie s1 du premier demi- additioneur  
* une nouvelle entrée c0 (corresponant à la retenue éventuelle se propageant)    
  
En sortie s2 on trouvera : l'addition de c0 et s1

Enfin, les deux retenues c1 et c2 des deux demi-additionneurs seront combinées afin de gnérer une retenue finale c valant 1 si l'une des deux( c1 ou c2) est à 1

__🥇 En utilisant la description précédente réaliser un additionneur 1 bit__, vous vérifierez votre montage à l'aide de la table de vérité suivante :

|__e0__|__e1__|__c0__|__s__|__c__|     
|:---:|:---:|:---:|:---:|:---:|   
|0|0|0|0|0|    
|0|0|1|1|0|   
|0|1|0|1|0|   
|0|1|1|0|1|       
|1|0|0|1|0|    
|1|1|0|0|1|    
|1|1|1|1|1|    


--> Faites une copie d'écran du circuit réalisé et enregistrez-la sous le nom `add1bit.jpg`  


[^1]: http://herve.boeglen.free.fr/Cours%20Logique%20Combinatoire.pdf
