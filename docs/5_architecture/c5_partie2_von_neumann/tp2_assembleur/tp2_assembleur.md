---
title: "Chapitre 5  Architecture des ordinateurs"  
subtitle: "TP2 : Von Neumann et Langage Assembleur"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Exercice 1 : QCM &#x1F3C6;

__Question 1__   Le microprocesseur comprend :  

* [ ] uniquement l’unité arithmétique et logique
* [ ] uniquement l’unité de commande et de contrôle
* [ ] l'UAL, l'UC et la mémoire
* [ ] l'UAL et l'UC
  

__Question 2__   L' UAL :   

* [ ] permet de gérer la mémoire
* [ ] permet de gérer les entrées/sorties
* [ ] est l'endroit où les calculs sont effectués
* [ ] est une adresse mémoire   


__Question 3__   L'accumulateur stocke :    

* [ ] le résultat de l’instruction exécutée par l'UAL
* [ ] l’adresse de l’instruction en cours d'exécution
* [ ] l'instruction en cours d'exécution
* [ ] la prochaine instruction à exécuter



__Question 4__   Quelle affirmation est vraie ? :   

* [ ] la RAM stocke les informations de manière temporaire
* [ ] la ROM peut être lue et modifiée pendant l'exécution de n'importe quel programme
* [ ] Un registre est une mémoire externe
* [ ] Il n'y a aucun registre dans l'UAL



# Exercice 2 : unités de mémoire &#x1F3C6;

Les fabricants expriment généralement les capacités des disques durs de manière décimale . Par contre les sytèmes d'exploitation utilisent toujours des variantes binaires.
Calculez la capacité système d'un disque dur dont la capacité est annoncé à 250 Go par le constructeur.


# Exercice 3 : utilisation de la machine M999  &#x1F3C6; &#x1F3C6;

Soit l'état suivant de la mémoire 

```
       00     1     2     3     4     5     6     7    8      9
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
   0 | 399 | 011 | 112 | 301 | 608 | 402 | 299 | 599 | 412 | 299 |
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
  10 | 599 | 123 | 042 |     |     |     |     |     |     |     |  
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
     |…
     +---
```

Que provoque la mise en route de la machine ?

# Exercice 4 : Soustractions en langages assembleur (M999) &#x1F3C6;&#x1F3C6;  

__1)__ Sur le modèle vu en cours, vous allez réaliser un programme en langage assembleur en utilisant la machine M999 qui va réaliser une soustraction. La première donnée qui va se trouver à l'adresse mémoire 95 à la valeur de 20. La deuxième donnée qui va se trouver à l'adresse mémoire 96 à la valeur 16.   
La première donnée sera chargée dans le registre A et l'autre dans le registre B. 
Le résultat sera affecté à l'adresse mémoire 97, puis on arrêtera le programme.   
__Ecrire le code avec les mnémoniques puis avec le code chiffré dans la machine M999__

__2)__ Réaliser un programme en langage assembleur qui va réaliser une autre soustraction, cette fois-ci avec une condition. 
Si le résultat de la soustraction donne une valeur strictement positive, le programme s'arrête sinon, on inverse les valeurs affectées aux registres A et B, on réalise de nouveau la soustraction puis on arrête le programme.  
__Ecrire le code avec les mnémoniques puis avec le code chiffré dans la machine M999__  


# Exercice 5 : Multiplication en langage assembleur (M999) &#x1F3C6;&#x1F3C6;&#x1F3C6;

En vous aidant de l'algorithme suivant et des programmes déjà réalisés en langage assembleur, réalisez la multiplication de deux entiers stockés dans les mémoires d'adresse 95 et 96. (Vous prendrez les valeurs que vous voulez.)

```
Entrées : x, y des entiers  
Sortie : somme un entier

1: somme ← 0  
2: tant que y > 0 faire 
3:    somme ← somme + x  
4:    y ← y - 1  
5: fin tant que
```  

Les valeurs de `x`, `y`, `somme` et autres variables pourront déjà être affectées dans les mémoires.

