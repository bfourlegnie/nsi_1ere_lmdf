---
title: "Chapitre 5  Architecture des ordinateurs"  
subtitle: "PARTIE 2 - Cours : Modèle de Von Neumann"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---  

Depuis plus de soixante-dix ans, l'architecture des ordinateurs repose sur un modèle dit "de Von Neumann"

# L'architecture von Neumann  

Avant 1945, les premiers ordinateurs n'étaient pas réellement programmables, ils éxécutaient un seul programme câblé dans l’ordinateur (Chaque exécution d’un nouveau programme nécessitait de recâbler l’ordinateur ou de changer une carte, un ruban ...)

![Carte perforée 80 colonnes ( modèle mis au point par IBM en 1928)[^1] ](./img/punched_card.jpg)    


https://www.youtube.com/watch?v=MDQHE0W-qHs


Dans les années 1945, __John Von Neumann__ définit un modèle de machine entierement nouvelle de part sa conception. L'idée est de stocker les données et les instructions grâce à l'électronique à l'intérieur même de la machine.  

![modèle originel de von Neumann](./img/modele-originel2.gif)

L’architecture des ordinateurs à programme enregistré peut se décomposer en quatre types de composants :  

* Une unité de commande (ou contrôle (__UC__))   
  
* Une unité arithmétique et logique (__UAL__)  
  
* Une mémoire    
  
* Des périphériques d’entrées-sorties   
  

Un documentaire d'Arte sur la vie très riche de John von Neumann est disponible :  
https://www.youtube.com/watch?v=c9pL_3tTW2c  

Regarder surtout entre 39:12 et 46:00

## La mémoire 

### La mémoire principale

__Elle contient les instructions du ou des programmes en cours d'exécution et les données associées à ce programme__
Cette mémoire peut être vue comme un tableau de cases mémoires élémentaires appleées __mot mémoire__ (de taille pouvant varier de 8 à 64 bits). __Chaque case possède une adresse unique__ à laquelle on se référe pour accéder à son contenu.  


Cette mémoire se décompose souvent en :  

* `mémoire morte`, une mémoire à lecture seule : elle est chargée de stocker le programme. On la désigne aussi par le nom `ROM` pour _Read Only Memory_  
  
* `mémoire vive`, car c’est une mémoire volatile : le contenu des mots disparait s’il n’y a plus d’alimentation électrique de l’ordinateur. Elle est chargée de stocker les données intermédiaires ou les résultats de calculs. On peut lire ou écrire des données dedans.On la désigne aussi sous le nom de `RAM` pour _Random Access Memory_.  
  
  

Les échanges entre l'UC et la mémoire se font à travers des dispositifs nommés __bus__.  

![Les différents bus](./img/bus.jpg)  
  

### Unités de mesure  

Les unités de mesure des capacités de ces mémoires sont le plus souvent exprimé en multiples d'octet. Historiquement les multiples utilisés en informatique étaient des puissances de 2 : ceci peut engendrer des confusions

| Unité standard | Variante binaire|  
|:---:|:---:|   
| kilooctet 1 ko = $`10^{3}o`$| kibiooctet 1Kio = $`2^{10}o=1024o`$|  
| mégaoctet 1 Mo = $`10^{6}o`$| mébiooctet 1Mio = $`2^{20}o=1048576o`$|  
| giagaoctet 1 Go = $`10^{9}o`$| gibiooctet 1Kio = $`2^{30}o=1073741824o`$|  


## L'unité centrale CPU (Central Processing Unit)

Le CPU (ou plus simplement processeur) regroupe l’__unité de contrôle__ et l’__unité arithmétique et logique__.
L'UC joue le rôle du "chef d'orchestre" : elle lit en mémoire un programme et le transmet à l'UAL qui exécute une séquence d'instructions.

Le processeur est  cadencé au rythme d’une __horloge interne__.
On caractérise le processeur par :

* sa `fréquence d'horloge` exprimée en __Hertz__ (Hz) traduisant le nombre de cycles par secondes   
  
* le nombre d’instructions par secondes qu’il est capable d’exécuter : en __MIPS__ (millions d'instructions par seconde)    
  
* la taille des données qu’il est capable de traiter : en __bits__  

### L'unité de commande

__📢 Définition :__  

> Un __registre__ est une mémoire interne très rapide

L' __UC__  contient trois sous composants :   

* un registre contenant l'adresse de l'instruction courante appelé __compteur de programme__ (__PC__)      
  
* un second registre indiquant l'emplacement mémoire de la prochaine instruction à exécuter     
  
* un séquenceur contrôlant tous les mouvements de données de la mémoire vers l'UAL ou les périphériques d'entrées-sorties   
  
### L'unité arithmétique et logique

L'__UAL__ est une unité de traitement composée :  

* de plusieurs registres de données   
  
* d'un registre spécial appelé __accumulateur__ dans lequel vont s'effectuer tous les calculs  
  
* de circuits électroniques rélalisant les opérations arithmétiques, logiques, de comparaisons et...  
  

![Schéma de l'unité arithmétique et logique](./img/ual.jpg)



## Les dispositifs d'entrées-sortie

Il existe un très grand nombre de périphériques (écrans, claviers, souris, imprimantes, disques durs, cartes réseaux, etc..)

Ces composants sont connectés à l'ordinateur par des circuits électroniques appelés __ports__.  
 

## Qu’en est-il aujourd’hui ?

Plus de soixante ans après son invention, le modèle d’architecture de von Neumann régit toujours l’architecture des ordinateurs. Par rapport au schéma initial, on peut noter deux évolutions.

![modèle actuel](./img/modele-actuel.gif)


Les entrées-sorties, initialement commandées par l’unité centrale, sont depuis le début des années 1960 sous le contrôle de processeurs autonomes . 
   
Les ordinateurs comportent maintenant des **processeurs multiples**, qu’il s’agisse d’unités séparées ou de « cœurs » multiples à l’intérieur d’une même puce. Cette organisation permet d’atteindre une puissance globale de calcul élevée sans augmenter la vitesse des processeurs individuels, limitée par les capacités d’évacuation de la chaleur dans des circuits de plus en plus denses.


# Programmer le processeur 

## Le langage assembleur  


![chaîne de compilation](./img/compilation.jpg)

Un programme écrit dans un __langage de haut niveau__ (comme Python par exemple) est proche du langage naturel: il dépend le moins possible de la nature du processeur et du système d'exploitation. Ces programmes seront toujours traduits dans un __langage de plus bas niveau__  avant d'être exécuté par le microprocesseur dans un langage machine (en binaire). 


Chaque processeur posséde un __langage assembleur__ : par exemple la documentation relative au INTEL core I5 regroupe 2,5 millions de mots permettant de réaliser différentes instructions. Il est parfois nécessaire d'écrire des "morceaux" de programmes directement en langage assembleur quand par exemple un caclul précis doit être réalisé le plus vite possible.


## La machine débranchée M999

[Machine débranchée M999a](./pdf/M999a.pdf) : _Voir Annexe_

La machine M999 est une architecture qui comporte 100 mots mémoires de 3 chiffres emplacements mémoire adressables par des adresses codées sur 2 chiffres. Cette mémoire va contenir données et instructions.  
Deux registres A et B  forment l'entrée de l'UAL.  
Un accumulateur R reçoit le résultat de l'UAL.  
Le processeur dispose aussi d'un registre compteur de programme __PC__ contenant l'adresse mémoire de la prochaine instruction à exécuter.  


###  Jeu d'instructions 

Pour écrire un programme en langage assembleur, on utilise des mots appelés __mnémoniques__. 

* [Jeu d'instructions M999 et Registres](./pdf/jeu_instructions_registres_m999a.pdf) : _Voir Annexe_  

Pour comprendre comment on code en langage assembleur, nous allons commenter certaines instructions :   

- **LDA 12** se traduira d'après le tableau par **012** (0 est le code attribué à LDA) ce qui veut dire charger la valeur contenue dans l'adresse mémoire 12 dans le registre A.  

- **JMP 14** se traduira par **514**. Cela consiste à réaliser un saut de l'adresse où l'on se trouve jusqu'à l'adresse 14.

- On suppose que le registre A contient la valeur 5 et le registre B la valeur 8. L'instruction **ADD 00** se traduira par **300**. Cela va réaliser l'addition du contenu de A et B et le mettre dans R. Donc R contiendra la valeur 13.


### Un exemple de fonctionnement :  [Diaporama commenté](./exemple_utilisation_m999/exemple_m999.pdf)


[^1]: Par Mutatis mutandis — Travail personnel, CC BY 2.5, https://commons.wikimedia.org/w/index.php?curid=1406914

Sources :  
 
* https://interstices.info/le-modele-darchitecture-de-von-neumann/  
  
* Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)  
  
* M999, le processeur débranché, Martin Quinson, Philippe Marquet
github.com/InfoSansOrdi/M999  

