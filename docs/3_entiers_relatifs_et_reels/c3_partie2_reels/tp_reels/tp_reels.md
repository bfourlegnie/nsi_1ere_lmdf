---
title: "Chapitre 3 Représentations des entiers relatifs et des réels"
subtitle : "TP 2 : Les nombres réels"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1: Représentations de nombres dyadiques &#x1F3C6; 
  
Convertir en binaire les nombres dyadiques suivants

__a)__  0,25

__b)__  1,75

__c)__  $`\dfrac{101}{8}`$


# Exercice n°2: Représentations de nombres réels  &#x1F3C6;  &#x1F3C6;
  
Convertir en binaire les nombres réels suivants avec une précision de 8 chiffres après la virgule en base 2

__a)__  $`\dfrac{11}{15}`$ (On calculera d'abord la division et on prend trois chiffres après la virgule.)

__b)__  12,458

__c)__  $`\dfrac{1}{3}`$


# Exercice n°3: Egalité de flottants  &#x1F3C6;

__1)__ Donner la représentation de __0,1__ en binaire.

__2)__ Donner la représentation de __0,2__ en binaire.

__3)__ Calculer la somme des deux réels en base 10 et en binaire.
Que pouvez-vous en conclure?

__4)__ Sachant que les ordinateurs utilisent le binaire, expliquez pourquoi on ne peut comparer deux réels?


# Exercice n°4 : Patriot Missile  &#x1F3C6;&#x1F3C6; &#x1F3C6; 
(__Exercice 3 à faire avant !__)

![Lancement d'un missile Patriot](./fig/Patriot_missile_launch_b.jpg)[^1]

Le 25 février 1991, durant la première guerre du Golfe, une batterie de missiles Patriot basée à Dharan, en Arabie Saoudite, échoua à suivre et intercepter un missile Scud irakien en approche. Le missile Scud frappa un barraquement américain, tuant 28 soldats et blessa environ 100 autres personnes.  
Le rapport du commandement général, GAO/OMTEC-92-26, détailla l'échec de la manière suivante : un problème logiciel a conduit à une défaillance du système. Il est apparu que la cause était un mauvais calcul du temps écoulé depuis le démarrage du missile dû à des erreurs d'arrondies numériques.  
Plus spécifiquement, le temps qui était mesuré en dixième de secondes par l'horloge interne, était ensuite multiplié par la valeur 10 pour avoir le résultat en secondes. Ce calcul était réalisé avec des __nombres à virgule fixe ayant 24 chiffres après la virgule__.

__1)__   
__a-__ En utilisant le travail réalisé dans l'exercice 6 donner la représentation d'un dixième (de seconde) en binaire avec 24 chiffres après la virgule.  

__b-__ 🥇 En déduire la différence (décimale) entre la valeur réelle à représenter et la représentation trouvée(N'arrondissez pas les calculs intermédiaires)

__2)__ En raison de l'approximation précédente et de l'erreur sytématique sur le dernier chiffre stocké, on peut estimer l'erreur commise lors de l'approximation de 0.1 comme étant de $`\bf9.54\times10^{-8} s`$.    
La batterie de missiles étant allumée depuis 100 heures quelle erreur de temps a été commise ?  

__3)__ Sachant qu'un Scud se déplace à une vitesse de croisière de Mach5 soit environ 1702 m/s, de combien s'est déplacé le Scud pendant l'erreur précédente ?  

# Exercice n°5 : Représentation et types &#x1F3C6;  

Soit la représentation suivante $`(10010111)_{2}`$.  
__a)__ Quelle information manque-t-il pour pouvoir l'utiliser ?  
__b)__ Donner deux nombres entiers pouvant avoir cette représentation.    


[^1]: [Missile Patriot](redstone.army.mil) (domaine public)
