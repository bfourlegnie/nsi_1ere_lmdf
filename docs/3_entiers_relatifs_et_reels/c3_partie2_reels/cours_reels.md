---
title: "Chapitre 3 Représentations des entiers relatifs et des réels"
subtitle : "PARTIE 2, Cours :  Les nombres réels"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


Les réels peuvent être des __flottants__ (_ex :_  0,125) , des nombres __rationnels__ ( _ex :_ $`\dfrac{1}{3}`$ ) ou des __irrationnels__ ( _ex :_ $`\pi`$ ) ...

Le codage en machine de ces nombres est limité par l'étendue des valeurs possibles (en fonction du nombre de bits utilisés) mais aussi comme nous allons le voir par la __précision de la représentation__.

# Représentation en binaire des nombres réels

## Cas des nombres dyadiques

Par analogie avec les nombre décimaux, il existe des nombres flottants en binaire.

__📢 Définition :__

> On appelle __nombre dyadique__ tout nombre pouvant s'écrire sous la forme $`\dfrac{x}{2^{n}}`$, avec $`x \in \mathbb{Z}`$ et $`n \in \mathbb{N}`$.</div>

On souhaite représenter en binaire la fraction :  $`\dfrac{11}{16}`$ ce nombre est  __dyadique__ car    $`\dfrac{11}{16} = \dfrac{11}{2^{4}}`$  

Dès lors on peut comme pour un nombre décimal procéder à un développement de la fraction  :  

* le numérateur peut s'exprimer ainsi : $`11=8+2+1=2^{3}+2^{1}+2^{0}`$  

* d'où : $`\dfrac{11}{16} = \dfrac{2^{3} +2^{1}+2^{0}}{2^{4}} = 2^{-1} +2^{-3}+2^{-4}`$  

Comme pour un nombre décimal, on peut exprimer un nombre flottant en binaire :

$`\dfrac{11}{16} = \underbrace{\boxed 0}_{\text{partie  entière}}\times2^{0} +\underbrace{\boxed 1\times2^{-1}+\boxed 0\times2^{-2}+\boxed 1\times2^{-3}+\boxed 1\times2^{-4}}_{\text{partie  fractionnaire}}= \bf(0.1011)_{2}`$ 



On voit qu'il est facile dans notre exemple de convertir un dyadique en binaire sans perte d'information. Comment cela se passe-t-il dans les autres cas ? 


## Cas général  

Prenons l'exemple du nombre __48,195__ comportant une partie entière __48__ et une partie fractionnaire __0,195__

- Nous avons déjà vu comment convertir un entier naturel signé en binaire (_sur 8 bits par exemple_) :  $`48 = (0011 0000)_{2}`$

- Nous allons voir comment dans un cas quelconque représenter la __partie fractionnaire__.
  
__📌Méthode:__     

Tant que la précision souhaitée n'a pas été atteinte :  

* On multiplie par deux la partie fractionnaire    
   * si le résultat obtenu est supérieur à 1, on conserve un __1__  
   * si le résultat est inférieur à 1, on conserve un __0__  
* La nouvelle partie fractionnaire correspond à la partie fractionnaire (et uniquement elle)  du résultat.    
  
 __La succession de 1 et de 0 conservés correspondra à la représentation binaire de la partie fractionnaire__  


__📝Application__ :  
Représentation de  la partie fractionnaire de 48,195 avec une précision de 10 chiffres après la virgule.

|étapes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|test de la valeur |chiffre conservé|Puissance de 2 correspondante|  
|-----|:------:|:---:|----|  
|$`0.195\times2=0.390`$|<1|__0__|$`2^{-1}`$|  
|$`0.390\times2=0.780`$|<1|__0__|$`2^{-2}`$|  
|$`0.780\times2=1.560`$|>1|__1__|$`2^{-3}`$|  
|$`0.560\times2=1.120`$|>1|__1__|$`2^{-4}`$|  
|$`0.120\times2=0.240`$|<1|__0__|$`2^{-5}`$|  
|$`0.240\times2=0.480`$|<1|__0__|$`2^{-6}`$|  
|$`0.480\times2=0.960`$|<1|__0__|$`2^{-7}`$|  
|$`0.960\times2=1.920`$|>1|__1__|$`2^{-8}`$|  
|$`0.920\times2=1.840`$|>1|__1__|$`2^{-9}`$|  
|$`0.840\times2=1.680`$|>1|__1__|$`2^{-10}`$|  

On peut bien évidemment continuer le processus...

Ainsi on peut en déduire qu'avec 10 chiffres de précisions 0.195 est décomposé ainsi :  
$`0\times2^{-1}+0\times2^{-2}+1\times2^{-3}+1\times2^{-4}+ 0\times2^{-5}+0\times2^{-6}+0\times2^{-7}+1\times2^{-8}+1\times2^{-9}+1\times2^{-10}`$  soit $`\bf0.194`$  !
  
En faisant le calcul précédent on arrive seulement à une **approximation** de la partie fractionnaire qu'on souhaite représenter et __ce sera le cas pour la plupart des nombres flottants__

Ainsi en machine avec une précision de 10 chiffres :
$`0,195 \text{ sera représenté par }  (0.0011000111)_{2}`$

Et donc avec 8 bits pour la partie entière le nombre 48,195 sera représenté ainsi : $`(\underbrace{00110000}_{\text{partie entière}}.\underbrace{0011000111}_{\text{partie fractionnaire}})_{2}`$

# 💾 Comparaison entre deux nombres flottants

## Premier exemple

Nous faisons simplement une comparaison entre deux nombres flottants:  
Par exemple 0.1 + 0.2 est-il égal à 0.3 ? 
    En théorie oui mais nous allons tester dans la console Python.

```python
>>> (0.1 + 0.2) == 0.3
False
```

Nous allons essayer de comprendre pourquoi cette comparaison est fausse. Pour cela, nous allons utiliser une bibliothèque qui va nous afficher un grand nombre de décimales du nombre flottant (_celles stockées en mémoire_).

```python
>>> from decimal import Decimal
>>> Decimal(0.1 + 0.2)
Decimal('0.3000000000000000444089209850062616169452667236328125')
```

```python
>>> Decimal(0.3)
Decimal('0.299999999999999988897769753748434595763683319091796875')
```

Nous comprenons maintenant pourquoi cette comparaison n'est pas vérifiée.

## Deuxième exemple

Nous faisons maintenant une deuxième comparaison mais cette fois ci avec des nombres dyadiques.  
Par exemple $`\dfrac{5}{16} + \dfrac{3}{16}`$ est-il égal à $`\dfrac{8}{16}`$ soit $`0.5`$ ?

```python
>>> (5/16 + 3/16) == 8/16
True
```

```python
>>> Decimal(5/16 + 3/16)
Decimal('0.5')
```

```python
>>> Decimal(0.5)
Decimal('0.5')
```
On voit ici que la conversion se fait de façon exact puisque la bibliothèque nous indique qu'il n'y a pas de décimale après le 5 de 0.5.

__Conclusion__
Comme nous l'avons vu, nous approximons la conversion de la partie décimale dans certains cas.    
* __Les calculs sur les flottants ne sont donc pas exacts en Python__ (comme dans les autres langages)  
* Il ne faut pas se fier à l'affichage d'un nombre qui ne représente pas toutes les décimales stockées  
* Il est donc impératif en programmation de __ne pas comparer des réels entre eux__ : le résultat sera alors aléatoire.

# Pour aller plus loin : La norme IEEE-754


On sait que les nombres décimaux peuvent être représentés à l'aide d'une __écriture scientifique__ de la forme $`\bf a\times10^{n}`$ avec $`1\leq a<10`$ et $`n\in \mathbb{Z}`$  
Par analogie, on peut représenter les réels en base 2 comme   $`\bf (-1)^{S}\times M \times 2^{e}`$  avec :   
* S : le signe   
* M : la mantisse  
* e : l'exposant  
  
Par exemple : $`\bf48,195 = (-1)^{0}\times 1,506094 \times 2^{5}`$

La norme IEEE-754 utilisée très couramment dans le domaine informatique fixe pour la taille d'un mot le nombre de bits réservés pour la mantisse et l'exposant.  
Pour le stockage des nombres réels, la taille des mots est de 32 bits (simple précision) ou 64 bits (double précision).    

![Titre](./fig/format_simple_double.png)  


Pour plus de précision sur la manière de déterminer __M__ et __e__ : https://fr.wikipedia.org/wiki/IEEE_754


# __Une même représentation : plusieurs types de données__

On l'a vu,  la manière de coder un nombre peut être très différente en fonction des conventions utilisés:   
* codage des entiers naturels sur n bits ou de manière arbitraire (comme en Python)    
* codage des entiers signés sur n bits grâce au complément à deux   
* codage des réels   

Par exemple la représentation sur 8 bits suivante $`(11010111)_{2}`$ correspond :  
* soit à  $`215`$   
* soit à  $`-41`$   

Ou encore la représentation sur 32 bits $`(0 01010111 01011010001101101101100)_{2}`$ peut correspondre à  :  
* $`732765036`$ si c'est un entier codé en complément à deux  
* $`1.23e-12`$ si c'est un réel en format simple précision  

Certains langages de programmation contrairement à Python nécessitent de spécifier le type exact d'une variable numérique.

A titre d'exemple quelques types représentant tous des entiers

|type | langage  | intervalles de nombres correspondants |  
|:---:|:---:|:---:|  
| __int__ | Python  | pas de limite supérieure|   
| __int__ | Java  | entre $`-2^{31}`$ et $`2^{31} -1`$ |   
| __long__ | Java  | entre $`-2^{63}`$ et $`2^{63} -1`$|   
| __sign char__ | C  | entre $`-2^{7} + 1`$ et $`2^{7} -1`$|    
| __int__ | C  | entre $`-2^{15}`$ et $`2^{15} -1`$|   
