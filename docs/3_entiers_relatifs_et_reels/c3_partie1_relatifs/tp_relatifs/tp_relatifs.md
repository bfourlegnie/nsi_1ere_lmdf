---
title: "Chapitre 3 Représentations des entiers relatifs et des réels"
subtitle : "TP 1 : Opérations binaires et entiers signés"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice n°1 : addition binaire  &#x1F3C6;

__1)__ Convertir les nombres $`54_{10}`$ et $`38_{10}`$ en base 2.  

__2)__ Poser puis réaliser l'addition de ces deux nombres binaires.  

__3)__ Vérifier que la valeur correspond bien à 92  

# Exercice n°2: Multiplication binaire &#x1F3C6;

Comme pour l'addition les règles de la multiplication binaire sont très simples :   
$`0 \times 0 = 0`$   
$`0 \times 1 = 0`$   
$`1\times 0 = 0`$   
$`1 \times 1 = 1`$   

Poser chacune des multiplications binaires suivantes et réalisez-les(vérifiez à la fin la cohérence de vos réponses)  
__a-__ $`(110)_{2} \times (1)_{2}`$  

__b-__ $`(101)_{2} \times (10)_{2}`$  

__c-__ $`(111)_{2} \times (101)_{2}`$  


# Exercice n°3 : Complément à deux &#x1F3C6;  

__1)__ Donner le complément à deux représenté sur 8 bits de chacun des entiers suivants __en utilisant à chaque fois les deux méthodes vues en cours__  :  6 ; 44 ; -15 ; -20;  -74 ; 

Vous pouvez vérifier vos résultats :   

* soit à l'aide d'un convertisseur en ligne comme par exemple :  
  
https://www.exploringbinary.com/twos-complement-converter  

* soit à l'aide du module numpy à l'aide de la méthode :  
`binary_repr(nombre,width=nbre de bits)`   

```python
>>> import numpy as np
>>> np.binary_repr(-8,width=8)
'11111000'
```

__2)__ En utilisant les représentations précédentes, réalisez les additions suivantes et vérifiez les résultats :   
__a)__ 44 -15 = 29    
__b)__ -20 -15 = -35  
__c)__ 6 - 74 = -68    

Là encore vérifiez vos résultats à l'aide du convertisseur en ligne.  

# Exercice n°4 : Codage des entiers relatifs en machine &#x1F3C6; &#x1F3C6;

__1)__ On considère un codage en complément à deux sur __16 bits__   
    __a)__ Donner l'intervalle de nombre entiers relatifs pouvant être représentés avec ce codage.   
    __b)__ Donner les représentations binaires du minimum et du maximum.    

__2)__ Que se passe t-il si on rajoute 1 à la valeur maximale ? (Poser l'addition en binaire) 

__3)__ On peut vérifier nos réponses à l'aide du module numpy comme on l'a précédemment rencontré.

Importer le module ainsi
```python
>>> import numpy as np
```

La syntaxe `as np` permetttra d'appeler le module avec un nom "plus court " (`np` au lieu de `numpy`)

Le module numpy permet de travailler avec des entiers de taille fixée ici `numpy.int16()` impose un codage d'entiers signés  sur 16 bits  

__a-__ Vérifions les caractéristiques de cette représentation:

```python
>>> np.iinfo(np.int16)
```
Retrouvez l'intervalle de nombre entiers de la question __1)__

__b-__ Ajoutez 1 à la valeur maximale en complétant le code ci-dessous et vérifiez la question 2).

```python
>>> np.int16(....+1)
```

