---
title: "Chapitre 3 Représentation des entiers relatifs et des réels"
subtitle : "PARTIE 1 - Cours : Opérations binaires et entiers signés"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


#  ADDITION BINAIRE  

Dans le cas particulier de la base 2, seuls les chiffres 0 et 1 sont utilisés, ce qui rend les opérations mathématiques de base particulièrement simples à réaliser.  
En effet, les règles de calcul apprises à l’école primaire en base 10 (addition, soustraction, multiplication, division) se généralisent en base 2.

__📐  Point mathématique : régles d'addition en binaire__     

> __0 + 0 = 0__

> __1 + 0 =  1__

> __0 + 1 = 1__

> __1 + 1 = 10__   :  le 1 du nombre $`(10)_{2}`$ sera considéré comme une **retenue** ou **report**.  
 


__📝Application__ :  

On souhaite additionner les nombres binaires $`(1101)_{2}`$ et  $`(101)_{2}`$

![Addition binaire](./fig/Addition_binaire.gif)  

Donc : $`(1101)_{2} + (101)_{2}= (10010)_{2}`$  

# REPRESENTATION DES NOMBRES ENTIERS RELATIFS  

## Première approche : codage avec bit de signe.

Les entiers relatifs sont appelés en informatique des **entiers signés** : ce sont les entiers positifs et négatifs.    
Dans la suite du cours __on précisera systématiquement sur combien de bits sont représentés ces nombres__.

La première idée pour coder ces nombres consiste à réserver le __bit de poids fort__ pour représenter le signe, les autres bits pour stocker la valeur absolue du nombre.      
__Le 0 sera réservé au signe positif et le 1 au signe négatif__  

__📢 Définition :__

> Le __bit de poids fort__ (ou __MSB__ : _most significant bit_) est celui ayant le plus grand poids c'est à dire le plus à gauche dans la notation positionnelle 

__📝Application__ :  

Codage avec bit de signe pour __-7__ : $`\underbrace{1}_{\text{bit de signe  }}\underbrace{111}_{\text{  valeur absolue}}`$

Quelques exemples

|Entiers relatifs | Représentation binaire sur 4 bits|  
|:----:|:----:|   
|3|__0 0 1 1__|  
|2|__0 0 1 0__|  
|1|__0 0 0 1__|  
|0|__0 0 0 0__ ou __1 0 0 0__ |  
|-1|__1 0 0 1__|  
|-2|__1 0 1 0__|  
|-3|__1 0 1 1__|  

Nous voyons que même si cette idée est simple à comprendre, elle présente deux inconvénients.  


- Le zéro est codé de deux façons différentes : **0000** et **1000**, ce qui peut poser problème.   
- Si on additionne deux nombres de signe opposé en utilisant les règles définies précédemment par exemple 2 et -2, on ne trouve pas zéro mais __-4__ !!   


|En binaire|En décimal|  
|----------|----------|  
|$`\begin{array}{ccccc}&&^{1}&& \\ &0&0&1&0 \\ +&1& 0&1&0 \\ \hline \\ =&1&1&0&0 \end{array}`$|$`\begin{array}{cc}& 2\\ +&(-2) \\ \hline \\ =&-4 \end{array}`$|  

**Nous ne pouvons donc pas garder cette première approche de représentation des entiers relatifs.**

## Le complément à deux : la bonne méthode !  

Le principe dit en __complément à 2__ consiste à ramener le cas des nombres négatifs à des nombres positifs pour pouvoir utiliser l'addition binaire.

__📢 Définition : codage en complément à deux__

> Pour un __codage sur n bits__ :  
> - si l'entier  x à coder est __positif__ on prend directement sa __valeur binaire__  
> - si l'entier x à coder est __négatif__  on prend la valeur $`\bf 2^{n} + x`$
</div>

__📝Application__ :   

* On souhaite repésenter en complément à deux le nombre  __5__, sur 8 bits :   
$`5 = \bf (00000101)_{2}`$  

* On souhaite repésenter en complément à deux le nombre  __-8__, sur 8 bits :     
$`(2)^{8}-8= 248 = \bf (11111000)_{2}`$


__📌Une méthode plus pratique:__  

Lorsque l’entier à représenter est négatif :  

* Dans un premier temps, on cherche la représentation binaire positive de l'entier relatif négatif sur n bits.   
   
* Ensuite, on fait le complément à 1 de ce nombre (__ce qui consiste à inverser la valeur de chacun des bits__)   
  
* On ajoute enfin 1 à cette dernière valeur  

__📝Application__ :   

On souhaite repésenter en complément à deux le nombre  __-8__  

* représentation binaire positive : $`(00001000)_{2}`$   
  
* complément à 1 : $`(11110111)_{2}`$   
  
* ajout de 1 :    
  
$`\begin{array}{ccccccccc} &&&&&^{1}&^{1}&^{1}& \\ &1&1&1&1&0&1&1&1 \\ +&&&&&&&&1\\ \hline \\ =&1&1&1&1&1&0&0&0 \end{array}`$    
  

__La représentation de -8 en complément à deux est $`\bf(11111000)_{2}`$__  


On peut vérifier que si on additionne 8 et -8 on retrouve 0.  

|Codage complément à deux sur 8 bits|En décimal|  
|----------|----------|  
|$`\begin{array}{ccccccccc}^{1}&^{1}&^{1}&^{1}&^{1}&&&& \\ &0&0&0&0&1&0&0&0 \\ +&1&1&1&1&1&0&0&0 \\ \hline \\ =&0&0&0&0&0&0&0&0 \end{array}`$|$`\begin{array}{cc}& 8 \\ +&(-8) \\ \hline \\ =&0 \end{array}`$|   

_Remarque_ :  

Lorsqu'on additionne deux nombres codés sur n bits et qu'il y a une retenue qui se reporte sur le (n+1)<sup>ème</sup> bit alors, il y a _dépassement de capacité_ : dans ce cas, la retenue est perdue.

__📢 Définition :__  

> __Sur __n bits__, la représentation en complément à deux permet de stocker tous les entiers compris entre $`\bf-2^{n-1}`$ et  $`\bf 2^{n-1} -1`$.__    
> __Le bit de poids fort représente toujours le signe de l'entier__  

__📝Application__ :  
Nous pourrons donc sur 8 bits avoir la représentation des nombres allant de  __-128__  à  __127__  c'est à dire  de $`(\textbf{1}0000000)_{2}`$ à $`(\textbf{0}1111111)_{2}`$.   


![Complement à deux sur 8 bits](./fig/complement_2_8bits.jpg)  

_Remarques pour un codage sur 8 bits_ :  

- Le nombre 0 n'a qu'une seule représentation avec cette convention : $`\bf(00000000)_{2}`$   
  
- Les entiers positifs sont représentés dans l'intervalle $`[0;127]`$   
  
- Les entiers négatifs le sont dans l'intervalle $`[128;255]`$ :  
   
    * En effet, $`\bf-128`$ est représenté comme $`2^{8}-128 \text{ soit } 128`$   
     
    * Le plus grand entier négatif : $`\bf-1`$ est représenté comme $`2^{8}-1 \text{ soit } 255`$   