# 📜 Contributions et licence

* La plupart des documents proposés sur ce site sont sous licence creative commons : __BY-NC-SA__.  
Ils ont été conçus soit par l'équipe pédagogique de NSI du [lycée Marguerite de Flandre](https://marguerite-de-flandre-gondecourt.enthdf.fr/) soit par [moi](patrice.thibaud@ac-lille.fr).

![licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

_Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre_  
Merci de __respecter les conditions de la__ [__licence__](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/)

* Pour les documents provenants de sources extérieures : ces sources sont sytématiquement citées

---

* Vous pouvez accéder aux versions numériques des cours et des TP en cliquant sur les noms correspondants
  
* Pour Les cours une version notebook (interactive) est également proposée en cliquant sur les images  ![my binder](https://mybinder.org/badge_logo.svg).  
Après chargement du notebook vous pouvez exécuter chaque cellule de code en la sélectionnant puis en utilisant la combinaison de touches `ctrl`+`Entrée`

---
# Chapitre 15 : Les dictionnaires

* 🆕 [Cours ](./15_dicos/c15_dicos.md)[![Notebook Tuples](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c15_dicos.ipynb)  
* 🆕 [TP ](./15_dicos/c15_tp_dicos.md) 


--- 
# ♨ Mini-Projet n° 3 : PIERRE FEUILLE CISEAUX

* 🆕 [Enonce](./mini_projets/pfs/mini3_pfs.md)   
 
---
# Chapitre 14 : Représentations des caractères en machine

* [Activité : codages de caractères ](./14_caracteres/ac_intro/c14_caracteres_ac_intro.md) 
* [Cours ](./14_caracteres/cours/c14_caracteres_cours.md) 
* [Exercices ](./14_caracteres/tp_caracteres/c14_tp_caracteres.md) 

---
# Chapitre 13 : Tuples

* [Cours : les n-uplets ](./13_tuples/c13_tuples_cours.md)[![Notebook Tuples](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c13_tuples_cours.ipynb)  
* 🎬 [Les tuples : video complements de cours ](https://youtu.be/Kw8UIavg8Os)    
* [Exercices ](./13_tuples/c13_tp.md)   


---
# Chapitre 12 : Réseaux

## PARTIE 1 : Relation client/serveur et protocole HTTP

* [TP : Formulaires et protocole HTTP](./12_reseaux/c12_p1_http/tp_formulaire/tp_formulaire.md)  
* 🎬 [The Internet: HTTP & HTML ](https://www.youtube.com/watch?v=kBXQZMmiA4s)  
* [Cours 1 : Relation client/serveur sur le Web](./12_reseaux/c12_p1_http/cours/c12_cours_http.md) 
* [Exercices ](./12_reseaux/c12_p1_http/exercices/c11_exos_client_serveur.md)

## PARTIE 2 : Transmission de données dans un réseau

* [Cours 2 : Transmission des données dans un réseau ](./12_reseaux/c12_p2_tcp/cours/c12_cours_tcp.md)  
* [TP1 : Premières requêtes sur les réseaux](./12_reseaux/c12_p2_tcp/tp1_filius/tp1_filius.md)  
* 🎬 [The Internet: Packets, Routing & Reliability ](https://www.youtube.com/watch?v=AYdF7b3nMto&t=5s)   
* [TP2 : Protocole TCP](./12_reseaux/c12_p2_tcp/tp2_tcp/tp2_tcp.md) 

---
# Chapitre 11 : Interface Homme-Machine (IHM) sur le Web

* [Découverte du Javascript ](./11_ihm_web/c11_p1_decouvertejs/c11_decouverte_js.md)  
* [Cours : Programmation événementielle  ](./11_ihm_web/c11_p2_evenementiel/c11_evenementiel.md) 
* [TP : Evenements en javascript](./11_ihm_web/c11_p2_evenementiel/tp_evenements/tp_evenements.md) 

---
# Chapitre 10 : Les tris

* [Activité débranchée : Introduction aux algorithmes de tris ](http://www.nileb.net/nsi/algo01/balance.html)  

* [TP n°1 : tri par insertion ](./10_tris/tp1_tri_insert/tri_insertion_intro.md)

* [Cours : les algorithmes de tris ](./10_tris/c10_tris_cours.md)   

* [TP n°2 : Comparaisons de tris ](./10_tris/tp2_comparaisons_tris/tp2_comparaisons_tris.md)  


---
# Chapitre 9 : Technologies du web 1, langages HTML et CSS 

* [Prérequis (niveau débutant) ](./9_html_css/prerequis_html_css.md)    

* [Activité niveau avancé ](./9_html_css/html_css_niveau2.md)  


---
# Chapitre 8 : Spécification et tests des programmes 

* [Cours ](./8_specification_tests/c8_specification_tests.md)[![Notebook Listes Python](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c8_specification_tests.ipynb)   

* [TP ](./8_specification_tests/tp_spe_tests/tp_spe_tests.md)  


--- 
# ♨ Mini-Projet n°2 : Jeu de BATAILLE 

* [Enonce](./mini_projets/bataille/jeu_bataille.md)   
  
* [Script support](./mini_projets/bataille/jeu_bataille.py)   

---
# Chapitre 7 : Une structure de donnée construite, les tableaux  

## PARTIE 1 : Listes Python

*  [Cours :  Listes Python ](./7_tableaux/c7_partie1_listes/c7_p1_cours_listes.md)[![Notebook Listes Python](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c7_p1_cours_listes.ipynb)  


*  Comprendre la mutabilité des listes avec Python Tutor    
    * 🎓 [1er exemple : sans utilisation de copy ](http://pythontutor.com/visualize.html#code=def%20copieruneliste%28%29%3A%0A%20%20%20%20liste%20%3D%20%5B%200,%201,%202,%203,%204,%205%5D%0A%20%20%20%20listebis%20%3D%20liste%0A%20%20%20%20liste.append%286%29%0A%20%20%20%20listebis.pop%28%29%0A%20%20%20%20print%28id%28liste%29%29%0A%20%20%20%20print%28id%28listebis%29%29%20%20%20%20%20%20%20%20%20%20%20%20%0A%0Acopieruneliste%28%29%0A%0A%23%20On%20voit%20bien%20que%20liste%20et%20listebis%20pointent%20vers%20le%20m%C3%AAme%20emplacement%20m%C3%A9moire.%20%0A%23%20Ils%20agissent%20sur%20la%20m%C3%AAme%20liste.&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)  
   
    * 🎓 [2ème exemple : avec copy ](http://pythontutor.com/visualize.html#code=from%20copy%20import%20copy%0Adef%20copieruneliste%28%29%3A%0A%20%20%20%20liste%20%3D%20%5B%200,%201,%202,%203,%204,%205%5D%0A%20%20%20%20listebis%20%3D%20copy%28liste%29%0A%20%20%20%20liste.append%286%29%0A%20%20%20%20listebis.pop%28%29%0A%20%20%20%20print%28id%28liste%29%29%0A%20%20%20%20print%28id%28listebis%29%29%0A%20%20%20%20%20%20%20%20%20%20%20%20%0Acopieruneliste%28%29%0A%0A&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)    
  

*  [TP : Découverte des listes ](./7_tableaux/c7_partie1_listes/tp1_listes/tp1_listes.md)



## PARTIE 2 : Listes imbriquées et compréhensions

*  [Cours :  Compréhensions de listes et tableaux à 2 dimensions ](./7_tableaux/c7_partie2_listes/c7_p2_cours_listes.md)[![Notebook Compréhensions de listes et tableaux à 2 dimensions ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c7_p2_cours_listes.ipynb)  

*  [TP : Compréhensions de listes et structures imbriquées ](./7_tableaux/c7_partie2_listes/tp2_listes/tp2_listes.md)


--- 
# Chapitre 6 : Boucles bornées  

## PARTIE 1 : Boucles for

*  [Cours :  Boucles for ](./6_boucles_bornees/c6_partie1_boucles_for/c6_partie1_for.md)[![Notebook Boucles for](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c6_partie1_for.ipynb)  


*  [TP : Dessiner avec la tortue ](./6_boucles_bornees/c6_partie1_boucles_for/tp_turtle/tp_turtle.md)  
  
* 🐢[Pour aller plus loin : Défie la Tortue !](./6_boucles_bornees/c6_partie1_boucles_for/defi_turtle/challenges_turtle.md)  


## PARTIE 2 : Parcourir une chaîne de caractères

* [Cours :  Parcours des chaînes ](./6_boucles_bornees/c6_partie2_string/c6_partie2_for_string.md)[![Notebook Boucles for](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=c6_partie2_for_string.ipynb)  


*  [TP : Parcours des chaînes ](./6_boucles_bornees/c6_partie2_string/tp_parcours_chaines/tp_parcours_chaines.md)  


--- 
#  Analyse de code (retour sur l'exercice n°5 du DS)  

*  [Détecter les erreurs (DS chap 4, 5) ](./analyse_code/analyse_code_1.md)  


--- 
# Chapitre 5 : Architecture des ordinateurs    

## PARTIE 1 : Portes logiques et circuits combinatoires  

* [Cours fonctions boolennes et portes logiques](./5_architecture/c5_partie1_portes logiques/cours_portes_logiques.md)  

* [TP Circuits combinatoires](./5_architecture/c5_partie1_portes logiques/tp_portes_logiques/tp_portes_logiques.md)  

## PARTIE 2 : Modèle de von Neumann  

* [Cours Modèle de von Neumann](./5_architecture/c5_partie2_von_neumann/cours_vonneumann.md)  

* 🎬 [La 1ère carte perforée](https://www.youtube.com/watch?v=MDQHE0W-qHs)   

* 🎬 [La vie de Jon von Neumann(documentaire ARTE)](https://www.youtube.com/watch?v=c9pL_3tTW2c)  _Regarder surtout entre 39:12 et 46:00_  
  
*  [Exemple d'utilisation de la machine débranchée M999](./5_architecture/c5_partie2_von_neumann/exemple_utilisation_m999/exemple_m999.pdf)  

* [TP : Architecture et langage assembleur](./5_architecture/c5_partie2_von_neumann/tp2_assembleur/tp2_assembleur.md)    


--- 
# ♨ Mini-Projet n°1 : Jeu de NIM  

* [Enonce](./mini_projets/nim/mini_projet_1_nim_enconce.md)   
  
* [Script support](./mini_projets/nim/nim_sujet.py)   




--- 
# Chapitre 4 : Opérateurs booléens et structures conditionnelles 

* [Cours : Opérateurs booléens et conditionnelles ](./4_booleens_conditions/cours_booleens_conditions.md)[![Notebook Entiers relatifs et réels](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours_booleens_conditions.ipynb)  


* [TP : Opérateurs booléens et conditionnelles ](./4_booleens_conditions/tp/tp_booleens_conditions.md)  
  



--- 
# Chapitre 3 : Représentations des entiers relatifs et des nombres réels  

## PARTIE 1 : Opérations binaires et entiers signés 

* [Cours :  Entiers relatifs ](./3_entiers_relatifs_et_reels/c3_partie1_relatifs/cours_entiers_relatifs.md)[![Notebook Entiers relatifs et réels](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours_entiers_relatifs.ipynb)


* [TP : Entiers relatifs ](./3_entiers_relatifs_et_reels/c3_partie1_relatifs/tp_relatifs/tp_relatifs.md)


## PARTIE 2 : Nombres réels 

* [Cours : Nombres réels ](./3_entiers_relatifs_et_reels/c3_partie2_reels/cours_reels.md)[![Notebook Entiers relatifs et réels](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours_reels.ipynb)


* [TP : Nombres réels](./3_entiers_relatifs_et_reels/c3_partie2_reels/tp_reels/tp_reels.md)  



---
# Chapitre 2 : Représentations des entiers naturels

## PARTIE 1 : Les bases de numération 

* 🎬 [Le langage binaire](https://www.youtube.com/watch?v=VRdp_vaNRoY&feature=youtu.be)  

* [Cours :  Bases numériques](./2_representations_entiers_naturels/entiers1_bases/cours1_bases.md)
[![Notebook Bases numériques](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours1_bases.ipynb)  
  
* [TP1 :  Les bases](./2_representations_entiers_naturels/entiers1_bases/tp1_bases/tp1_bases.md)  


## PARTIE 2 : Boucles tant que et Algorithmes


* 🎬 [L'histoire d'Al-Khwarizmi](https://www.youtube.com/watch?v=QbZms6RgLRE)  

* [Cours :  Boucles tant que et algorithmes ](./2_representations_entiers_naturels/entiers2_algo/cours2_while_algo.md)
[![Notebook Boucles tant que et algorithmes ](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours2_while_algo.ipynb)

* 🎓 [Pythontutor : _déroulé d'une boucle tant que_](http://www.pythontutor.com/visualize.html#code=x%20%3D%200%0Awhile%20x%20%3C%203%3A%0A%20%20%20%20x%20%3D%20x%20%2B%201%0A%20%20%20%20print%28x%29%0Aprint%28'Fin'%29&cumulative=true&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)   

  
* [TP2 :  Les boucles tant que](./2_representations_entiers_naturels/entiers2_algo/tp2_boucles_while/tp2_boucles_while.md)  


* 💻[Pour aller plus loin...](https://python.iutsf.org/lecon-4-les-boucles/) : Rubrique _"Des exercices pour s'entrainer"_ à partir de _"Liste des multiples d'un entier"_
  

---


# Chapitre 1 : Découverte du langage Python

## PARTIE 1 : Manipuler les nombres  

* [Cours :  Manipuler les nombres](./1_decouverte_python/python1_nombres/cours1_manipuler_nombres.md)
[![Notebook Manipuler les nombres](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours1_manipuler_nombres.ipynb)


* [TP1 :  Nombres](./1_decouverte_python/python1_nombres/tp1_nombres/tp1_nombres.md)


## PARTIE 2 : Fonctions 

* [Cours :  Les fonctions](./1_decouverte_python/python2_fonctions/cours2_fonctions.md)
[![Notebook Fonctions](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours2_fonctions.ipynb)


* [TP2 :  Fonctions](./1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md)


* 💻[Pour aller plus loin.... ](http://www.fil.univ-lille1.fr/~L1S1Info/Doc/New/TP2_fr.html)

## PARTIE 3 : Chaînes de caractères

* [Cours :  Les chaînes de caractères](./1_decouverte_python/python3_strings/cours3_strings.md)
[![Notebook chaînes de caractères](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fpatrice.thibaud%2Fnotebooks_1ere_nsi/master?filepath=cours3_strings.ipynb)


* [TP3 :  Chaînes de caractères](./1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md)  

 



