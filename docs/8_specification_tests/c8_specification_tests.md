---
title: "Chapitre 8 Spécification et tests des programmes "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

La __spécification__  d'une fonction consiste à décrire de manière explicite ce que doit faire cette fonction. 
Nous allons voir comment spécifier et tester son programme pour être le plus rigoureux possible.

# Documenter son programme
Il est fortement conseillé de donner une documentation (description) à chaque fonction c'est ce que nous avons déjà fait à l'aide de __docstrings__

```python
def division_euclidienne(a, b) :
    """ 
    renvoie le quotient et le reste de la division euclidienne de a par b
    """
    quotient = a // b  
    reste = a % b  
    return [quotient, reste] 
```

L'utilisateur peut ainsi comprendre à quoi sert chacune des fonctions :

```python
>>> help(division_euclidienne)

 Help on function division_euclidienne in module __main__:

 division_euclidienne(a, b)
     renvoie le quotient et le reste de la division euclidienne de a par b
```

__📢 Définitions :__  

La documentation pour être réellement efficace doit cependant contenir plus d'informations comme notamment :   

* des `préconditions `  : elles doivent garantir que l'exécution du traitement est possible sans erreur.   

* des `postconditions` :  elles doivent garantir que le traitement a bien réalisé son travail.  


__📝Application__ :

Dans le cas de la fonction `division_euclidienne` on peut facilement lister ces conditions

|__préconditions__|__postconditions__|    
|:---:|:---:|      
|`a` et `b` sont deux entiers|`quotient`  et  `reste` sont deux entiers|   
|$`a>=0`$ et  $`b >0`$|$`a = quotient \times b + reste`$|   

\newpage

Ces informations peuvent être structurées en utilisant un format appelé __ReStructuredText (reST)__

```python
def division_euclidienne(a, b) :
    """ 
    renvoie le quotient et le reste de la division euclidienne de a par b

    :param int a: diviseur a >= 0
    :param int b: dividende b > 0
    :return: [quotient, reste] tel que a = quotient * b + reste
    :rtype: list(int)
    """

    quotient = a // b  
    reste = a % b  
    return [quotient, reste] 
```

* chaque paramètre associé à son type est décrit ainsi : `:param <type> <nom>: <description>` , ceci permet de préciser les préconditions   
  
* la valeur de retour est aussi décrite : `:return: <description>` ainsi que son type `:rtype:`, ceci permet de préciser les postconditions    



# Programmation défensive

La documentation ne protége pas d'une mauvaise utilisation d'une fonction surtout si on ne la lit pas!  Si on souhaite éviter des erreurs ou des résultats incohérents on peut interrompre le programme dès lors que la fonction reçoit une information non valide ou renvoyer des valeurs spéciales


## Assertions

Une __assertion__ est une expression qui doit être évaluée à vrai. Si cette évaluation échoue elle peut mettre fin à l'exécution du programme. L'utilisation du mot clé `assert` en Python permet de réaliser des tests unitaires.

Dans le cas de la fonction `division_euclidienne` on pourrait par exemple s'assurer que le dividende est entier mais aussi strictement positif avant de calculer quotient et reste.

```python
def division_euclidienne(a, b) :
    # docstring

    assert type(b) == int  , "le dividende doit être entier"
    assert b != 0 , "le dividende ne peut être nul"

    # suite des instructions 
```

L'assertion a pour syntaxe : `assert condition , "message"`

Réalisons maintentant un appel ne vérifiant pas la première précondition :

```python
>>> division_euclidienne(5, 1.5)

 Traceback (most recent call last):
   File "<pyshell>", line 1, in <module>
   File "C:\Users\Desktop\test.py", line 7, in division_euclidienne
     assert type(b) == int  , "le dividende doit être entier"
 AssertionError: le dividende doit être entier
```

Le programme est interrompu et un message d'erreur permet d'expliciter ce qui pose problème : on appelle ceci la __programmation défensive__.

## La valeur spéciale  None

Une autre façon d'être défensif consiste, plutôt que d'échouer, à renvoyer une valeur qui ne peut être confondue avec un résultat valide.

```python
def division_euclidienne(a, b) :

    # docstring
    if b == 0:
        return None
    # suite des instructions 
```

```python
>>> division_euclidienne(5, 0)
>>> 
```

Cette façon de procéder permet d'appeler la fonction sans précaution particulière et de la  tester avant de l'utiliser. Aucune valeur n'est à priori renvoyée en fait, c'est une valeur spéciale appelée `None` : c'est la même qui est est renvoyée lors d'appel de procédure (comme les affichages), son type est `Nonetype`

```python
>>> type(print())
 <class 'NoneType'>
```

# Tests dans la docstring

## Le module doctest

Le module `doctest` permet d’inclure les tests dans la docstring descriptive de la fonction écrite. On présente dans la docstring, en plus des explications d’usage, des examples d’utilisation de la fonction tels qu’ils pourraient être tapés directement dans le Shell.

```python
def division_euclidienne(a, b) :
    """ 
    # début de la docstring

    :example:

    >>> division_euclidienne(5, 2)
    [2, 1]

    >>> division_euclidienne(11, 3)
    [3, 2]
    """
    # suite des instructions
```

Remarquez l'utilisation de la rubrique `:example:` placée en fin de docstring.

Afin d'exécuter l'ensemble des tests on peut utiliser dans le Shell les instructions suivantes :

```python
>>> import doctest
>>> doctest.testmod()

TestResults(failed=0, attempted=2)
```

La fonction `testmod` du module `doctest` est allée chercher dans les docstring des fonctions du module actuellement chargé tous les exemples (reconnaissables à la présence des triples chevrons `>>>`), et a vérifié que la fonction documentée satisfait bien ces exemples. Dans le cas présent, une seule fonction dont la documentation contient deux exemples (attempted=2) a été testée, et il n’y a eu aucun échec (failed=0).


Si les résultats annoncés dans le doctest ne correspondent pas à ceux qui sont renvoyés par la console, chacun des tests est détaillé

__📝Application__ :

Modifions le résultat du premier exemple dans la docstring puis relançons le test :

```python
    """
    >>> division_euclidienne(5, 2)
    [4, 1]
    """
```

```python
.......
File "test.py", line 12, in __main__.division_euclidienne
Failed example:
    division_euclidienne(5, 2)
Expected:
    [4, 1]
Got:
    [2, 1]
.........
**********************************************************************
1 items had failures:
   1 of   2 in __main__.division_euclidienne
2 tests in 2 items.
1 passed and 1 failed.
***Test Failed*** 1 failures.
```

## Bon ensemble de test

Même si l’écriture de tests ne garantit en rien qu’un programme est correct, le fait de réfléchir aux cas limites que l’on peut rencontrer et en écrire une vérification explicite permet de délimiter correctement le problème.
Il est dificile de déterminer si l'ensemble de test est suffisant mais on peut déjà se baser sur les points suivants :

* Tester les __cas particuliers__ évoqués dans la description des paramètres.    
  
* Si une fonction renvoie un __booléen prévoir au moins deux tests__ avec des résultats différents.    

* Si une fonction utilise un __nombre__, prévoir des tests pour des __valeurs positives, négatives et nulle__.    

* Si une fonction fait intervenir des valeurs appartenant à un intervalle, il est utile de prévoir des __tests aux limites de cet intervalle__.  


