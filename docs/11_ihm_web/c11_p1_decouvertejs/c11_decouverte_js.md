---
title: "Chapitre 11 : Interface Homme-Machine (IHM) sur le Web "
subtitle: "PARTIE 1 :  Découverte du javascript"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
En _1995_ , __Brendn Aich__ travaille chez Netscape Communication Corporation, la société qui éditait le célèbre navigateur Netscape Navigator, alors principal concurrent d’Internet Explorer. Il y développe le LiveScript, un langage destiné à être installé sur les serveurs développés par Netscape. Netscape se met à développer une version client du LiveScript, qui sera renommée __JavaScript__ en hommage au langage Java créé par la société Sun Microsystems.    

![logo JS](./fig/logo_js.png)

- Javascript est un __langage de programmation interprété__. (Il faut un navigateur web qui dispose d’un interpréteur JavaScript)
-	Il est utilisé principalement dans la conception de pages web et permet d’agir sur les éléments HTML, rendant ainsi la page web interactive.

# Ma première instruction

Afin de tester simplement ce langage nous allons utiliser un outil de Firefox appelé __Console web__.

__📝Application 1__:

__1)__ Lancez le navigateur Mozilla Firefox.  
Ouvrez la __console web__ par  le raccourci  `Ctrl + Maj + K`  (ou par le menu Développement web / Console web ) 

![Console Web](./fig/console_web.png)
     
Tapez le code suivant et observez l'effet :   

```javascript
window.alert('Hello World');
```

- L’instruction `window.alert` ouvrira une fenêtre d’alerte (ou __pop-up__)  dans laquelle sera visible le contenu de  l’__argument__ (ce qui est à l’intérieur de la parenthèse) 
Dans notre cas l’argument est une _chaine de caractères_ .  

> __📢 Chaque fin d’instruction en javascript  est marquée par un point-virgule `;`__

__2)__ Modifiez le code précédent en changeant le contenu de l’argument 

# Les variables

## Déclaration et affectation

> __📢 Avant d’être utilisée, une variable doit être déclarée à l’aide du mot-clef `let`. Il n'est pas nécessaire de lui affecter tout de suite une valeur.__

Exemple de déclaration d’une variable nommée `x` puis affectation d'une valeur:  

```javascript
>> let x;
>> x = 24;

← 24
```

Le fait de valider par entrée la ligne d’instruction permet de l’évaluer (comme dans un shell python ). C’est le sens de la valeur fournie par console.  

> __📢 Il n’y a pas de type entier ou flottant en javascript mais seulement un type `number`__


```javascript
>> typeof x;
← "number"
```

Attention une redéfinition de la même variable est interdite avec le mot clé `let` :

![Redéfinition variable](./fig/let.png)

## Mon premier script

Bien évidemment écrire les instruction lignes par ligne et très contraignant : on dispose d’un éditeur multiligne bien utile sur Firefox. Cliquez à droite de la ligne de commande sur le bouton ![Editeur multilignes](./fig/editeur_multilignes.png)


__📝Application 2__:

Créer un programme donnant qui demande à l’utilisateur de rentrer le dividende et le diviseur et qui affiche le 
résultat de la division euclidienne sous la forme : `dividende=diviseur*quotient + reste`
                                      
Par exemple si l’utilisateur entre 5 et 2, une boite de dialogue affichera :

![Division euclidienne](./fig/division.png)

On fournit les fonctions et les opérateurs utiles :

|Opérateur / Instruction| Effet|   
|:-----:|:-----|   
|`+`| __concaténation__ : (peu importe le type) sous forme de chaine de caractères |   
|`%`| opérateur modulo|  
|`parseInt()`| Analyse une chaine de caractère passée en paramètre et renvoie la partie entière du nombre correspondant|  
|`Math.floor()`| Arrondi à l’entier inférieur le nombre passé en paramètre|  
|`window.prompt()`| Equivalent de `input` mais avec une boite de dialogue|  
|`window.alert()`| Equivalent de `print` mais avec une boite de dialogue|   

Remarques :

-	Pour recharger la page `F5` (évite les erreurs de redéclaration)
-	Une sauvegarde du script est possible `CTRL + S` : le fichier a pour extension   `.js`
-	Pour plus de détails sur l’utilisation des fonctions fournies consultez [MDN : Guide Javascript](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide)

# Structures conditionnelles

## Comparaisons et instructions conditionnelles

> __📢 Les opérateurs de comparaison utilisés en Python sont les mêmes en Javascript `< > == <= >= !=`__   
__L’évaluation d’un expression les utilisant conduit à un __booléen__ noté `true` ou `false` (attention tout en minuscule !)__  

```javascript
>> 2 == 3;
← "false"

>> 'a' != 'b';
← "true"
```

Les structures conditionnelles quant à elles différent légèrement de part leur écriture :

```javascript
let x = 4;

if (x < 3){              
  window.alert("x est plus petit que 3");
}

else if (x > 5){
  window.alert("x est plus grand que 5");
}

else {
  window.alert("x est compris entre 3 et 5");
}
```

\newpage

> __📢 La syntaxe des structures conditionnelles doit respecter les éléments suivants:__
> 
> * __Les conditions sont placées entre parenthèses `()`__
> * __Chaque bloc d'instructions est délimité par une paire d'accolades `{}`__
> * __L'indentation n'a pas d'importance (même si cela est conseillé pour plus de lisibilité)__
> * __`else if` (javascript) remplace _elif_ (python)__


## Opérateurs logiques

> 📢 En javascript les opérateurs logiques ont une syntaxe un peu différente

|Opérateur| Syntaxe en javascript|   
|:-----:|:-----:|   
|__ET__|`&&`|
|__OU__|$`\Vert`$|
|__NON__|`!`|   


__📝Application 3__:

Voici l’extrait d’une grille de catégories d’un club de judo pour l’année en cours.

![Catégories JUDO](./fig/cat_judo.jpg)

La catégorie d'âge n'est pas déterminée en début de saison sportive, mais est calculée au 1er janvier de chaque année  
 Source : http://www.judobarsurloup.com/

Créer un programme demandant à l’utilisateur de rentrer le nom, le prénom ainsi que l’année de naissance de la personne à inscrire.
Le programme se terminera par un message d’inscription du type :

«  Bienvenue, Teddy RINNER, vous êtes inscrit en catégorie Seniors »    



# Les fonctions

> __📢 On peut définir ses propres fonctions à l’aide du mot-clé `function`__

Par exemple : 

```javascript
function carre(n) {
  return n**2;
}
```

_Remarques :_ 

-	Dans l’exemple ci-dessus on déclare une fonction nommée `carre`, cette fonction accepte un seul paramètre appelé ici `n`. 
- Le bloc d'instructions correspondant est placé entre accolades.
-	Le mot-clé return a la même signification qu'en Python.

La procédure d’appel d’une fonction est similaire à celle rencontrée en Python :

```javascript
>> carre(5)
← 25
```



__📝Application 4__:

__a)__	Ecrire un programme qui demande la saisie d’un nombre représentant une température en degré Celsius et qui affiche la température en degré Fahrenheit équivalente en faisant appel à une fonction nommée `CF` 

__b)__	Modifiez votre programme pour laisser le choix à l’utilisateur de la conversion (Celsius vers Fahrenheit ou le contraire) toujours en faisant appel à des fonctions

AIDES :

-  la fonction `parseFloat()` permet de transformer une chaine de caractère en un nombre décimal

-	Afin d’arrondir un résultat à un nombre de décimal fixé on peut utiliser la méthode
`.toFixed()` (nbre de decimales)        
exemple : 

```javascript
>> (7.568).toFixed(2)
← 7.57
```



__📝Application 5__: (SPE MATHS)

__a)__	Créer une fonction `Calcul_Discriminant` qui renvoie le discriminant d’un trinôme du second degré du type __ax² + bx + c__  à condition de lui fournir le bon nombre de paramètres

__b)__	Réutilisez la fonction précédente pour créer un nouveau script demandant à l’utilisateur de fournir les paramètres `a`, `b` et `c` du trinôme et lui affichant dans une boite de dialogue la plus grande racine réelle à condition que le discriminant soit positif ou nul (un message adapté s’affichera si ce n’est pas le cas)

remarques :
-	Vous nommerez donc `coeff_a`, `coeff_b` et `coeff_c` les données fournies par les utilisateurs et `a`, `b`, `c`
 celles utilisées dans les fonctions
-	vous nommerez discriminant la variable qui stockera la valeur retournée par la fonction `Calcul_Discriminant`
-	créer une fonction nommée `plus_grande_racine`  qui renvoi la plus grande solution du trinôme précédent à condition que le discriminant soit positif

__AIDES :__

Les fonctions `Math.max()` et `Math.sqrt()` vous aideront dans cette tâche.

\newpage

# Points communs et différences entre langages

« Les langages de programmation permettent de décrire d'une part les structures des données qui seront manipulées par l'appareil informatique, et d'autre part d'indiquer comment sont effectuées les manipulations, selon quels algorithmes. Ils servent de moyens de communication par lesquels le programmeur communique avec l'ordinateur, mais aussi avec d'autres programmeurs »

_source Wikipedia_ 

Wikipedia référence plus de 600 langages de programmation, il bien évidemment imoossible deles connaitre et encore plus de les maîtriser tous !

Cependant on pu constater des similitudes dans les notions employées en Python et en JAvascript comme par exemple:

* Les instructions
* Les variables
* Les types de données
* Les fonctions

....

L'adaptation à un nouveau langage est souvent assez simple car dépendant principalement d'un nouveau vocabulaire (les instructions ne sont pas écrites avec les mêmes mots) et de quelques éléments de syntaxe spécifiques.

![Popularité langages](./fig/langages.jpeg)

_source : Statista [^1]_

__📝Application 6__: 

Dresser un tableau comparatif des langages Javascript et Python à travers les applications rencontrées. Il est inutile de mentionner des instructions équivalentes qui s'écrivent différemment : par exemple `def` en python et `function` en javascript.  
Vous vous intéresserez aux différences notables dans les structures et dans l'utilisation de ces langages.

[^1]:https://fr.statista.com/infographie/16559/langages-programmation-informatique-les-plus-populaires/
