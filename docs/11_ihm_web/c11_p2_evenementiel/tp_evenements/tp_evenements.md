---
title: "Chapitre 11 : Interface Homme-Machine (IHM) sur le Web "
subtitle: "TP : Evenements en Javascript"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 :  Bonnes pratiques &#x1F3C6;

__1.__ Récupérer tout d'abord l'archive [`bonnes_pratiques_js.zip`](./bonnes_pratiques_js.zip) et décompressez-la. A l'aide d'un éditeur de votre choix visualiser le code html de `index1.html` et afficher-la dans le même temps avec le navigateur de votre choix pour comprendre le code associé.

__a)__ Dissocier le CSS du reste du code en créant un fichier séparé et en faisant le lien avec la page html.

__b)__ Dissocier le code JS de la même manière.

__c)__ Il reste encore des "traces" de javascript dans le code html : il s'agit de la propiété `onclick`. A l'heure actuelle il est recommandé d'éviter d'utiliser cette propriété.
Modifiez les codes html et javascript afin de respecter les bonnes pratiques présentées en cours pour la gestion d'événements.

# Exercice 2 :  Une page qui a du style &#x1F3C6;  &#x1F3C6;

__1.__ Soit une page HTML contenant un élément `p` dont l'`id` est `p1` et un bouton.

```html
....
<p id="p1">un paragraphe</p>
<button>Modifier le paragraphe</button>
...
```

En respectant les bonnes pratiques vues dans l'exercice 1, mettez en place un gestionnaire d'événément `modif_style` associé au clic sur le bouton qui aura pour effet de modifier quelques propriétés de style de la page :

* la couleur du texte deviendra bleue
* le paragraphe sera encadré par un trait continu noir de largeur 2px.
* l'élément body aura une couleur d'arrière-plan cyan.

__2.__

__a-__ Supprimez le bouton, maintenant les changements précédents devront s'effectuer lors du survol du paragraphe.

__b-__ Pour aller plus loin vous pouvez détécter la sortie de la zone de survol avec l'événement `mouseout` pour réinitialiser les propiétés de style.


# Exercice 3 : QCM  &#x1F3C6;
_(Questions 1,2  extraites de QCM sujet 0 prime )_

__1.__  Lors de la consultation d’une page HTML, contenant un bouton auquel est associée la fonction
suivante, que se passe-t-il quand on clique sur ce bouton ?

```javascript
function action(event) {
this.style.color = "blue"
}
```

* [ ] Le texte du bouton passe en bleu. 
* [ ] Le texte de la page passe en bleu.
* [ ] Le texte du bouton est changé et affiche maintenant le mot “bleu”. 
* [ ] Le pointeur de la souris devient bleu quand il arrive sur le bouton.

__2.__ Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?

* [ ] La machine de l’utilisateur sur laquelle s’exécute le navigateur web.
* [ ] Le serveur web sur lequel est stockée la page HTML.
* [ ] La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.
* [ ] La machine de l’utilisateur ou du serveur, suivant la confidentialité des données manipulées. 


__3.__ Parmi les attributs suivants d'une balise `<button>`, lequel doit être rédigé en javascript ?

* [ ] l'attribut `class`.
* [ ] l'attribut `id`.
* [ ] l'attribut `onclick`.
* [ ] l'attribut `title`.

