---
title: "Chapitre 12 : Reseaux "
subtitle : "TP : Premières requêtes sur les réseaux"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1: Manipulation de l'invite de commandes de votre PC. 

* Pour aller dans l'invite de commandes, appuyez sur les touches `windows + R`.
* Tapez `cmd` puis validez avec `Entrée`.

__1)__ Essayez de retouver votre adresse IP version 4 sur le réseau du lycée ansi que l'adresse physique (MAC) de la carte réseau de votre ordinateur. Notez-les

```bash
ipconfig /all
```
* IP (v4)  : ...........................
* MAC : ..............................

__2)__ Etudions la table ARP stockée sur votre ordinateur.

```bash
arp -a
```
_Remarque_ : Vous pouvez, si vous avez les droits nécessaires la vider (essayez chez vous):  `arp -a -d`


* Demandez à votre voisin si votre IP apparait dans sa table (et réciproquement)
* Si ce n'est pas le cas Envoyez la commande `ping` suivi de l'IP que vous voulait tester. Par exemple :  

```bash
ping 192.168.0.11
```
La commande `ping` permet d'envoyer des paquets de données vers la machine dont on indique l'IP.

* Combien de paquets ont été envoyés?  ............. 
 
On voit la caractéristique `ttl` qui veut dire __Time To Live__  : ce nombre décroit de une unité à chaque fois qu'il rencontre un routeur.  
Le TTL initial est fixé pour tous à 128.

* Combien de routeurs a rencontré chaque paquet ?  ...................

* Consultez de nouveau votre table ARP. Que remarquez vous ?..............................................

* Sachant que lorsque TTL atteint la valeur de 0, le paquet est détruit. Combien de sauts peut faire le paquet?  .....

__3)__ Nous allons envoyer un ping sur le site `facebook.fr `

- Ecrivez la ligne de commande correspondante : .............................. 
- Quelle est l'adresse IP publique du serveur qui vous répond? ........................
- Combien de routeurs ont été traversés? (Attention dans ce cas le TTL initial est de __64__) ........................  

__4)__ On peut vérifier la dernière information en suivant les chemins des paquets de données à l'aide du programme `tracert` sous Windows

```bash
tracert facebook.fr
```
Chaque routeur représente un point de connexion entre deux réseaux, au travers duquel le paquet de données a été transmis.   
* Vous pouvez notez les adresses IP ou les noms des routeurs rencontrés, ils apparaissent dans la dernière colonne


__Attention__  : au lycée parfois (voir souvent !) vous aurez "Délai d'attente de la demande dépassée" : cela indique simplement que certains routeurs sur la route ne réponde pas à la requête.

* Comparez le nombre de routeurs traversés  à la valeur du TTL final .............................

Vous pouvez également utiliser des outils existant en ligne (avec modération !) : https://ping.eu/traceroute/

* Notez les pays dans lesquels sont situés les réseaux traversés .........................................


# Exercice n°2: Binaire et hexadécimal

Utilisez vos adresses IP et MAC notées dans l'exercice précédent.  
__a)__  Convertissez l'adresse MAC en adresse décimale  
__b__  Convertissez l'adresse IP en adresse binaire  
__c)__  Combien au total d'adresses IPv4 peuvent exister ? et en IPv6 ?  
__d)__  Même question pour les adresses MAC  


# Exercice n°3: Simulation de réseaux avec Filius  

__PARTIE 1 : Connexion point à point__

__FILIUS__ est un logiciel pédagogique pour aider à la compréhension et à l’apprentissage des concepts des réseaux informatiques.  

* __Pour le télécharger__ :  [Filius à télécharger](https://www.lernsoftware-filius.de/Herunterladen) Cliquer sur le système d'exploitation Windows (ou Ubuntu)
* __Pour l'installer__ : l'installateur est en Allemand, validez les choix par défaut avec Entrée. Lors de la première utilisation voux choisirez votre langue.
* __Pour l'utiliser__ : un lien vers quelques videos montrant quelques manipulations basiques : [Utilisation FILIUS](https://pixees.fr/informatiquelycee/n_site/snt_internet_sim1.html)



![Image logiciel Filius](./fig/filius1.png)  

On y trouve :  

* _une zone centrale blanche_ où l’on construira le réseau 
* _sur la gauche_  : les différents composants du réseau 
* _en haut_ : des boutons pour charger ou enregistrer un fichier, des boutons de commande pour la simulation (le bouton play) ou la construction d’un réseau (le marteau) et enfin un glisseur pour contrôler la vitesse des simulations.

__1) On commence par créer un réseau élémentaire avec deux ordinateurs de bureau reliés par un cable__

* Cliquez/ glissez pour déposer un premier ordinateur dans la zone centrale
* Faites un double click sur lui pour observer en bas ses caractéristiques réseau
* Affectez l'adresse IP __192.168.0.10__ au premier ordinateur (en changeant éventuellement la valeur dans `Adresse IP`) puis cochez la case `Utiliser l'adresse IP comme nom`
* Faites la même chose avec un autre ordinateur d'IP: __192.168.0.11__
* Reliez-les par l'intermédiaire d'un cable


🎬 [une vidéo d'accompagnement à utiliser si nous n'arrivez pas à utiliser correctement FILIUS](https://youtu.be/GTJ3FIqjQuo)


__2)__ Appuyez sur le bouton "play". Vous passez en mode simulation.  

* Double-cliquer sur l'ordinateur `192.168.0.10 `
* Cliquer sur l'icône `Installation des logiciels`
* Cliquer sur `ligne de commandes` et la fleche verte gauche pour mettre l'instruction dans `installés`.
* Cliquer sur `Appliquer les modifications`.
* Exécuter alors l'application `ligne de commandes` en double cliquant dessus.

Une invite de commandes vous permet d'utiliser quelques commandes sur cet ordinateur du réseau.
A tout moment vous pouver utiliser la commande 
```bash
help
```
pour accéder aux commandes disponibles

__3)__ Testez la connexion avec l'autre ordinateur à l'aide de la commande `ping`

__PARTIE 2 : Construction d'un réseau local.__  

Construisez maintenant un réseau local comprenant trois ordinateurs chacun relié à  un switch par un cable.  

* Affecter des adresses IP différentes à chaque ordinateur. 
* Passer en mode simulation

🎬 [video pour la PARTIE 2](https://youtu.be/pvWdJLA1z8o)

__4)__ Observer le réseau avant communication.  

Cliquer sur le switch: Une boite de dialogue apparait avec la table MAC.

- Qu'observez-vous? .................................................  
  
Cliquer bouton droit sur l'ordinateur d'adresse IP 192.168.0.10.  

- Qu'observez-vous dans les échanges de données? ...

__5)__ A partir du poste d'IP 192.168.0.10, vous allez envoyer un ping sur les deux autres ordinateurs.  

- Qu'observez-vous dans la __table MAC__ du switch ? ........................................................
  
- Qu'observez-vous dans la table d'échanges de données de l'ordinateur d'IP 192.168.0.10.  
.................................................  

__6)__ Dans la table d'échanges de données, cliquer sur une requête de protocole ARP.  

- En lisant les commentaires, que pouvez-vous dire de l'intérêt de ce type de requête?  
.............................................

__7)__ En regardant l'ensemble des requêtes de protocole ICMP. Il y a les requêtes effectuées par 192.168.0.10 et d'autres. 
   
- Comment appelle-t-on ce type de transfert de données? .......................................................
- Comprenez-vous pourquoi on appelle cela un ping? ............................................ 
- Comment évolue le TTL des requêtes? Est-ce normal?..........................................................  
 

# Exercice n°4: Manipulation d'un réseau avec routeurs.  

Téléchargez le fichier [`reseau-de-reseaux.fls`](./reseau-de-reseaux.fls) est ouvrez-le depuis Filius.  

__1)__ Passer en mode simulation. Analyser la table ARP de l'ordinateur 192.168.0.10.  

* Quelle adresse IP est présente ? ...........................................
* Vérifiez cette information sur d'autres ordinateurs du réseau local  
* Recherchez la signification de [__broadcast__](https://fr.wikipedia.org/wiki/Broadcast_(informatique)) est expliquez son utilité  
...................................................................................


__2)__ Lancer un ping depuis l'ordinateur 192.168.0.11 vers un ordinateur __d'un autre réseau local__. (192.168.1.11 par exemple)  

* Observez vous un découpage en paquets? ...................
* Si oui combien? ..........................
* Observez le Time To Live des paquets? Commentez ................................

__3)__ Les routeurs disposent de plusieurs adresses IP. A votre domicile votre box en posséde en général deux.

* Pourquoi 2 adresses IP ?
* Ces adresses peuvent-elles être identiques d'un réseau local à un autre réseau local ? Différencier les cas.

🎬 [video d'accompagnement Ex 4](https://youtu.be/3hkRhGSSy4U)

