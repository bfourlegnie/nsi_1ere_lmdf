---
title: "Chapitre 12 : Reseaux"
subtitle : "Partie 1 - COURS :  Relation client/serveur sur le Web"
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---


# Le modèle OSI.[^1]  

Au début des années 70, chaque constructeur a développé sa propre solution réseau autour d'architecture et de protocoles privés et il s'est vite avéré 
qu'il serait impossible d'interconnecter ces différents réseaux «propriétaires» si une norme internationale n'était pas établie. 

![Modèle OSI](./fig/osi.png)  


- Le modèle OSI est un __modèle théorique__ servant de guide à la conception des protocoles réseau
- Il divise le processus de réseau en sept couches logiques, chacune comportant des fonctionnalités uniques et se voyant attribuer des services et des protocoles spécifiques.
- Chaque couche ne peux communiquer qu’avec celle du dessus et celle du dessous.

Par exemple l'adressage IP se situe dans la couche 3 (Réseau), le protocole TCP ainsi que l'attribution des ports se situe dans la couche 4 (Transport) et l'interprétation du langage HTML se situe dans la couche 6 (Présentation).

Le Web est basé sur une __architecture client/serveur__ décrite dans la couche 7 de ce modèle : la couche __Application__.

* _Côté client_ : l'application utilisée sera par exemple le navigateur.
* _Côté serveur_ : les logiciels couramment employés de nos jours sont `Apache`, `Nginx` ou encore `Node.js`.
  
# Le protocole HTTP

## Présentation

Les échanges de données entre ordinateurs ont été conçus suivant le modèle client-serveur car dans la plupart des cas, un ordinateur (client) souhaite avoir des ressources situées sur le disque dur d'un autre ordinateur (serveur). (La situation n'est pas symétrique donc on différencie le rôle de chaque ordinateur.) 
Le schéma suivant résume la situation :[^2]

![Architecture client/serveur](./fig/client-server.png)  

- Comme dans tout échange, il faut fixer des règles de communication, c'est le rôle d'un __protocole__

> 📢 Le protocole `HTTP` (_Hyper Text Transfert Protocol_) est un ensemble de règles qui permet au client d'effectuer des __requêtes__ au serveur web. En retour le serveur web va envoyer une réponse en relation avec la requête.  

Vous avez certainement rencontré d'autres protocoles lors de vos navigations comme par exemple :

* `https` (_Hyper Text Transfert Protocol Secure_) combinaison du protocole http et d'une couche de chiffrement 
* `ftp` (_File Transfer Protocol_) permettant de copier des fichiers vers un autre ordinateur
* `ssh` (_Secure SHell_) permettant de communiquer de manière sécurisé, il est basé sur des clés de chiffrement


## URL

Lors qu'on souhaite accéder à une ressource distante depuis un navigateur, on utilise son URL.[^3]

![Exemple d'URL](./fig/url.jpg)  

> 📢 Une `URL` (_Uniform Ressource Locator_) aussi appelée adresse web est une chaîne de caractères qui permet d'identifier une ressource du Web  par son emplacement et de préciser le protocole pour la récupérer.

_Quelques remarques_ :

* `nom de domaine `: c'est le serveur web appelé
* `port` : c'est la "porte" pour accéder à la ressource (cela permet à un ordinateur de distinguer ces différents intercoluteurs). Dans le cas du protocole htpp il s'agit du __port 80__ par défaut : il est inutile en général de le préciser.
* `requête` : Ce sont des paramètres additionnels fournis au serveur web sou sforme de couples clé/valeur séparés par le symbole `&`.

## Requêtes HTTP

Les messages envoyés par le client sont appelés des `requêtes`, ceux envoyés par le serveur des `réponses`.

📝Application

> Visualisons-les avec par exemple l'url http://www.nsi-premiere.fr/test.html en utilisant l'onglet Réseau des outils de développement de Firefox.

Chaque requête possède :

* une __méthode__ 
* un __en-tête__ : on y trouvera des indications sur la ressource demandée, le navaigateur utilisé par le client...
* un __corps__ : on peut y trouver les paramètres passé par un formulaire par exemple

Chaque réponse contiendra :

* une __ligne de statut__ : la version du protocole utilisé, un code d'erreur et un texte associé
* un __en-tête__ : contenant de nombreuses informations comme l'horodatage , le modèle du serveur ....
* un __corps de réponse__ : par exemple le code html de la page à afficher par le client



## Les méthodes de requête associées au protocole http.  

Les méthodes de requêtes indiquent les actions que l'on souhaite réaliser sur la ressource indiquée. Nous allons détailler deux méthodes de requêtes:  

- La __méthode GET__: c'est la méthode la plus courante pour demander une ressource. Elle est sans effet sur la ressource. Les requêtes GET doivent uniquement être utilisées afin de récupérer des données. (Récupérer un fichier HTML, un fichier CSS, une image ...)  

- La __méthode POST__: Cette méthode est utilisée pour soumettre des données en vue d'un traitement (côté serveur). Typiquement c'est la méthode employée lorsque l'on envoie au serveur les données issues d'un formulaire.  

## Formulaire HTML et protocole HTTP

Pour le client, un formulaire est une IHM  permettant de construire une requête HTTP pour envoyer des données à un serveur (voir TP formulaire). 

- Si un formulaire est envoyé vers un serveur avec la méthode GET les données seront explicitement présentes dans l'URL ce qui pose un problème de confidentialité évident.
De plus la longueur d'une URL est parfois limitée (on conseille de ne pas dépasser 2000 caractères ) ce qui empêchera la transmission de longs formulaires par cette méthode
Par exemple  

- Si un formulaire est envoyé vers un serveur avec la méthode POST les données seront incorporées au corps de la requête mais pas dans l'URL.



Pour les deux méthodes de requête, les données ne sont pas sécurisées, il faut utiliser le protocole https pour crypter les données transférées.  

[^1]: https://sti2d.ecolelamache.org/ii_rseaux_informatiques__3_le_modle_de_rfrence_osi.html

[^2]: https://developer.mozilla.org/en-US/docs/Learn/Forms/Sending_and_retrieving_form_data

[^3]: https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/Identifier_des_ressources_sur_le_Web


