---
title: "Chapitre 2 Représentations des entiers naturels"
subtitle : "PARTIE 1 - Cours : Les bases de numération"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


L'ensemble des entiers naturels $`\mathbb{N}`$ regroupe le 0 et les nombres positifs $`\{{0;1;2;3;4\cdots}\}`$.

La notation décimale que nous sommes habitués à utiliser depuis notre enfance n'est pas la seule façon de réprésenter ces nombres, elle est de plus inadaptée aux machines informatiques.


# Ecriture en base 10

## Notation décimale

N'importe quel sytème d'écriture d'un nombre utilise un jeu de symboles (on parle d'_alphabet_).  
Le système de notation décimale est un système __positionnel__ utilisant un alphabet de dix symboles : les dix chiffres  $`\{{0;1;2;3;4;5;6;7;8;9}\}`$.

La position d'un chiffre dans cette représentation indique son poids en puissance de 10, par exemple pour  le nombre  __quatre cent cinquante sept__ :  
  $`\bf \underbrace{4}_{\text{centaines}}\underbrace{5}_{\text{dizaines}}\underbrace{7}_{\text{unités}} = 4\times 100 + 5 \times 10 + 7 \times 1`$ 

Autrement dit :   
  $`\bf 457 = 4\times {\color{red}10^{2}} + 5 \times {\color{red}10^{1}} + 7 \times {\color{red}10^{0}}`$ 


__📝Application__ : _décomposer 345 632 en base 10_ 

  $`345632 = 3\times 10^{5} + 4\times 10^{4}+5\times 10^{3}+6\times 10^{2}+3\times 10^{1}+2\times 10^{0}`$



## Décomposition généralisée dans une base donnée

La représentation décimale d'une nombre __N__ est la suite ordonnée de _n_ chiffres : $`\bf a_{n-1} a_{n-2}\cdots a_{1} a_{0}`$

Comme on l'a vu on peut donc décomposer cette représentation ainsi :  

$`\bf N = a_{n-1} \times 10^{n-1} + a_{n-2} \times10^{n-2}+ \cdots +a_2 \times10^2 +a_1 \times10^1 +a_0 \times10^0`$

Par exemple pour le nombre 253 :  
 $`a_{2} = 2, a_{1} = 5, a_{0} = 3`$ --> n = 3 chiffres sont nécessaires.


On peut généraliser cette décomposition à n'importe quelle base __B__ de numération :  $`\bf N=\sum\limits_{{i=0}}^{n-1}{a_i \times B^i}`$

soit :  $`\boxed{\bf{N =a_{n-1} B^{n-1} + a_{n-2} B^{n-2}+ \cdots +a_2 B^2 +a_1 B^1 +a_0 B^0}}`$

La représentation associée sera notée : $`\bf{(a_{n-1}a_{n-2}\cdots a_{1}a_{0})}_{B}`$ 

__📐 Point mathématique :__  
_Le signe $`\sum`$ se lit "sigma". Il décrit une série de plusieurs additions._  
_Par exemple_  $`N=\sum\limits_{{i=1}}^{4}{2n}`$ _se lit "somme pour __n__ allant de 1 à 4 de __2n__._  
 _Cela signifie que l'on fait prendre au nombre n toutes les valeurs entières entre 1 et 4 et qu'on fait la somme des termes 2n :_   $`N = 2\times 1 + 2\times 2 +2\times 3 +2\times 4 = 14`$


# Ecriture en base 2

L'alphabet binaire comporte uniquement deux symboles __0__ et __1__.  
Ce mode de représentation est utilisé par les systèmes électroniques les plus courants car il traduit simplement le passage ou non d'un courant, l'existence ou non d'une tension dans un circuit...

Lorsqu'il s'agit de représenter un entier naturel, si on écrit 1011 une confusion peut exister.   
On choisira donc de ne rien préciser si on utilise une représentation décimale sinon la base utilisée sera indiquée comme ceci $`\bf (1011)_{2}`$

_Remarque_ : Attention, de nombreuses notations coexistent comme $`1011_{2}`$ ou $`\overline{1011}_{2}`$ ou encore $`\overline{1011}^{2}`$.

## Décomposition et passage vers la notation décimale

On décompose cette représentation ainsi :  
$`\bf N = a_{n-1} \times 2^{n-1} + a_{n-2} \times2^{n-2}+\cdots +a_2 \times2^2 +a_1 \times2^1 +a_0 \times2^0`$ avec $`a_{i} \in  \{0;1\}`$

La position d'un chiffre dans cette représentation indique son poids en puissance de 2.  
Le nombre binaire précédent utilise _n = 4_ symboles

$`\bf (\underbrace{1}_{\text{huitaines}} \underbrace{0}_{\text{quatraines}} \underbrace{1}_{\text{deuzaines}} \underbrace{1}_{\text{unités}})_{2} = 1\times {\color{red}2^{3}} + 0\times {\color{red}2^{2}} + 1 \times {\color{red}2^{1}} + 1 \times {\color{red}2^{0}}`$ 

Soit, si on donne la représentation en notation décimale : 

$`\bf (1011)_{2} = 1\times 8 + 0\times 4 + 1 \times 2 + 1 \times 1 =  8 + 0 + 2 +1 = 11`$ 

On constate que pour représenter le nombre onze en notation décimale on utilise 2 symboles contre 4 en notation binaire.  

_Remarques_ :   
* Ces notations sont à longueur variables (peu de symboles pour un petit nombre, plus pour un plus grand)   
* Plus la base à un alphabet conséquent, moins il faudra de symboles pour représenter un nombre.   

Enfin, il peut être intéressant de connaitre quelques puissances courantes de 2 

|$`\bf n`$|0|1|2|3|4|5|6|7|8|  
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|  
|$`\bf 2^{n}`$|1|2|4|8|16|32|64|128|256|  

## De la notation décimale à la notation binaire

L'écriture d'un nombre décimal en binaire s'appuie sur sa décomposition en puissance de 2 (en choisissant à chaque fois la plus grande possible).

$`42 = 32 + 8 + 2 = 2^{5} + 2^{3} + 2^{1}`$  

Toutes les positions de 0 à n-1 (ici 5) doivent contenir un symbole ce qui donne :

$`42 = \boxed1 \times 2^{5} + \boxed0 \times 2^{4} + \boxed1 \times 2^{3} +\boxed0 \times 2^{2} +\boxed1 \times 2^{1} + \boxed0 \times 2^{0}`$  

soit $`42=(101010)_{2}`$


__📌 Méthode :__

> On réalise des divisions euclidiennes successives par 2 jusqu'à obtenir un quotient nul.    
La représentation binaire est obtenue en lisant les restes du dernier vers le premier.

![divisions successives](./fig/divisions_successives.jpg)  

[^1] On retrouve bien le résultat précédent

## Représentation en machine

__📢 Définitions :__  
> Un __bit__ (binary digit) correspond à l'unité d'information apportée par une variable binaire.

En mémoire machine la représentation d'un entier naturel sera donc stockée sur plusieurs __bits__ 

> L'assemblage de plusieurs symboles binaires constitue un __codet__ (l'équivalent d'un mot).

Dans nos langues les mots ont une longueur variable, en informatique pour une question d'efficacité cette longueur est de taille fixe multiple d'un octet.
 
> Un __octet__ est une séquence ordonnée de 8 bits

Les mots utilisés pour coder les entiers sont couramment de taille __8__, __16__, __32__ ou __64__ bits.
Ceci entraine limitation sur la taille des entiers naturels codés.  

__Quels entiers peut-on coder avec 8 bits ?__   
* L'entier naturel de valeur minimale est $`(00000000)_{2}`$ soit 0   
* L'entier naturel de valeur maximale est $`(11111111)_{2}`$ soit 255   

On peut généraliser cette démarche à n'importe quelle taille de mots.

__📢 Définition :__  
> __n__ bits permettent de représenter tous les entiers naturels compris entre __0__  et $`\bf 2^{n}-1`$  


__📝Application__:   
Si on reprend l'exemple précédent,  le codage des entiers est effectué sur n = 8 bits.  
La valeur minimale est bien 0, et la maximale : $`2^{8}-1 =256-1 =255`$

__Il est donc important de préciser sur combien de bits sont codés les entiers manipulés.__

_"Il y a 10 sortes de personnes : celles qui comprennent le binaire et celles qui ne le comprennent pas."_

# Ecriture en base 16 (hexadécimale)

## Intérêt

Pour la base 16 les symboles utilisés sont les 10 chiffres et les 6 premières lettres de notre alphabet.  

|alphabet hexadécimal|0|1|2|3|4|5|6|7|8|9|A|B|C|D|E|F|  
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|  
|équivalence en décimal|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|  

Cette notation simplifie la représentation des nombres car moins de symboles seront nécessaires pour représenter un nombre qu'en décimal par exemple.  
Elle sera notamment utilisée pour représenter les données stockées en machine de manière plus lisible pour un humain.

## Décomposition

On décompose cette représentation ainsi :  

$`\bf N = a_{n-1} \times 16^{n-1} + a_{n-2} \times16^{n-2}+ ..... +a_2 \times16^2 +a_1 \times16^1 +a_0 \times16^0`$

Le principe de passage de l'hexadécimal vers le décimal est donc le même que celui du binaire vers le décimal si ce n'est que l'on remplacera les lettres utilisées par les valeurs décimales correspondantes.

$`\bf (F2)_{16} = 15\times {\color{red}16^{1}} + 2\times {\color{red}16^{0}} = 240 + 2= 242`$ 

__📝Application__ : _décomposer $`\bf (10C)_{16}`$ en base 10_ 

  $`\bf (10C)_{16} = 1\times 16^{2} + 0\times 16^{1}+12\times 16^{0} = 1\times 256 + 0\times 16 + 12\times 1 = 268`$


## De l'hexadécimal vers le binaire

Tout groupe de 4 bits peut être représenté par un seul caractère hexadécimal.

En effet :
* la valeur minimale avec 4 bits en binaire est $`(0000)_{2}`$ soit   $`\bf (0)_{16}`$  
* la valeur maximale est $`(1111)_{2}`$ soit   $`\bf (F)_{16}`$  


__📌 Méthode :__  
> Pour passer du binaire à l'hexadécimal, on regroupera donc les paquets par 4 

__📝Application__ : $`\bf (\overbrace{1011}^{\text{B}} \overbrace{1110}^{\text{E}})_{2}  = (BE)_{16}`$ 


# 💾 Utilisation des bases numériques en Python

## Préfixes et notations

En Python, on peut directement écrire les nombres en binaire en utilisant le préfixe __`0b`__

```python
>>> 0b101010
42
```

En effet :  $`{(101010)}_{2}=42`$  

Pour les nombres hexadécimaux : le préfixe utilisé est __`0x`__

```python
>>> 0x2A
42
```
Là aussi :  $`{(2A)}_{16}=42`$

##  Les opérateurs de comparaison

Nous souhaitons faire la comparaison du contenu de 2 représentations nommées X et Y grâce à la syntaxe suivante : `X operateur Y`
    
Voici, ci-dessous le lien entre les symboles mathématiques et les opérateurs python correspondants
   
|Symbole mathématique|Opérateur python|  
|--------------------|----------------|  
|$`=`$                 |`==`            |  
|≠                     |`!=`            |  
|$`>`$                 |`>`             |  
|$`\geq`$              |`>=`            |  
|$`<`$                 |`<`             |  
|$`\leq`$              |`<=`            |  


Par exemple l'expression  `12 > 13` sera évaluée comme la réponse à la question *"Est-ce que le nombre entier 12 est supérieur au nombre entier 13 ?"*  

```python
>>> 12 > 13
False
```

L'évaluation de telles expressions conduit nécessairement à deux réponses possibles Vrai ou Faux : en langage Python `True`  ou `False`.


```python
>>> 14 >= 13
True
```

Ces deux valeurs particulières appartiennent à un nouveau type de données : les __booléens__ (_cette notion sera approfondie ultérieurement_)


```python
>>> type(True)
<class 'bool'>
```

On peut notamment vérifier l'égalité des représentations du nombre 42 en bases binaire et hexadécimale.

```python
>>> 0b101010 == 0x2A
True
```

[^1]: Conversion décimal-Binaire https://zestedesavoir.com




