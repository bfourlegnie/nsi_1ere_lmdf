---
title: "Chapitre 2 Représentations des entiers naturels"
subtitle: "TP n°1 : les bases"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


__CONVERSIONS__


# Exercice n°1 : Du binaire au décimal &#x1F3C6;

__Convertir en décimal les nombres binaires suivants :__   

__a-__ $`(1010)_{2}`$ 

__b-__ $`(11001)_{2}`$  

__c-__ $`(10001101)_{2}`$   

__d-__ $`(1111010111)_{2}`$  

__2)__ Vérifications (_par 2 méthodes_)

* Vous pouvez vérifier vos résultats à l'aide de la calculatrice windows réglée en mode Programmeur

![calculatrice en mode programmeur](./fig/calculatrice.png)

* Il est également possible d'utiliser la fonction `int(chaine,base)` ou `chaine` représente la séquence de caractères à convertir et `base`la base de départ. La valeur de retour sera le nombre entier exprimé en base 10.
Par exemple : 
```python
>>> int('101', 2)
5
```


# Exercice n°2 : Du décimal au binaire &#x1F3C6;

Convertir en binaire les nombres décimaux suivants :  

__1)__ $`37`$  
__2)__ $`78`$   
__3)__ $`189`$   
__4)__ $`205`$  


# Exercice n°3 :  Horloge binaire &#x1F3C6; &#x1F3C6;

![Horloge binaire](./fig/binary_clock.png)  
[^1]

L'horloge binaire ci-dessus affiche __10:37:49__  
__a)__ Expliquez.  
__b)__ Justifiez le nombre de disques dans chaque colonne   

# Exercice n°4 :  De l'hexadécimal au décimal &#x1F3C6; 

Convertir en décimal les nombres hexadécimaux suivants :  

__a)__ $`(23)_{16}`$  
__b)__ $`(A0)_{16}`$   
__c)__ $`(1010)_{16}`$   
__d)__ $`(DD)_{16}`$  

# Exercice n°5 :  Codage des couleurs &#x1F3C6; &#x1F3C6;

La restitution des couleurs par un écran est basé sur le principe de la synthèse additive de trois Lumières colorées: Rouge, Vert et Bleu.  
Chacune de ces composantes est codé sur 8 bits, on parle de codage RVB (_ou RGB en anglais_) 24 bits  
En binaire cela donne par exemple pour du rouge :  
 $`\bf (\underbrace{11111111}_{\text{rouge}}\underbrace{00000000}_{\text{vert}}\underbrace{00000000}_{\text{bleu}})_{2}`$  

 __1)__   
__a)__ Donner le codage équivalent en décimal sous la forme d'un triplet (r,v,b)   
__b)__ Pour une composante (R ou V ou B) combien de nuances de couleur peuvent être codé avec ce sytème ?  
__c)__ En déduire le nombre total de couleurs pouvant être représentées sur 24bits.    

 __2)__ Combien de caratères hexadécimaux seraient nécessaires pour chaque composante ?  Donner le codage équivalent en base 16.  

 __3)__ En cherchant dans le code d'une page web on remarque qu'une des couleurs utilisée est codée ainsi __#00FFFF__.  
 Le symbole `#` signifie que le codage est réalisé en hexadécimal.  
 __Quelle est cette couleur ?__  


# Exercice n°6 :  Du binaire à l'hexadécimal &#x1F3C6; 

Convertir en hexadécimal les nombres binaires suivants :

__1)__ $`(11110000)_{2}`$  
__2)__ $`(10101101)_{2}`$   
__3)__ $`(101111010001)_{2}`$   
__4)__ $`(110000111100)_{2}`$  

# Exercice n°7 :  Du décimal à l'hexadécimal &#x1F3C6;  &#x1F3C6;
En adaptant la méthode des divisions successives vue en cours, convertir en hexadécimal les nombres décimaux suivants :

__1)__ $`512`$  
__2)__ $`2000`$   
__3)__ $`12600`$   
__4)__ $`16000`$  

# Exercice n°8 : Codage et taille des entiers &#x1F3C6; 

__1)__ Quels entiers naturels peut on représenter avec 16, 32, 64 bits ?    
__2)__  Dans la plupart des langages de programmation les entiers sont codés sur un nombre fixe de bits.    
__a-__ En Python, il n'existe pas de taille maximale pour les entiers, on peut le vérifier.    
Evaluer grâce au Shell un entier de valeur supérieur à la valeur précédente.  
__b-__ On peut par contre imposer une représentation qui limite cette taille à l'aide d'un module dédié.    

* Tapez le code suivant

```python
>>> import numpy
>>> x = numpy.uint8(12)
```
Le module numpy permet de travailler avec des entiers de taille fixée ici `numpy.uint8()` impose un codage de l'entier 12 sur 8 bits.

* vérifiez-le en testant le type.

```python
>>> type(x)
```
* Que se passe-il si on dépasse la valeur maximale autorisée pour un codage 8 bits ? Testez-le avec les nombres suivants

```python
>>> numpy.uint8(256)
```

```python
>>> numpy.uint8(258)
```


```python
>>> numpy.uint8(-5)
```

Concluez.



# Exercice n°9 : Evaluations d'expressions booléennes &#x1F3C6;

Evaluez les expressions suivantes et expliquez leur résultat

_Remarque : Aucune parenthèses n'est utilisé dans les expressions suivantes car l'opérateur compare "tout ce qui est écrit à droite à tout ce qui est à gauche"_  

|Expression|Evaluation|Explication &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|  
|:---------|:--------:|:----------|  
| 33 <= 50  |          |         |   
| 15 != 2  |          |           |   
| 14 == 3  |          |           |   
| 10 == '10'|          |           |   
| 2 * 20 > 12 +2  |          |           |   
| "booléen" == 'Booléen'  |          |           |   
| 'NSI' == 'N' + 'S' + 'I'|          |           |   


# Exercice n°10 : Représentations des nombres entiers en binaire &#x1F3C6;

__1) Sans ordinateur, évaluez les expressions suivantes et détaillez les calculs vous conduisant à ce résultat__  
 Vous effectuerez une vérification à la fin.


|Expression|Evaluation|Détail des calculs  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
|:---------|:--------:|------|
| 0b1111 >= 15  |          ||
| 0b1101 + 0b10 == 15  |          ||
| 0b101 + 12 > 17  |          ||
| 0b11111 * 2 < 60|          ||



__2)__ Python permet également d'obtenir la notation préfixée en binaire sous forme de chaîne de caratères à partir d'un entier écrit en décimal à l'aide de la fonction `bin`.

```python
>>> bin(3)
'0b11'
```

__Sans ordinateur, évaluez les expressions suivantes et détaillez les calculs vous conduisant à ce résultat__  
 Vous effectuerez une vérification à la fin.  

 |Expression|Evaluation|Détail des calculs  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
|:---------|:--------:|----|
| bin(5) == '0b101' |          ||
| bin(5) == 0b101 |          |        ||
| bin(256) == '0b100000000' |          ||
| bin(1+1) != '0b11'  |          ||
| bin(35) == '0b100001'  |          ||


# Exercice n°11 : Pair ou impair ? &#x1F3C6; &#x1F3C6; &#x1F3C6;
__1)__ 🥇 Donnez un critère rapide permettant de savoir si, à partir d'une représentation d'un nombre entier en binaire, ce nombre est pair ou impair.  

__2)__ 🥇 Donnez une expression Python qui utilise un opérateur de comparaison et un opérateur mathématique bien choisi permettant de savoir si un nombre exprimé en décimal est pair ou non.  



# Exercice n°12 : Base 8  &#x1F3C6; &#x1F3C6;


Les avions possédent des transpondeurs pour aider à les identifier sur le contrôle du trafic aérien radar  

![Transpondeur](./fig/cessna_transponder.jpg)  
[^2]

Ces transpondeurs transmettent quatre nombres à un chiffre compris entre 0 et 7 : ce système est dit octal (__base 8__)  

__1)__ Le code  `7500` signifie détournement d'avion.   
En utilisant vos connaissances sur la base 2, démontrez l'égalité suivante avec le nombre exprimé en décimal $`{(7500)}_{8}=3904`$  

__2)__ On peut le vérifier en Python en utilisant le préfixe `0o`  

```python
>>> 0o7500
3904
```
Donnez les valeurs décimales des nombres suivant (__détaillez vos calculs et n'utilisez l'interpréteur qu'à la fin pour vérifier__)  
* $`{(0033)}_{8}`$  
* $`{(0600)}_{8}`$  
* $`{(7700)}_{8}`$  

__3)__ Quel est le code le plus grand utilsable par les transpondeurs en base 8. Donnez son équivalent en base 10.  

__4)__ Combien de codes différents sont possibles avec ce système ?   



[^1]: [Visual explanation of a binary clock by Alexander Jones & Eric Pierce](https://commons.wikimedia.org/wiki/File:Binary_clock.svg?uselang=fr)

[^2]: [Cessna ARC RT-359A transponder and Bendix/King KY197 VHF communication radio](https://commons.wikimedia.org/wiki/File:CessnaARC-RT-359ATransponder04.jpg?uselang=fr)
