---
title: "Chapitre 2 Représentations des entiers naturels"
subtitle : "PARTIE 2 - Cours : Boucles tant que et algorithmes"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Algorithmes et changements de bases  

__📢 Définition :__

> Un  __algorithme__ est  une  procédure  de  résolution  de  problème qui  produit, en un nombre fini d’étapes constructives, effectives, non-ambigües  et  organisées,  la  réponse  au  problème  pour  toute instance de ce dernier.

On souhaite écrire un entier naturel __n__ dans une base __b__.  
Voici un algorithme décrivant la suite d'étapes à réaliser.  

```
Entrée : b la base de numération, n un entier naturel.
Sortie : a0, a1,. . . les chiffres de l’écriture de n en base b.

1:  m ← n, i ← 0
2:  tant que m ≥ b faire
3:     r ← m (mod b)        // mod correspond à l'opérateur modulo
4:     m ← m ÷ b            // la division est entière car m est un entier
5:     ai ← chiffre correspondant à r
6:     i ← i + 1
7:  fin tant que
8:  ai ← chiffre correspondant à m
9:  renvoyer a0, a1, . . . , ai
```

__📢 Définition :__  

> L'algorithme présenté utilise du __pseudo-code__:  une manière de décrire un algorithme dans un langage presque naturel utilisant quelques conventions :  
    *  __Entrée__ : données néccessaires au fonctionnement de l'algorithme  
    * __Sortie__ : données fournies par l'algorithme    
    * __←__  symbolise l'_affectation_  
    * Indentation utilisée pour distinguer les blocs de code (conditions, boucles...)  
    * __//__  symbolise un commentaire     


On constate la présence d'une boucle de répétition __tant que__.  
Cela signigie que _"tant que la condition $`m \ge b`$ sera réalisée"_ toutes les instructions, présentes dans le bloc de code indenté, entre _"faire"_ et _"fin tant que"_ seront exécutées en boucle.  

Analysons le déroulement de cet algorithme avec un exemple :

* Entrée : __n=10__ et __b= 2__  

|                          | r   | m  |$`\bf a_{i}`$| i  |  
|:------------------------:|:---:|:--:|:--------:|:--:|  
|_etape 1 (initialisation)_|     |10  |          | 0  |  
|_boucle tour n°1 ($`10 \ge 2`$)_  |0    |5   |$`a_{0}=0`$| 1  |  
|_boucle tour n°2 ($`5 \ge 2`$)_   |1    |2   |$`a_{1}=1`$| 2  |  
|_boucle tour n°3 ($`2 \ge 2`$)_   |0    |1   |$`a_{2}=0`$| 3  |  
|_etape 8_                 |     |    |$`a_{3}=1`$|    |  

* Sortie : __0101__

La donnée fournie en sortie est $`a_{0}a_{1}a_{2}a_{2}`$, en remettant dans le bon ordre on a bien la représentation en binaire du nombre décimal 10 soit $`{(a_{3}a_{2}a_{1}a_{0})}_{2}`$ soit $`\bf {(1010)}_{2}`$


# 💾 La Boucle tant que en Python  

Voyons comment implémenter une boucle tant que en Python

```python
>>> x = 0
>>> while x < 3:
      x = x + 1
      print(x)
    print('Fin')
1
2
3
'Fin'
```

Le but de ce programme très simple est d'__afficher__ les nombres entiers allant de 1 à 3 en utilisant une fonction native de Python : `print`.    
_Note : nous reviendrons plus en détail sur cette fonction dans quelque temps_

Analysons le code :   
* Une variable `x` est initialisée avec la valeur `0`   
* L'instruction `while(x < 3)` signifie *tant que x est inférieur à 3*.    
* On retrouve juste après un bloc d'instructions indenté qui s'exécutera __tant que__ la condition `x < 3` est vérifiée autrement dit si l'expression correspondante est évaluée à `True`

On peut réaliser un tableau de suivi:    

|                          | x   | affichage  |   
|:------------------------:|:---:|:----------:|  
|_etape 1 (initialisation)_|0    |  |  
|_boucle tour n°1 (0<5)_   |1    |1   |  
|_boucle tour n°2 (1<5)_   |2    |2   |  
|_boucle tour n°3 (2<5)_   |3    |3   |  

La condition `x < 5` est ensuite vérifiée une dernière fois et est évaluée à : `False`.  
Le bloc d'instructions est alors ignoré et le programme se poursuit par l'affichage de la chaîne de caractères `'Fin'`

On peut retenir que la boucle while a la structure suivante :

```python
while <condition>:
    <instructions a exécuter si la condition est vraie>
```




