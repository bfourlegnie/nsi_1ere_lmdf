---
title: "Chapitre 6 : Boucles bornees"
subtitle : "PARTIE 1 Cours : les boucles for"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Vous avez déjà rencontré la boucle TANT QUE (`while`) qui permet de répéter un bloc d'instructions tant qu'une condition est vérifiée.  
Il en existe une autre : la boucle POUR (`for`).

# Structure de la boucle POUR en Python

```python
for variable in sequence:
    <instruction 1>
    <instruction 2>
    ...
```

La boucle `for...in` est une boucle inconditionnelle : en effet aucune expression booléenne n'est utilisée.  
  
De plus c'est une boucle __bornée__ : le nombre d'itérations de la boucle est connu à l'avance. Elle permet de parcourir des séquences d'éléments.  

_Premier exemple_:  

```python
>>> for _ in range(3):
        print('un tour de boucle')

 un tour de boucle
 un tour de boucle
 un tour de boucle
```

Cette structure simple permet de réaliser trois répétitions de l'instruction `print('un tour de boucle')`.  

_Quelques commentaires_ :

* Le caractère blanc souligné `_` (_underscore_ en anglais) fait partie de la construction : on explicitera son utilisation ultérieurement.  
  
* l'instruction `range` accepte un paramètre correspondant tout simplement à un nombre entier de répétitions.   
  
* remarquez l'utilisation des `:` et de l'indentation pour délimiter le bloc d'instructions à réaliser.  


# Utilisation d'une variable de boucle

Dans l'exemple précédent la variable de boucle `_` n'était pas réutilisée dans les instructions du corps de la boucle : c'est uniquement dans ce cas qu'on emploiera ce caractère spécial.  
 Mais bien évidemment on peut rencontrer d'autres situations :  

```python
>>> for i in range(4):
        carre = i * i
        print(carre)
 0
 1
 4
 9 
```

Ici, la séquence à balayer générée par `range(4)` est un intervalle de quatre entiers 0, 1, 2 puis enfin 3 : ce sont les valeurs prises par la variable `i` lors du parcours de la séquence.   

La variable `carre` prendra donc successivement la valeur du carré des nombres précédents.  


On peut visualiser ce qui se passe à chaque tour de boule à l'aide de 🎓[Python Tutor](http://pythontutor.com/visualize.html#code=for%20i%20in%20range%284%29%3A%0A%20%20%20%20carre%20%3D%20i%20*%20i%0A%20%20%20%20print%28carre%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)   



# Aller plus loin avec l'instruction range

La fonction `range` en Python génère une suite arithmétique croissante ou décroissante de nombres entiers. 
Ainsi, la variable de boucle prendra successivement pour valeur les éléments de cette suite à chaque itération : [Rappels sur les suites arithmétiques](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tique)

La syntaxe complète est la suivante `range(debut, fin, pas)` :   

* Le premier paramètre `debut` fixe le premier terme de la suite arithmétique. Si ce premier paramètre n'est pas précisé, la valeur sera de __0__ par défaut.  
  
* Le deuxième paramètre `fin` fixe le dernier terme de la suite. Attention, la variable ne prendra pas la valeur de ce terme. __Ce paramètre est obligatoire.__  
  
* Le troisième terme `pas` fixe la raison de la suite arithmétique. Si la valeur du paramètre n'est pas précisée, par défaut la raison sera de 1.  


__📝 Application n°1__ :

Si vous voulez créer une séquence qui soit une suite arithmétique: 0, 1, 2, 3, 4 vous devez écrire la fonction `range` avec les paramètres suivants.   __range(5)__   
En effet, comme il n'y a qu'un paramètre de fixé, cela correspond au deuxième paramètre qui est obligatoire soit le dernier terme de la suite (dont la valeur ne sera pas prise par la variable). On sait donc que le premier terme est 0, le dernier est 4 et la raison 1.  

```python
>>> for i in range(5):
        print(i, end=" ")

 0 1 2 3 4 
```


> 💾 _Remarque_ : le second argument `end` utilisé dans la fonction `print()` permet de changer le caractère de fin utilisé après l'affichage (à chaque tour). Par défaut il s'agit d'un saut de ligne, on le remplace ici par un espace ` `.


__📝 Application n°2__ :

Pour la suite 3, 6, 9, 12, 15, 18, on doit avoir __range(3, 21, 3)__. Il faut impérativement imposer les trois paramètres pour réaliser cette suite.

```python
>>> for i in range(3, 21, 3):
        print(i, end=" ")

 3 6 9 12 15 18 
```


# Utilisation d'un accumulateur

Etudions l'exemple suivant :  

```python
a = 1
for _ in range(3):
    a = a + 2
    print(a, end=" ")

 3 5 7 
```

Le bloc d'instructions sera répété trois fois, à chaque tour la valeur de la variable `a` (qui n'est pas la variable de boucle) progresse.

|étapes|valeur de a|   
|:---:|:---:|   
|initialisation (_avant la boucle_)|1|  
|_tour n°1_|3|  
|_tour n°2_|5|  
|_tour n°3_|7|  

Cette variable est habituellement appelée __accumulateur__ : elle permet de stocker progressivement le résultat d'un calcul.



__📝 Application__ :  
(_Extrait de "Numérique et Sciences informatiques"  Balabonski, Conchon, Filiâtre, Nguyen_)

On souhaite calculer la moyenne des notes de n élèves :

```python
n = int(input("Entrer le nombre d'élèves : "))
somme = 0
for _ in range(n):
    k = int(input("Entrez une note : "))
    somme = somme + k
moyenne = somme / n
print("La moyenne est", moyenne)
```
L'accumulateur utilisé ici est la variable `somme` il contient la somme des notes. Il est préalablement initialisé avec la valeur 0. A chaque tour de boucle il stocke la somme des notes entrées (depuis le début de la boucle).  
Une fois le nombre de répétitions `n` atteint, on réutilise cet accumulateur pour calculer la moyenne.  
