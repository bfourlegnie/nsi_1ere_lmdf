---
title: "Analyse de code"
---


On considére l'énoncé suivant

> _Dans cet exercice, on ne cherchera pas à vérifier le type des valeurs entrées par l'utilisateur_
> __Ecrire un programme qui demande trois entiers à l'utilisateur, puis affiche ces trois entiers dans l'ordre décroissant__


Pour chacun des codes proposés, Expliquer ce qui est faux (syntaxe, consignes non respectées..) ou inutile et pourquoi. Seule une partie du code est présentée....


# code n°1

```python
def decroissant(a, b, c):
    if a > b  and b > c:
        print(a, b, c)
    # suite du code .......
```


# code n°2

```python
a = input('Proposez un entier')
b = input('Proposez un entier')
c = input('Proposez un entier')
if a > b  and b > c:
    print(a, b, c)
# suite du code....
```


# code n°3

```python
# a, b, c ont été saisis correctement par l'utilisateur  
if a > b  and b > c:
    return (a, b, c)
#suite du code
```


# code n°4

```python
# a, b, c ont été saisis correctement par l'utilisateur  
if a > b  and b > c:
    return abc
#suite du code
```


# code n°5

```python
# a, b, c ont été saisis correctement par l'utilisateur  
if a > (b and c) and b > c:
    print(a, b, c)
#suite du code
```


# code n°6

```python
# a, b, c ont été saisis correctement par l'utilisateur  
If a > b > c:
    print(a, b, c)
Elif
    If b > a > c:
    print(b, a, c)
#suite du code
```


# code n°7

```python
def ordre_croissant(a, b, c):
    a = int(input('Proposez un entier'))
    b = int(input('Proposez un entier'))
    c = int(input('Proposez un entier'))
if a > b and a > c and b > c:
    print(a,">", b,">", c)
# suite du code....
```


# code n°8

```python
a = int(input('Proposez un entier'))
b = int(input('Proposez un entier'))
c = int(input('Proposez un entier'))
if c < b and b < a :
    print(c, b, a)
# suite du code....
```

# code n°9

```python
# a, b, c ont été saisis correctement par l'utilisateur  
if a > b > c
    print(a, b, c)
elif b > a > c
    print(b, a, c)
#suite du code
```

# code n°10

```python
a = int(input("Entrez la valeur du premier nombre entier : "))
b = int(input("Entrez la valeur du deuxième nombre entier : "))
c = int(input("Entrez la valeur du troisième nombre entier : "))
if a < b :
    if b < c :
        print(c, b, a)
    elif a < c :
        print(b, c, a)
    else: 
        print (b, a, c)
else :
    if  a < c :
        print(c, a, b)
    elif b < c :
        print(a, c, b)
    else :
        print (a, b, c)
```