---
title: "Chapitre 14 : Représentations des caractères en machine "
subtitle: " TP "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : QCM  &#x1F3C6;
_(Question 1 extraite de QCM sujet 0 prime, Questions 2,4 et 5 extraites de formation DIU Lille[^1] )_


__1.__  Quelle est l'affirmation vraie concernant le codage UTF-8 des caractères ?  

* [ ] Le codage UTF-8 est sur 1 à 4 octets 
* [ ] Le codage UTF-8 est sur 8 bits 
* [ ] Le codage UTF-8 est sur 8 octets 
* [ ] Le codage UTF-8 est sur 7 bits


__2.__ Lequel de ces noms ne correspond pas à un systéme d'encodages de texte ?  

* [ ] FAT-32
* [ ] UTF-8
* [ ] Latin-1
* [ ] UTF-16


__3.__  Quelle affirmation est vraie concernant la suite de bits `10001001` ?  

* [ ] Elle représente l'entier naturel 136 exprimé sur 8 bits
* [ ] Elle représente le caractère `A` en ASCII
* [ ] Elle représente -119 codé en complément à deux sur 8 bits
* [ ] Elle est équivalente à la représentation `FF` en hexadécimal


__4.__ Sur combien d'octets a été codé la chaîne de caractères suivante en utf-8 ?  
_égalités_

* [ ] 10
* [ ] 7
* [ ] 16
* [ ] 1

__5.__ D'après l'extrait de de table la table ASCII ci-dessous, à quel mot correspond ce code binaire :  `010000100100000101000011` ?  

| Dec | Hex | Caractère |
|-----|-----|-----------|
| 065 |  41 |     A     |
| 066 |  42 |     B     |
| 067 |  43 |     C     |  


* [ ] BAC
* [ ] ABC
* [ ] CCB
* [ ] ABB


# Exercice 2  : Unicode et Python &#x1F3C6;&#x1F3C6;

__1)__ Consultons l’aide sur la fonction`chr`

```python
>>> help (chr)
Help on built-in function chr in module builtins:

chr(...)
    chr(i) -> Unicode character

    Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.
```

L’aide nous apprend que cette fonction renvoie une chaîne de un caractère dont le numéro (ordinal) Unicode passé en paramètre est compris entre 0 et 0x10ffff.

Par exemple la caractère de point de code __U+262E__ peut être affiché de deux manières:

```python

>>> chr (0x262e)  # soit en utilisant la notation hexadécimale
'☮'

>>> chr (9774) # soit en utilisant la notation décimale équivalente
'☮'
```

__Afficher ainsi le caractère `Შ` de point de code U+1CA8 : vous trouverez d'abord la valeur décimale équivalente__

__2)__  L'espace Unicode est divisé en 17 zones contenant chacune 65 536 points de codes.   
Ces zones sont appelées plans eux même divisés en sous-ensembles.    
Vous pouvez les retrouver à l’adresse suivante par exemple: https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode 

Le sous-ensemble [__Cyrillique__](https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode/U0400) est par exemple situé entre les points de code `U+0400` et `U+04FF`

Concevez un script en Python permettant d’afficher l’intégralité de ces symboles comme ceci

![Cyrillic](./fig/cyrillic.jpg)

__Sauvegardez votre script sous le nom `script_cyrillic.py`__

__3)__ Recherchez dans le plan 0 appelé aussi [__plan Multilingue de Base__](https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(0000-FFFF)) les codes des sous-ensembles allant de __tagal__ à __tabganoua__.

Construisez une liste Python nommée `unicode` contenant des triplets de la forme `(nom_du_sous_ensemble, point_de_code_min, point_de_code_max)` pour ses sous-ensembles.

avec  :
* _nom_du_sous_ensemble_ : une chaine de caractère
* _point_de_code_min_   / _point_de_code_min_  : des valeurs décimales

Structure de la liste à construire (_ici les valeurs ne sont pas celles recherchées !_):

```python
unicode = [('latin de base', 0, 127), (.., .., ..), etc...]
```

🥇 Définissez enfin une procédure `affichage_unicode` qui acceptera un paramètre : _nom_du_sous_ensemble_ et qui affichera les caractères correspondant en utilisant la variable `unicode`.

![tagal](./fig/affichage_unicode.jpg)


_Remarque_ : Parfois le caractère obtenu par la fonction chr se présente sous forme d’un carré contenant un code hexadécimal ੄ : cela se produit lorsque le dispositif qui produit l’affichage ne connaît pas le glyphe associé au caractère. __Un glyphe est une représentation graphique d’un signe typographique.__

__AIDE__ On pourra s'aider d'une fonction annexe de recherche de l'indice auquel se trouve le bon triplet dans la liste

__Sauvegardez votre script sous le nom `script_unicode.py`__  en n'oubliant pas de réaliser une __docstring complète__ pour chacune de vos fonctions (les tests ne sont pas demandés ici)



[^1]: https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/raw/master/qcm/qcm.md