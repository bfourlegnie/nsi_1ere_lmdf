---
title: "Chapitre 4 : Opérateurs booléens et structures conditionnelles"
subtitle: "TP "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice n°1 : Tables de vérité &#x1F3C6;

__1)__ En considérant deux propositions notées __P__ et __Q__, dressez les tables de vérité correspondants aux expressions suivantes.

__a)__ $`(\lnot P) \land Q`$  

__b)__ $`(\lnot P) \lor Q`$  

__c)__ $`(\lnot P) \land (\lnot Q)`$  

__2)__ Donner les expressions booléennes pythons équivalentes.



# Exercice n°2 : Expressions booléennes &#x1F3C6;

Evaluez les expressions suivantes et expliquez leur résultat

|Expression|Evaluation|Explication &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|  
|:---------|:--------:|:----------|  
| 15 <= 20 or 1 > 150   |          |         |   
| 2 < 4  and 2 < 3 |          |           |   
| "A" == "A" and "B"=="B"  |          |           |   
| not (1 < 3)|          |           |   
| not (15 <= 20) or 1 < 150  |          |           |   



# Exercice n°3 : QCM Structures conditionnelles &#x1F3C6;

Pour chaque cas, indiquez __quelle est la valeur de x__ après exécution du code fourni.  

Une __seule réponse est correcte__ (l'utilisation de l'ordinateur est interdit).

__Question 1__  

```python
x = 2
if x >= 2:
    x = x + 1
else:
    x = x - 1
```
Entourez la bonne réponse :  1 - 2  - 3 - 4

__Question 2__  

```python
x = 4
if x < 5:
    x = x % 2
else:
    x = 2
if x == 0:
    x = 1
else:
    x = 2
```
Entourez la bonne réponse :  1 - 2  - 3 - 4 

__Question 3__  

```python
x = 1
y = 2
if x < y and x > 2:
    x = 3
elif x > y or x >= 1:
    x = 2
else:
    x = 0
```
Entourez la bonne réponse :  0 - 1 - 2  - 3

__Question 4__  

```python
x = 0
if x < 2:
    x = x + 1
if x < 3:
    x = x * 4
if x < 4:
    x = x + 1
```
Entourez la bonne réponse :  0 - 1  - 4 - 5


# Exercice n°4 : Predicats &#x1F3C6; &#x1F3C6;

> 💾 Un __predicat__ est une fonction dont la valeur de retour est un booléen

Par exemple la fonction suivante `is_positive()` renverra `True` si l'argument passé en paramètre est positif, `False` dans le cas contraire.

```python
def is_positive(n):
    return n > 0
```

```python
>>> is_positive(12)
True
```

```python
>>> is_positive(-5)
False
```
_Remarques_ :

* dans le cas présenté, aucune précaution n'est prise pour vérifier que le paramètre est bien un nombre ! Ce n'est pas l'objectif de l'exercice.  
*  💾 L'instruction `return` stoppe immédiatement le déroulement d'une fonction et donc de toutes les autres instructions dans cette fonction. 


__Ecrire les prédicats suivants et testez-les.__

__1)__   Définissez un prédicat `est_nul` renvoyant `True` si le nombre passé en paramètre est égal à zero, `False` sinon.  

__2)__  Définissez un prédicat `est_obtus` renvoyant `True` si l'angle exprimé en ° passé en paramètre est obtus, `False` sinon. (On considérera uniquement les angles compris entre 0 et 180°) 

__3)__  Définissez un prédicat `est_voyelle` renvoyant `True` si le caractère passé en paramètre est une voyelle (_peut importe la casse_) , `False` sinon.  

__4)__    
__a-__ Définissez un prédicat `is_liquid_water` renvoyant `True` si le nombre passé en paramètre est une température exprimée en °C à laquelle l'eau est liquide, `False` sinon(_on considérera une pression atmosphérique normale_).  

__b-__ Proposez une autre version de ce prédicat `is_liquid_water_bis` sans utiliser d'opérateurs logiques (and, or, not ...) mais avec uniquement des suites d'instructions conditionnelles.

__5)__  Définissez un prédicat `is_even` renvoyant `True` si le nombre passé en paramètre est pair, `False` sinon.  

__6__) 🥇 Depuis l'ajustement du calendrier grégorien en 1582, les années sont bisextilles si l'année est divisible par 4 et non divisible par 100 __ou encore__ si l'annnée est divisible par 400  

Définissez un prédicat `est_bisextille` renvoyant `True` si l'entier passé en paramètre est une année bisextille  et `False` sinon.  

_Indications_ :

 * L'année doit être postérieure à 1582, testez d'abord ce critère dans votre Predicat.  
 * Quelques années bisextilles à tester : 2012, 2016, 2020 et quelques années ne l'étant pas : 1900, 2100  


# Exercice n°5 : Machine à sous  &#x1F3C6; &#x1F3C6;

![machine à sous](./fig/machine_sous.jpg)[^1]  

Il existe différents modèles de machines à sous, aussi appelées bandits manchots.   
Un modèle simple consiste en 3 rouleaux comportant 5 symboles (représentés ici par les chiffres de 1 à 5). 	
Les combinaisons gagnantes et les gains correspondants sont indiqués ci-dessous :  

|Combinaisons|gain(en euros)|  
|:----:|:---:|  
|1 1 1|300|  
|2 2 2 |100|  
|3 3 3 |70|  
|4 4 4|30|  
|5 5 5 |10|  
|deux 1 parmi les 3 rouleaux|5|  
|un seul 1 parmi les 3 rouleaux|2|   
  

__Définir une fonction `calcul_gain(r1, r2, r3)` acceptant trois paramètres entiers correspondant aux symboles sur les rouleaux et dont la valeur de retour est la somme gagnée.__  


# Exercice n°6 : Clé de détermination &#x1F3C6; &#x1F3C6; 

Une clé de détermination est un outil de détermination d'espèces se basant sur une successions de choix portant sur des caractères divers.  

On souhaite déterminer le type de mésange observée en France métropolitaine parmi 6 espèces courantes

![mésange boréale](./fig/mesange.jpg)[^2]  

En utilisant la clé suivante rédigez un programme permettant à un utilisateur de retrouver le nom de la mésange observée.

Vous utiliserez :  
* des fonctions `input` pour implémenter les questions posées. Pour simplifier le problème __n'utiliser que des questions dont la réponse est oui ou non__   
* une variable `reponse` stockant le nom de la mésange identifiée ou un message `'mésange non identifiée'`   
* une seule fonction `print` affichant le contenu de la variable réponse à la fin du programme.  

_Conseils_ :  
* Pensez aux indentations !   
* __Vous devez tenir compte des cas non déterminés__. Par exemple si le ventre est beige mais que le dessus de la tête n'est ni noir ni avec une huppe blanche et noire : la mésange n'est pas identifiée.   

![clé détermination mésanges](./fig/cle_mesanges.png)[^3]  


# Exercice n°7 : NIRPP  &#x1F3C6; &#x1F3C6; 

Le numéro de sécurité sociale ou NIRPP (" numéro d'inscription au répertoire des personnes physiques") est un nombre à 13 chiffres (suivi d'une clé de contrôle de 2 chiffres) permettant d'identifier des personnes à partir de données liées à leur état civil (sexe, date et lieu de naissance...)

Par exemple :     
$`\bf \underbrace{\underbrace{2}_{\text{   sexe   }} \underbrace{69}_{\text{     année de naissance     }} \underbrace{05}_{\text{     mois ...     }} \underbrace{49}_{\text{     département ...    }} \underbrace{588}_{\text{     commune...     }}\underbrace{157}_{\text{     ordre de naissance     }}}_{\text{   NIRPP   }} \underbrace{80}_{\text{     clé de contrôle    }}`$

_Remarques :_  

* Le premier chiffre est égal à 1 pour les hommes ou 2 pour les femmes   
* La clé de contrôle permet de vérifier la validité du NIRPP grâce à la formule suivante :    
$`\bf \text{\bf clé de contrôle}=97 - NIRPP\mod{97}`$

__1)__ Définissez une fonction `sexe_nirpp` acceptant en paramètre un nirpp complet (avec clé de contrôle) sous forme d'entier et renvoyant le sexe correspondant.

__2)__   
__a-__ A l'aide d'une expression Python bien choisi testez la formule donnée pour déterminer la clé de contrôle en prenant pour exemple le NIRPP fourni.    
__b-__ 🥇 Définissez un prédicat `verif_nirpp` acceptant en paramètre un nirpp complet (avec clé de contrôle) sous forme d'entier et renvoyant `True` si le NIRPP est valide, `False` dans le cas contraire.


# Exercice n°8 : Jeu du plus ou moins  &#x1F3C6; &#x1F3C6;

Le jeu du plus ou moins est un classique de la programmation pour les débutants, le principe est le suivant :  

* _Etape 1_: Un nombre entier compris dans entre 1 et 100 (bornes incluses) est tiré au sort.  
* _Etape 2_ :Le programme demande à l'utilisateur de deviner le nombre  
* _Etape 3_: Le programme compare la réponse au nombre "mystère" et vous indique si ce nombre est supérieur ou inférieur à votre réponse.    
* Les étapes 2 et 3 sont reproduites jusqu'à ce que l'utilisateur trouve le nombre mystère.  

__Proposez une implémentation de ce jeu en Python.__

[^1]:  [Slot machine](https://commons.wikimedia.org/wiki/File:Slot_machine.jpg) author Jeff Kubina

[^2]: [mésange à tête noire](https://commons.wikimedia.org/w/index.php?curid=485077)  

[^3]: [clé détermination mésanges](https://sites.google.com/site/desideespourlessvt/) par Alain LEMETAYER   
Clé d'après Guillaume Lecointre (sous la dir. de) (2008) Comprendre et enseigner la classification du vivant, Belin, page 185.    
Illustrations : Lars Svensson et al. (2000) L'album ornitho, Delachaux et Niestlé.
