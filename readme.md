# Documents pour la spécialité 1ère NSI : Lycée Marguerite de Flandre année scolaire 2019-2020

# 📜 Contributions et licence

* La plupart des documents proposés sur ce site sont sous licence creative commons : __BY-NC-SA__.  
Ils ont été conçus soit par l'équipe pédagogique de NSI du [lycée Marguerite de Flandre](https://marguerite-de-flandre-gondecourt.enthdf.fr/) soit par [moi](patrice.thibaud@ac-lille.fr).

![licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

_Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre_  
Merci de __respecter les conditions de la__ [__licence__](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/)

* Pour les documents provenants de sources extérieures : ces sources sont sytématiquement citées


# Cours, Exercices, TP

Suivez le lien pour accéder à l'ensemble des documents --> [📂](./docs/cours_tp.md)

# Ressources supplémentaires

Quelques fiches méthodes, conseils, liens  --> [📚](./ressources/ressources.md)

# Planning 

* Groupe __1NSINF1__

|Jours  |  Horaires  | Salle  |  
|:------|:------------|:-------|  
|Mardi|08H30-10H30|B101|  
|Jeudi|10H30-12H30|B101|  

* Groupe __1NSINF3__

|Jours  |  Horaires  | Salle  |  
|:------|:------------|:-------|  
|Lundi|15H30-17H30|B101|  
|Jeudi|13H30-15H30|B101|  

# Documents de rentrée

* [Diaporama de présentation](./docs/0_rentree/diapo_rentree_nsi.pdf)

* [Ressources](./docs/0_rentree/fiche_ressources_eleve.pdf)


# Contact

Pour toute demande ou remises de travaux   
📧 patrice.thibaud@ac-lille.fr

